package ch.soreco.xutil.excelexport;


import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import ch.soreco.xutil.util.DateParser;
import nu.xom.Attribute;
import nu.xom.Builder;
import nu.xom.Document;
import nu.xom.Element;
import nu.xom.Nodes;
import nu.xom.ParsingException;
import nu.xom.ValidityException;


public class XmlDataExtractor 
{
	String xmlData;
	Document doc;
	int	rowsize = 0;
	int columnsize = 0;

	public XmlDataExtractor(String xmlData) 
	{
		super();
		this.xmlData = xmlData;
		
		try
		{		
		  Builder parser = new Builder();
		  doc = parser.build(xmlData, null);
		}
		catch(IOException ex)
		{
			//Ivy.log().error("XmlDataExtractor: Constructor XmlDataExtractor -> IOException: "+ex.getMessage());
			String msg = String.format("XmlDataExtractor: Constructor XmlDataExtractor -> IOException: %1$s ", ex.getMessage());
			System.out.println(msg);
		}
		catch(ValidityException ex)
		{
			//Ivy.log().error("XmlDataExtractor: Constructor XmlDataExtractor -> ValidityException: "+ex.getMessage());
			String msg = String.format("XmlDataExtractor: Constructor XmlDataExtractor -> ValidityException: %1$s ", ex.getMessage());
			System.out.println(msg);
		}
		catch (ParsingException ex)
		{
			//Ivy.log().error("XmlDataExtractor: Constructor XmlDataExtractor -> ParsingException: "+ex.getMessage());
			String msg = String.format("XmlDataExtractor: Constructor XmlDataExtractor -> ParsingException: %1$s ", ex.getMessage());
			System.out.println(msg);
		}
		
	}
	
	
	public List<Object> getData()
	{
		List<Object> matrix = null;
		List<Object> row = null;
		Element eRecord;
		
		// looking for root element <exportdata>
		Element root = doc.getRootElement();
		
		// extract rowsize and columnsize from root element
		rowsize = getRowsize(root);
		columnsize = getColumnsize(root);

		//Ivy.log().info("XmlDataExtractor: getData -> Rowsize: "+rowsize);
		//Ivy.log().info("XmlDataExtractor: getData -> Columnsize: "+columnsize);
		
		// create the list --> a list of list! (List<List<Object>>)
		//matrix = List.create(Object.class);
		matrix = new ArrayList<Object>();
		
		//Ivy.log().debug("XmlDataExtractor: getData -> List matrix created: "+rowsize);
		
		//Nodes nRecords = doc.query("//exportdata/record");
		
		// *** get path to templtes ***
		for(int i=0 ; i<rowsize ; i++ )
		{
			// get record by rownumber (first is 1, not zero based)
			Nodes nRecords = doc.query("//record[@rownumber='"+(i+1)+"']");
			//Nodes nRecords = doc.query("//record["+i+"]");
			//Nodes nRecords = doc.query("//record");
			
			for(int k=0 ; k<nRecords.size() ; k++ )
			{
				// get the record element (should be only one)
				if( nRecords.get(k) instanceof Element )
				{
					// create the list for this row (list of objects to hold the fields)
					//row = List.create(Object.class);
					row = new ArrayList<Object>();
					//Ivy.log().debug("XmlDataExtractor: getData -> List for row created -> number of columns: "+columnsize);
					//Object[] row = new Object[columnsize];
					eRecord = (Element) nRecords.get(k);
					
					// get the value of every column
					for( int c=0 ; c<columnsize ; c++ )
					{
						// get the value of column c (first is 0, zero based)
						Nodes nData = eRecord.query("./data[@column='"+c+"']");
						
						//Ivy.log().debug("XmlDataExtractor: getData -> nData Element found! " + nData.size());
						
						for( int m=0 ; m<nData.size() ; m++ )
						{
							// get data element (should be only one)
							if( nData.get(m) instanceof Element )
							{
								//Ivy.log().debug("XmlDataExtractor: getData -> Data Element found!");
								Element eData = (Element) nData.get(m);
								// get value as correct data type
								Object obj = getDataObject(eData);
								// add object (field) to this row
								row.add(obj);
							}
						}
					}
					// add row (a list of objects) to list
					matrix.add(row);
				}
			}
		}
	
		return matrix;
	}
	
	
	public List<Object> getData2()
	{
		List<Object> matrix = null;
		List<Object> row = null;
		Element eRecord;
		
		// looking for root element <exportdata>
		Element root = doc.getRootElement();
		
		// extract rowsize and columnsize from root element
		rowsize = getRowsize(root);
		columnsize = getColumnsize(root);
	
		//Ivy.log().info("XmlDataExtractor: getData -> Rowsize: "+rowsize);
		//Ivy.log().info("XmlDataExtractor: getData -> Columnsize: "+columnsize);
		
		// create the list --> a list of list! (List<List<Object>>)
		//matrix = List.create(Object.class);
		matrix = new ArrayList<Object>();
		
		//Ivy.log().debug("XmlDataExtractor: getData -> List matrix created: "+rowsize);
		
		//Nodes nRecords = doc.query("//exportdata/record");
		
		// *** get path to templtes ***
//		for(int i=0 ; i<rowsize ; i++ )
//		{
			// get record by rownumber (first is 1, not zero based)
			//Nodes nRecords = doc.query("//record[@rownumber='"+(i+1)+"']");
			//Nodes nRecords = doc.query("//record["+i+"]");
			Nodes nRecords = doc.query("//record");
			
			for(int k=0 ; k<nRecords.size() ; k++ )
			{
				// get the record element (should be only one)
				if( nRecords.get(k) instanceof Element )
				{
					// create the list for this row (list of objects to hold the fields)
					//row = List.create(Object.class);
					row = new ArrayList<Object>();
					//Ivy.log().debug("XmlDataExtractor: getData -> List for row created -> number of columns: "+columnsize);
					//Object[] row = new Object[columnsize];
					eRecord = (Element) nRecords.get(k);
					
					// get the value of every column
					for( int c=0 ; c<columnsize ; c++ )
					{
						// get the value of column c (first is 0, zero based)
						Nodes nData = eRecord.query("./data[@column='"+c+"']");
						
						//Ivy.log().debug("XmlDataExtractor: getData -> nData Element found! " + nData.size());
						
						for( int m=0 ; m<nData.size() ; m++ )
						{
							// get data element (should be only one)
							if( nData.get(m) instanceof Element )
							{
								//Ivy.log().debug("XmlDataExtractor: getData -> Data Element found!");
								Element eData = (Element) nData.get(m);
								// get value as correct data type
								Object obj = getDataObject(eData);
								// add object (field) to this row
								row.add(obj);
							}
						}
					}
					// add row (a list of objects) to list
					matrix.add(row);
				}
			}
//		}
	
		return matrix;
	}	
	
	
	private Object getDataObject(Element _eData)
	{
		Object obj;
		String value;
		value = _eData.getValue();
		Attribute aDatatype = _eData.getAttribute("type");
		String datatype = aDatatype != null ? aDatatype.getValue() : "java.lang.String" ;
		
		//Ivy.log().debug( String.format("XmlDataExtractor: getDataObject -> Datatype: '%1$s' , Value: '%2$s'", datatype, value) );
		
		try
		{
			if( datatype.equals(BigDecimal.class.getName()) )			// java.math.BigDecimal
			{
				BigDecimal t = new BigDecimal(value);
				obj = t;
			}
			else if( datatype.equals(Double.class.getName()) || datatype.equals(Float.class.getName()) )	// java.lang.Double, java.lang.Float
			{
				BigInteger t = new BigInteger(value);
				obj = t;
			}
			else if( datatype.equals(Number.class.getName()) )		// java.lang.Number
			{
				BigDecimal t = new BigDecimal(value);
				obj = t;
			}
			else if( datatype.equals(BigInteger.class.getName()) )	// java.math.BigInteger
			{
				BigInteger t = new BigInteger(value);
				obj = t;
			}
			else if( datatype.equals(Integer.class.getName()) )		// java.lang.Integer
			{
				Integer t = new Integer(value);
				obj = t;
			}
//			else if( datatype.equals(Date.class.getName()) )			// ch.ivyteam.ivy.scripting.objects.Date
//			{
//				Date t = new Date(value);
//				obj = t;
//			}
//			else if( datatype.equals(DateTime.class.getName()) )		// ch.ivyteam.ivy.scripting.objects.DateTime
//			{
//				DateTime t = new DateTime(value);
//				obj = t;
//			}
			else if( datatype.equals(java.util.Date.class.getName()) ||				// java.util.Date
					 datatype.equals(java.util.GregorianCalendar.class.getName()) )	// java.util.GregorianCalendar
			{
				// create GregorianCalendar also for java.util.Date
				GregorianCalendar t = DateParser.parseGregorianCalendarDate(value);
				obj = t;
			}
			else if( datatype.equals(String.class.getName()) )	// java.lang.String
			{
				obj = value;
			}
			else
			{
				// Type not explicit supported --> Default is String
				obj = value;
			}
		}
		catch(NumberFormatException ex)
		{
			String msg = String.format("XmlDataExtractor: getDataObject -> NumberFormatException: Value '%1$s' has wrong format, check export data! '%2$s' ", value, ex.getMessage());
			//Ivy.log().warn(msg);
			System.out.println(msg);
			obj = value;
		}
		//Ivy.log().debug("Value found: "+obj.toString());
		return obj;
	}
	
	
	private int getRowsize(Element _root)
	{
		int _rowsize;
		
		Attribute aRowsize = _root.getAttribute("rowsize");
		String sRowsize = aRowsize != null ? aRowsize.getValue() : "" ;
		try
		{
			_rowsize = Integer.parseInt(sRowsize);
		}
		catch(NumberFormatException ex)
		{
			//Ivy.log().error("XmlDataExtractor: getData -> NumberFormatException: Attribute rowsize not valid, check xml! "+ex.getMessage());
			String msg = String.format("XmlDataExtractor: getData -> NumberFormatException: Attribute rowsize not valid, check xml! %1$s ", ex.getMessage());
			System.out.println(msg);
			_rowsize = 0;
		}
		
		return _rowsize;
	}
	
	private int getColumnsize(Element _root)
	{
		int _colsize;
		
		Attribute aColsize = _root.getAttribute("columnsize");
		String sColsize = aColsize != null ? aColsize.getValue() : "" ;
		try
		{
			_colsize = Integer.parseInt(sColsize);
		}
		catch(NumberFormatException ex)
		{
			//Ivy.log().error("XmlDataExtractor: getData -> NumberFormatException: Attribute rowsize not valid, check xml! "+ex.getMessage());
			String msg = String.format("XmlDataExtractor: getData -> NumberFormatException: Attribute rowsize not valid, check xml! %1$s ", ex.getMessage());
			System.out.println(msg);
			_colsize = 0;
		}
		
		return _colsize;
	}

}
