package ch.soreco.xutil.excelexport;

import org.junit.Test;

import ch.soreco.xbase.test.ExcelExportTestUnit;
import ch.soreco.xutil.util.ExLog;
import ch.soreco.xutil.util.PerfMeter;

//import static org.junit.Assert.assertEquals;


public class ExcelExportTest {
	
	
	//@Test
	public void test_prepareExcelExport() {
		
		@SuppressWarnings("unused")
		String template = "C:\\ECLIPSE_PROJECTS\\EXCEL_EXPORT\\templates\\Excel_Template_01.xltx";
		
		//ExLog.warn(this, new Object(){}, "Testmeldung information!");
		
		PerfMeter pm = new PerfMeter(); 
		ExcelExportTestUnit eetu = new ExcelExportTestUnit(500, false);
		pm.stop().logExecutionTimeInfo("ExcelExportTestUnit(generateData)");
		
		pm.start();
		String path = eetu.generateExcelFileSXSSF("" /*template*/);
		pm.stop().logExecutionTimeInfo("generateExcelFile");
		
		ExLog.info(this, new Object(){}, "Export File --> "+path);
		

//		String result = AbstractXpertLineSearch.prepareLookupString("Reto Weber Rapperswil-Jona");
//		String expected = " Like Upper('% Reto% Weber% Rapperswil-Jona%')";
//		assertEquals("prepareLookupStringSimple", expected, result);
	}
	
	
	//@Test
	public void test_prepareExcelExportWithStyle() {
		
		@SuppressWarnings("unused")
		String template = "C:\\ECLIPSE_PROJECTS\\EXCEL_EXPORT\\templates\\Excel_Template_01.xltx";
		
		//ExLog.warn(this, new Object(){}, "Testmeldung information!");
		
		PerfMeter pm = new PerfMeter(); 
		ExcelExportTestUnit eetu = new ExcelExportTestUnit(5000, false);
		pm.stop().logExecutionTimeInfo("ExcelExportTestUnit(generateData)");
		
		pm.start();
		String path = eetu.generateExcelFileSXSSF_Formatted("" /*template*/);
		pm.stop().logExecutionTimeInfo("generateExcelFile");
		
		ExLog.info(this, new Object(){}, "Export File --> "+path);
		

//		String result = AbstractXpertLineSearch.prepareLookupString("Reto Weber Rapperswil-Jona");
//		String expected = " Like Upper('% Reto% Weber% Rapperswil-Jona%')";
//		assertEquals("prepareLookupStringSimple", expected, result);
	}
	
	@Test
	public void test_prepareExcelExportWithMultiSheet() {
		
		@SuppressWarnings("unused")
		String template = "C:\\ECLIPSE_PROJECTS\\EXCEL_EXPORT\\templates\\Excel_Template_01.xltx";
		
		//ExLog.warn(this, new Object(){}, "Testmeldung information!");
		
		PerfMeter pm = new PerfMeter(); 
		ExcelExportTestUnit eetu = new ExcelExportTestUnit(150, true);
		pm.stop().logExecutionTimeInfo("ExcelExportTestUnit(generateData)");
		
		pm.start();
		String path = eetu.generateExcelFileSXSSF_MultiSheet(null /*template*/);
		pm.stop().logExecutionTimeInfo("generateExcelFile");
		
		ExLog.info(this, new Object(){}, "Export File --> "+path);
		

//		String result = AbstractXpertLineSearch.prepareLookupString("Reto Weber Rapperswil-Jona");
//		String expected = " Like Upper('% Reto% Weber% Rapperswil-Jona%')";
//		assertEquals("prepareLookupStringSimple", expected, result);
	}

}
