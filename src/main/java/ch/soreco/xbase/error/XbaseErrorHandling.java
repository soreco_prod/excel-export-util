package ch.soreco.xbase.error;

//import javax.faces.application.FacesMessage;
//import javax.faces.application.FacesMessage.Severity;

/**
 * XbaseErrorHandling 
 * @author he
 * 
 */
public class XbaseErrorHandling 
{
	public static enum XbaseSeverity {INFO, WARN, ERROR, FATAL};
	
	private String errorTitle;
	private String errorText;
	private Integer errorNumber;
	private String errorExpertText;
	//private String errorIconUrl;
	private XbaseSeverity errorSeverity;
	

	/**
	 * Default Constructor
	 */
	public XbaseErrorHandling()
	{
		super();
		errorNumber = 0;
		errorTitle = "";
		errorText = "";
		errorExpertText = "";
		errorSeverity = XbaseSeverity.INFO;	// Default
	}
	
	/**
	 * Constructor for simple initialize
	 * @param _severity severity
	 * @param _number error number
	 * @param _text msg text
	 */
	public XbaseErrorHandling(XbaseSeverity _severity, Integer _number, String _text)
	{
		super();
		errorNumber = _number;
		errorTitle = "";
		errorText = _text;
		errorExpertText = "";
		errorSeverity = _severity;
	}


	/**
	 * @return the errorTitle
	 */
	public String getErrorTitle()
	{
		return errorTitle;
	}

	/**
	 * @param errorTitle
	 *            the errorTitle to set
	 */
	public void setErrorTitle(String errorTitle)
	{
		this.errorTitle = errorTitle;
	}

	/**
	 * @return the errorText
	 */
	public String getErrorText()
	{
		return errorText;
	}

	/**
	 * @param errorText
	 *            the errorText to set
	 */
	public void setErrorText(String errorText)
	{
		this.errorText = errorText;
	}

	/**
	 * @return the errorNumber
	 */
	public Integer getErrorNumber()
	{
		return errorNumber;
	}

	/**
	 * @param errorNumber
	 *            the errorNumber to set
	 */
	public void setErrorNumber(Integer errorNumber)
	{
		this.errorNumber = errorNumber;
	}

	/**
	 * @return the errorExpertText
	 */
	public String getErrorExpertText()
	{
		return errorExpertText;
	}

	/**
	 * @param errorExpertText
	 *            the errorExpertText to set, typically the StackTrace
	 */
	public void setErrorExpertText(String errorExpertText)
	{
		this.errorExpertText = errorExpertText;
	}

//	/**
//	 * @return the errorIconUrl
//	 */
//	public String getErrorIconUrl()
//	{
//		return errorIconUrl;
//	}
//
//	/**
//	 * @param errorIconUrl
//	 *            the errorIconUrl to set
//	 */
//	public void setErrorIconUrl(String errorIconUrl)
//	{
//		this.errorIconUrl = errorIconUrl;
//	}

	/**
	 * @return the errorSeverity
	 */
	public XbaseSeverity getErrorSeverity()
	{
		return errorSeverity;
	}

	/**
	 * @param errorSeverity
	 *            the errorSeverity to set
	 */
	public void setErrorSeverity(XbaseSeverity errorSeverity)
	{
		this.errorSeverity = errorSeverity;
	}
	
	
//	public FacesMessage.Severity getFacesSeverity()
//	{
//		javax.faces.application.FacesMessage.Severity fs;
//		if( errorSeverity == XbaseSeverity.FATAL )
//		{
//			fs = javax.faces.application.FacesMessage.SEVERITY_FATAL;
//		}
//		else if( errorSeverity == XbaseSeverity.ERROR )
//		{
//			fs = javax.faces.application.FacesMessage.SEVERITY_ERROR;
//		}
//		else if( errorSeverity == XbaseSeverity.WARN )
//		{
//			fs = javax.faces.application.FacesMessage.SEVERITY_WARN;
//		}
//		else 
//		{
//			fs = javax.faces.application.FacesMessage.SEVERITY_INFO;
//		}
//		
//		return fs;
//	}

	
	public boolean errorOccurred()
	{
		// true if number is not zero
		return errorNumber != 0;
	}
	
	public boolean hasDetailInfo()
	{
		// true if errorExpertText is not empty
		return !errorExpertText.isEmpty();
	}
		

	/**
	 * Key of the objects are equal.
	 * 
	 * @param obj ErrorHandling
	 * @return true if all fields (values) of this object are equal
	 */
	@Override
	public boolean equals(Object obj) 
	{
		if (obj == null)
			return false;
		if (obj == this)
			return true;
		if (!(obj instanceof XbaseErrorHandling))
			return false;
		XbaseErrorHandling objErr = (XbaseErrorHandling) obj;
		return (	objErr.getErrorTitle().equalsIgnoreCase(this.errorTitle) && 	
					objErr.getErrorText().equalsIgnoreCase(this.errorText) && 
					objErr.errorNumber.intValue() == this.errorNumber.intValue() &&
					objErr.getErrorExpertText().equalsIgnoreCase(this.errorExpertText) && 
					//objErr.errorIconUrl.equalsIgnoreCase(this.errorIconUrl) && 
					objErr.getErrorSeverity().equals(this.errorSeverity)
				);
	}
	
	
}
