package ch.soreco.xbase.test;

import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
//import java.util.Date;
import java.util.logging.Logger;

import ch.soreco.xutil.excelexport.ExcelExporterSXSSF;
import ch.soreco.xutil.excelexport.ExcelExporterXSSF;
import ch.soreco.xutil.excelexport.config.ColumnStyle;
import ch.soreco.xutil.excelexport.config.ExcelSheetData;
import ch.soreco.xutil.excelexport.config.PredefinedRowStyle;
import ch.soreco.xutil.excelexport.config.RowStyle;
import ch.soreco.xutil.filemanager.FileHandler;
import ch.soreco.xutil.util.Base64Encoder;
import ch.soreco.xutil.util.GuidGenerator;

public class ExcelExportTestUnit 
{
	private int size;									// number of records
	private ArrayList<String> headers;					// column headers
	private ArrayList<ArrayList<Object>> data;			// matrix of data
	
	private List<ExcelSheetData> excelSheetDataList;
	
	
	// Constructor
	public ExcelExportTestUnit(int _numOfRecords, boolean multiSheet) 
	{
		super();
		
		this.size = _numOfRecords;
		if( _numOfRecords <= 0 )
		{
			// default
			this.size = 1000;
		}
		
		if( !multiSheet ) {
			// generate test data
			generateData();
		}
		else {
			generateSheetData();
		}
	}
	
	
	private void generateData()
	{
	
		// Generate headers
		headers = new ArrayList<String>();
		headers.add("Datum");
		headers.add("Datum-Zeit");
		headers.add("Name");
		headers.add("Betrag (BigDecimal)");
		headers.add("Bemerkung");
		headers.add("Type (Integer)");
		
		// Generate any data
		data = new ArrayList<ArrayList<Object>>(size) ;
		
		for( int i=0 ; i < size ; i++ )
		{
			ArrayList<Object> tmpL = new ArrayList<Object>(6);
			// COLUMN 1: Date
			Calendar cal = Calendar.getInstance();
			cal.clear();
			cal.set(2016, 8, i+1);
			tmpL.add( cal );
			// COLUMN 2: Date and Time
			Calendar cal2 = Calendar.getInstance();
			cal2.set(2014, 8, i+1);
			tmpL.add( cal2 );
			//tmpL.add( new Date(2014, 9, i+1, 13, 12, 22) );
			// COLUMN 3: String
			tmpL.add( new String("Name "+i) );
			// COLUMN 4: BigDecimal
			tmpL.add( new BigDecimal(i*1000).setScale(2) );
			// COLUMN 5: String
			String remark;
			if( i%10 == 0 )
			{
				remark = new String("Differenz "+i);
			}
			else
			{
				remark = new String("Bemerkung "+i);
			}
			tmpL.add( remark );
			// COLUMN 6: BigInteger
			//int k = i+1;
			//tmpL.add( k );
			tmpL.add( BigInteger.valueOf(i+1) );

			data.add(tmpL);
		}
	}
	
	private void generateSheetData()
	{
		excelSheetDataList = new ArrayList<ExcelSheetData>();
		
		
		
		for( int k=0 ; k<3 ; k++ ) {
			
			ArrayList<String> headers2;					// column headers
			ArrayList<ArrayList<Object>> data2;
			
			String sheetName = "Blatt "+(k+1);
			
			// Generate headers
			headers2 = new ArrayList<String>();
			headers2.add("Datum");
			headers2.add("Datum-Zeit");
			headers2.add("Name");
			headers2.add("Betrag (BigDecimal)");
			headers2.add("Bemerkung");
			headers2.add("Type (Integer)");
			
//			for( ExcelSheetData esdTemp : excelSheetDataList  ) {
//				headers2.add(esdTemp.getSheetName());
//			}
			
			
			// Generate any data
			data2 = new ArrayList<ArrayList<Object>>(size) ;
			
			for( int i=0 ; i < size ; i++ )
			{
				// insert additional header line
				if( i == 15 ) {
					ArrayList<Object> tmpL2 = new ArrayList<Object>(6);
					tmpL2.add(headers2.get(0));
					tmpL2.add(headers2.get(1));
					tmpL2.add(headers2.get(2));
					tmpL2.add(headers2.get(3));
					tmpL2.add(headers2.get(4));
					tmpL2.add(headers2.get(5));
					data2.add(tmpL2);
				}
				else if( i == 17 ) {
					ArrayList<Object> tmpL3 = new ArrayList<Object>(6);
					String blank = "";
					tmpL3.add(blank);
					tmpL3.add(blank);
					tmpL3.add(blank);
					tmpL3.add(blank);
					tmpL3.add(blank);
					tmpL3.add(blank);
					data2.add(tmpL3);
				}
				else {
				
					ArrayList<Object> tmpL = new ArrayList<Object>(6);
					// COLUMN 1: Date
					Calendar cal = Calendar.getInstance();
					cal.clear();
					cal.set(1970+((k+i)%50), 8, i+1);
					tmpL.add( cal );
					// COLUMN 2: Date and Time
					Calendar cal2 = Calendar.getInstance();
					cal2.set(1970+((k+i)%100), 9, i+1);
					tmpL.add( cal2 );
					//tmpL.add( new Date(2014, 9, i+1, 13, 12, 22) );
					// COLUMN 3: String
					tmpL.add( new String("Name "+i) );
					// COLUMN 4: BigDecimal
					tmpL.add( new BigDecimal((k+1)*(i+1)*1000).setScale(2) );
					// COLUMN 5: String
					String remark;
					if( i%10 == 0 )
					{
						remark = new String("Differenz "+(k+k+i));
					}
					else
					{
						remark = new String("Bemerkung "+(k+i));
					}
					tmpL.add( remark );
					// COLUMN 6: BigInteger
					//int k = i+1;
					//tmpL.add( k );
					tmpL.add( BigInteger.valueOf(i+1) );
					
	//				for( ExcelSheetData esdTemp : excelSheetDataList  ) {
	//					tmpL.add("Data_"+esdTemp.getSheetName());
	//				}
	
					data2.add(tmpL);
				}

			}
			
			
			// *** create sample styles (the excel export consumer has to define these styles for columns and rows) *** 
			// create some ColumnStyles to format columns
			ArrayList<ColumnStyle> columnStyles = new ArrayList<ColumnStyle>();
			ColumnStyle cs1 = new ColumnStyle(1, "#FAEA93");
			//cs1.setBold(true);
			ColumnStyle cs2 = new ColumnStyle(headers2.size()-1, "#ceffc2");
			cs2.setBold(true);
			cs2.setFontColorRgb("#B114DB");
			
			ColumnStyle cs3 = new ColumnStyle(3, "#D39DE6");
			cs3.setScale(0);
			
			columnStyles.add(cs1);
			columnStyles.add(cs2);
			columnStyles.add(cs3);
			
			// create some RowStyles to format rows
			ArrayList<RowStyle> rowStyles = new ArrayList<RowStyle>();
			RowStyle rs1 = new RowStyle("ROW1", "#F0D4A0");
			rs1.setBold(true);
			RowStyle rs2 = new RowStyle("ROW2", "#AFDCFA");
			RowStyle rs3 = new RowStyle("ROW3", "#FA3F5F");
			rs3.setBold(true);
			rs3.setFontColorRgb("#BEBEBE"); rs3.setScale(4);
			rowStyles.add(rs1);
			rowStyles.add(rs2);
			rowStyles.add(rs3);
			
			// set row style to some arbitrary rows
			Map<Integer,String> rowStyleIdMap = new HashMap<Integer,String>();
			rowStyleIdMap.put(3, rs1.getId());
			rowStyleIdMap.put(5, rs1.getId());
			rowStyleIdMap.put(7, rs1.getId());
			rowStyleIdMap.put(13, rs1.getId());
			
			rowStyleIdMap.put(10, rs2.getId());
			rowStyleIdMap.put(25, rs2.getId());
			rowStyleIdMap.put(50, rs2.getId());
			
			// a HEADER styled row
			rowStyleIdMap.put(15, PredefinedRowStyle.HEADER.getId());
			
			rowStyleIdMap.put(17, PredefinedRowStyle.SEPARATOR.getId());
			
			rowStyleIdMap.put(20, rs3.getId());
			
			for( int m=0 ; m<data2.size() ; m++ ) {
				int test =  m % 100;
				if( test == 0 ) {
					rowStyleIdMap.put(m, rs3.getId());
				}
			}
			
			
			ExcelSheetData esd = new ExcelSheetData(headers2, data2);
	    	esd.setSheetName(sheetName);
	    	esd.setTitle("Titel: "+sheetName);
//	    	esd.setSubtitle(subtitle);
//	    	esd.setConvertNumericStrings(convertNumericStrings);
	    	esd.setColumnStyles(columnStyles);
	    	esd.setRowStyles(rowStyles);
	    	esd.setRowStyleIdMap(rowStyleIdMap);
			
	    	excelSheetDataList.add(esd);
	    	
	    	System.out.println(String.format("Data for Sheet %1$s created: %2$s", sheetName, data2.size()));
			
		}

	}
	
	/**
	 * XSSF Test function to generate a excel file with generated data to the default system temp folder
	 * @return the path to the generated excel file
	 */
	public String generateExcelFileXSSF()
	{
		File f;
		String path;
		
		System.out.println("Start creating Excel file: ");
		
		// *** Create excel file ***
		ExcelExporterXSSF eex = new ExcelExporterXSSF();
		//ExcelExporterSXSSF eex = new ExcelExporterSXSSF();
		f = eex.exportListAsExcel(headers, data, "TEST", "TEST Excel Export (file)", null, false, "");
		
		//System.out.println("Excel File: " + f.getAbsolutePath());
		path = f.exists() ? f.getAbsolutePath() : "";
		return path;
	}
	
	/**
	 * SXSSF Test function to generate a excel file with generated data to the default system temp folder
	 * @param template path to template file
	 * @return the path to the generated excel file
	 */
	public String generateExcelFileSXSSF(String template)
	{
		File f;
		String path;
		String[] subTitles = new String[1];
		
		String subTitle1 = "Anzahl Datensätze: "+ data.size();
		subTitles[0] = subTitle1; 
		
		System.out.println("Start creating Excel file: ");
		
		// *** Create excel file ***
		ExcelExporterSXSSF eex = new ExcelExporterSXSSF();
		f = eex.exportListAsExcel(headers, data, "TEST", "TEST Excel Export (file)", subTitles, false, template, null, null, null);
		
		//System.out.println("Excel File: " + f.getAbsolutePath());
		path = f.exists() ? f.getAbsolutePath() : "";
		return path;
	}
	
	/**
	 * SXSSF Test function to generate a excel file with generated data to the default system temp folder
	 * @param template path to template file
	 * @return the path to the generated excel file
	 */
	public String generateExcelFileSXSSF_MultiSheet(String template)
	{
		File f;
		String path;
//		String[] subTitles = new String[1];
		
//		String subTitle1 = "Anzahl Datensätze: "+ data.size();
//		subTitles[0] = subTitle1; 
		
		System.out.println("Start creating Excel file: ");
		

		// *** Create excel file ***
		ExcelExporterSXSSF eex = new ExcelExporterSXSSF();
		f = eex.exportListAsExcel(this.excelSheetDataList, template);
		//f = eex.exportListAsExcel(headers, data, "TEST", "TEST Excel Export Styled (file)", subTitles, false, template, columnStyles, rowStyles, rowStyleIdMap);
		//f = eex.exportListAsExcel(headers, data, "TEST", "TEST Excel Export Styled (file)", subTitles, false, template, null, null, null);
		
		//System.out.println("Excel File: " + f.getAbsolutePath());
		Logger.getLogger("excel.export").info("Excel File SXSSF_MultiSheet: " + f.getAbsolutePath()+" !");
		path = f.exists() ? f.getAbsolutePath() : "";
		return path;
	}
	
	
	/**
	 * SXSSF Test function to generate a excel file with generated data to the default system temp folder
	 * @param template path to template file
	 * @return the path to the generated excel file
	 */
	public String generateExcelFileSXSSF_Formatted(String template)
	{
		File f;
		String path;
		String[] subTitles = new String[1];
		
		String subTitle1 = "Anzahl Datensätze: "+ data.size();
		subTitles[0] = subTitle1; 
		
		System.out.println("Start creating Excel file: ");
		
		// *** create sample styles (the excel export consumer has to define these styles for columns and rows) *** 
		// create some ColumnStyles to format columns
		ArrayList<ColumnStyle> columnStyles = new ArrayList<ColumnStyle>();
		ColumnStyle cs1 = new ColumnStyle(2, "#FAEA93");
		//cs1.setBold(true);
		ColumnStyle cs2 = new ColumnStyle(4, "#ceffc2");
		cs2.setBold(true);
		cs2.setFontColorRgb("#B114DB");
		columnStyles.add(cs1);
		columnStyles.add(cs2);
		
		// create some RowStyles to format rows
		ArrayList<RowStyle> rowStyles = new ArrayList<RowStyle>();
		RowStyle rs1 = new RowStyle("ROW1", "#F0D4A0");
		RowStyle rs2 = new RowStyle("ROW2", "#AFDCFA");
		RowStyle rs3 = new RowStyle("ROW3", "#FA3F5F");
		rs3.setBold(true);
		rs3.setFontColorRgb("#BEBEBE");
		rowStyles.add(rs1);
		rowStyles.add(rs2);
		rowStyles.add(rs3);
		
		// set row style to some arbitrary rows
		Map<Integer,String> rowStyleIdMap = new HashMap<Integer,String>();
		rowStyleIdMap.put(3, rs1.getId());
		rowStyleIdMap.put(5, rs1.getId());
		rowStyleIdMap.put(7, rs1.getId());
		rowStyleIdMap.put(13, rs1.getId());
		
		rowStyleIdMap.put(10, rs2.getId());
		rowStyleIdMap.put(25, rs2.getId());
		rowStyleIdMap.put(50, rs2.getId());
		
		rowStyleIdMap.put(20, rs3.getId());
		
		for( int k=0 ; k<data.size() ; k++ ) {
			int test =  k % 1000;
			if( test == 0 ) {
				rowStyleIdMap.put(k, rs3.getId());
			}
		}
		
		
		// *** Create excel file ***
		ExcelExporterSXSSF eex = new ExcelExporterSXSSF();
		f = eex.exportListAsExcel(headers, data, "TEST", "TEST Excel Export Styled (file)", subTitles, false, template, columnStyles, rowStyles, rowStyleIdMap);
		//f = eex.exportListAsExcel(headers, data, "TEST", "TEST Excel Export Styled (file)", subTitles, false, template, null, null, null);
		
		//System.out.println("Excel File: " + f.getAbsolutePath());
		path = f.exists() ? f.getAbsolutePath() : "";
		return path;
	}
	
	/**
	 * Test function to generate a excel file with generated data (this version returns the excel file as a base64 encoded string!)
	 * @return the excel file as a base64 encoded string
	 */
	public String generateBase64ExcelFile()
	{
		
		// *** Create excel file as base64 string ! ***
		ExcelExporterXSSF eexBase64 = new ExcelExporterXSSF();
		String b64 = eexBase64.exportListAsExcelBase64(headers, data, "TEST", "TEST Excel Export (base64)", null, false, "");
		
		// decode base64 and store as file (to check if b64 is really a excel file)
		String filepath = System.getProperty("java.io.tmpdir");
		filepath = FileHandler.formatPathWithEndSeparator(filepath)+"EXCEL_EXPORT";
		filepath = FileHandler.formatPathWithEndSeparator(filepath, true);
		filepath = filepath+"ExcelFileBase64_"+GuidGenerator.generateID()+".xlsx";
		File eexBase64File = new File(filepath);
		Base64Encoder.decodeBase64(b64, eexBase64File);
		
		System.out.println("Excel File Base64: " + eexBase64File.getAbsolutePath());
		Logger.getLogger("").info("Excel File Base64: " + eexBase64File.getAbsolutePath()+" !");
		
		return b64;
	}

}
