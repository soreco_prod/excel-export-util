package ch.soreco.xutil.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.nio.file.Files;
import org.apache.commons.codec.binary.Base64;

//import ch.ivyteam.ivy.environment.Ivy;
//import ch.ivyteam.ivy.scripting.objects.File;

public class Base64Encoder 
{
	
	public static String encodeBase64(File _path)
	{
		String data = null;
		//java.io.File f = _path.getJavaFile();
		java.io.File f = _path;
		String path = f.getPath();
		
		try
		{
			byte[] b = Files.readAllBytes(Paths.get(path));
			
			//FileInputStream fis = new FileInputStream(_path);
			//String gaga = new String(fis.);
					
			byte[] encodedBytes = Base64.encodeBase64(b);
			data = new String(encodedBytes);
		}
		catch(IOException ex)
		{
			String msg = String.format("Base64Encoder: encodeBase64 -> IOException: Can't encode file '%1$s' to Base64 string! '%2$s' ", path, ex.getMessage());
			//Ivy.log().error(msg);
			System.out.println(msg);
		}
		return data;
//		System.out.println("encodedBytes " + new String(encodedBytes));
//		byte[] decodedBytes = Base64.decodeBase64(encodedBytes);
//		System.out.println("decodedBytes " + new String(decodedBytes));
	}
	
	public static boolean decodeBase64(String _base64, File _file)
	{
		//byte[] data = _base64.getBytes();
		String filename = "";
		boolean result = false;
		
		try
		{
			_file.createNewFile();
			//filename = _file.getJavaFile().getPath();
			filename = _file.getPath();
			byte[] data = Base64.decodeBase64(_base64);
			
			BufferedOutputStream bw = new BufferedOutputStream(new FileOutputStream(filename));
			
			bw.write(data);
			bw.flush();
			bw.close();
			result = true;
		}
		catch(FileNotFoundException ex)
		{
			String msg = String.format("Base64Encoder: decodeBase64 -> FileNotFoundException: File '%1$s' does not exist! '%2$s' ", filename, ex.getMessage());
			//Ivy.log().error(msg);
			System.out.println(msg);
		}
		catch(IOException ex)
		{
			String msg = String.format("Base64Encoder: decodeBase64 -> IOException: Can't decode to file '%1$s'! '%2$s' ", filename, ex.getMessage());
			//Ivy.log().error(msg);
			System.out.println(msg);
		}
		
		return result;
	}
	
}
