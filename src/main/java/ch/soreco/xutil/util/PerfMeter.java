package ch.soreco.xutil.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;


/** Simple class to gauge execution time of a process (indiv start and end point)
 * 
 * @author he 16.09.2019
 *
 */
public class PerfMeter {
	
	private static	String amountNoScalePattern = "#,##0";
	private static	String amountPattern = "#,##0.00";

	private static	Locale currLocale = new Locale("de", "CH");
	private static	DecimalFormatSymbols unusualSymbols = new DecimalFormatSymbols(currLocale);
	private static	DecimalFormat amountNoScaleFormatter = new DecimalFormat(amountNoScalePattern, unusualSymbols);
	private static	DecimalFormat amountFormatter = new DecimalFormat(amountPattern, unusualSymbols);
	
	long starttime;
	long endtime;
	BigDecimal diff = BigDecimal.ZERO;
	

	public PerfMeter() {
		// init start
		starttime = System.nanoTime();
		diff = BigDecimal.ZERO;
	}
	
	public void start() {
		starttime = System.nanoTime();
		diff = BigDecimal.ZERO;
	}
	
	public PerfMeter stop() {
		endtime = System.nanoTime();
		
		diff = calc();
		
		return this;
	}
	
	
	public BigDecimal getExecutionTime() {
		return diff;
	}
	
	public String getExecutionTimeFormatted() {
		String sTime = String.format("%1$s ms (%2$s min)", formatBigDecimal_AmountNoScale(diff), format_AmountNoScale_Minutes(diff));
		return sTime;
	}
	
	public String logExecutionTimeInfo(String processName) {
		
		String msg = String.format("EXECUTION TIME for %1$s : %2$s ms (%3$s min).", processName, formatBigDecimal_AmountNoScale(diff), format_AmountNoScale_Minutes(diff));
		
		ExLog.log("-->");
		ExLog.log(msg);
		ExLog.log(String.format("<--%n"));
		
		//ExLog.logPlain("-->");
		//ExLog.logPlain(msg);
		//ExLog.logPlain(String.format("<--%n"));
		
		//System.out.println("-->");
		//System.out.println(msg);
		//System.out.println(String.format("<--%n"));

		return msg;
	}
	
	
	private BigDecimal calc() {
		
		BigDecimal x_difference = new BigDecimal( (endtime - starttime)/1e6 );
		x_difference = x_difference.setScale(0, RoundingMode.HALF_UP);
		
		return x_difference;
	}
	
	
	/** Formatting a BigDecimal as Amount without Scale to String'
	*/
	private String formatBigDecimal_AmountNoScale(BigDecimal amount_BD) 
	{
		BigDecimal abd;

		if (amount_BD != null)
			{
				abd = amount_BD;			
			}
		else
			{
				abd = BigDecimal.ZERO;
			}
		
		String amount_ST = amountNoScaleFormatter.format(abd);
		
		return amount_ST;
	}
	
	/** Formatting a BigDecimal, convert from milliseconds to minutes 
	*/
	private String format_AmountNoScale_Minutes(BigDecimal amount_BD){
		BigDecimal min = BigDecimal.ZERO;
		BigDecimal sixty = new BigDecimal(60000);	// calc milliseconds to seconds /1000 and to minutes /60

		if (amount_BD != null){
			try{
				min = amount_BD.divide(sixty, 2, RoundingMode.HALF_UP);
				//min = min.setScale(2, RoundingMode.HALF_UP);
			}catch(ArithmeticException ex){
				ExLog.error(this, new Object(){}, "--> "+ex.getMessage()+String.format(" <--%n"));
			}
		}
		else{
			min = BigDecimal.ZERO;
		}
		String amount_ST = amountFormatter.format(min);
		return amount_ST;
	}

}
