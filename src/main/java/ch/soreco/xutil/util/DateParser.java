package ch.soreco.xutil.util;

/*
 * Copyright 1999,2004 The Apache Software Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

//import ch.ivyteam.ivy.environment.Ivy;
//import ch.ivyteam.ivy.scripting.objects.Date;
//import ch.ivyteam.ivy.scripting.objects.DateTime;
//import java.util.Date;
//import java.util.TimeZone;


/**
 * ISO 8601 date parsing utility.  Designed for parsing.
 * 
 * @author <a href="mailto:rheiniger@soreco.ch">R.Heiniger (may the 4th be with you...)</a>
 * @version DateParser.java, v 1.0 - 18.09.2014
 */
public class DateParser
{
	
//    public static Date parseIvyDate( String _input )
//    {
//    	java.util.Date d = null;
//    	Date ivyDate = null;
//        SimpleDateFormat df = new SimpleDateFormat( "yyyy-MM-dd" );
//        
//        try
//        {
//        	d = df.parse( _input );
//        }
//        catch(java.text.ParseException ex)
//        {
//			String msg = String.format("DateParser: parseIvyDate -> ParseException: Value '%1$s' seems not to be a date, check export data! '%2$s' ", _input, ex.getMessage());
//			//Ivy.log().warn(msg);
//			System.out.println(msg);
//        }
//        ivyDate = new Date(d);
//        
//        return ivyDate;
//    }
//    
//    public static DateTime parseIvyDateTime( String _input )
//    {
//    	java.util.Date d = null;
//    	DateTime ivyDate = null;
//        SimpleDateFormat df = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ss" );
//        
//        try
//        {
//        	d = df.parse( _input );
//        }
//        catch(java.text.ParseException ex)
//        {
//			String msg = String.format("DateParser: parseIvyDateTime -> ParseException: Value '%1$s' seems not to be a date/time, check export data! '%2$s' ", _input, ex.getMessage());
//			//Ivy.log().warn(msg);
//			System.out.println(msg);
//        }
//        ivyDate = new DateTime(d);
//        
//        return ivyDate;
//    }
    
    public static java.util.Date parseDate( String _input )
    {
    	java.util.Date d = null;
        SimpleDateFormat df = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ss" );
        
        try
        {
        	d = df.parse( _input );
        }
        catch(java.text.ParseException ex)
        {
			String msg = String.format("DateParser: parseDate -> ParseException: Value '%1$s' seems not to be a date, check export data! '%2$s' ", _input, ex.getMessage());
			//Ivy.log().warn(msg);
			System.out.println(msg);
        }
        
        return d;
    }
    
    public static GregorianCalendar parseGregorianCalendarDate( String _input )
    {
    	GregorianCalendar gc = null;
    	java.util.Date d = null;
        SimpleDateFormat df = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ss" );
        
        try
        {
        	d = df.parse( _input );
        	gc = new GregorianCalendar();
        	gc.setTime(d);
        }
        catch(java.text.ParseException ex)
        {
			String msg = String.format("DateParser: parseGregorianCalendarDate -> ParseException: Value '%1$s' seems not to be a date, check export data! '%2$s' ", _input, ex.getMessage());
			//Ivy.log().warn(msg);
			System.out.println(msg);
        }
        
        return gc;
    }
    
}
