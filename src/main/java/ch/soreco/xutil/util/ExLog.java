package ch.soreco.xutil.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class ExLog {
	
	public static enum ExSeverity {INFO, WARN, ERROR, FATAL};
	
	public static final String CLASSNAME = "ExcelExporter";
	public static final String METHODNAME = "unknown";
	
	
	public static void info(Object _class, Object _method, String _message) {
		ExLog.log(getClassName(_class), getMethod(_method), ExSeverity.INFO, _message);
	}
	
	public static void warn(Object _class, Object _method, String _message) {
		ExLog.log(getClassName(_class), getMethod(_method), ExSeverity.WARN, _message);
	}
	
	public static void error(Object _class, Object _method, String _message) {
		ExLog.log(getClassName(_class), getMethod(_method), ExSeverity.ERROR, _message);
	}
	
	public static void fatal(Object _class, Object _method, String _message) {
		ExLog.log(getClassName(_class), getMethod(_method), ExSeverity.FATAL, _message);
	}
	
	public static void info(String _message) {
		ExLog.log(CLASSNAME, METHODNAME, ExSeverity.INFO, _message);
	}
	
	public static void warn(String _message) {
		ExLog.log(CLASSNAME, METHODNAME, ExSeverity.WARN, _message);
	}
	
	public static void error(String _message) {
		ExLog.log(CLASSNAME, METHODNAME, ExSeverity.ERROR, _message);
	}
	
	public static void fatal(String _message) {
		ExLog.log(CLASSNAME, METHODNAME, ExSeverity.FATAL, _message);
	}

	
	private static void log(String _className, String _methodName, ExSeverity exSeverity, String _msg) {
		
		String msg = String.format("%1$s %2$s [%3$s]->%4$s(): %5$s", getTime(), exSeverity.toString(),_className, _methodName, _msg);
		System.out.println(msg);
	}
	
	public static void log(String _msg) {
		
		String msg = String.format("%1$s : %2$s", getTime(), _msg);
		System.out.println(msg);
	}
	
	public static void logPlain(String _msg) {
		
		//String msg = String.format("%1$s", _msg);
		System.out.println(_msg);
	}
	
	private static String getTime() {
		Calendar cal = GregorianCalendar.getInstance();
		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss,SSS");
		// 2020-01-29 08:44:03,975
		String now = sdfDate.format(cal.getTime());
		
		return now;
	}
	
	private static String getClassName(Object _class) {
		String _className = _class != null ? _class.getClass().getName() : CLASSNAME;
		return _className;
	}
	
	private static String getMethod(Object _method) {
		String _methodName = 	_method != null && 
								_method.getClass() != null && 
								_method.getClass().getEnclosingMethod() != null &&
								_method.getClass().getEnclosingMethod().getName() != null ? _method.getClass().getEnclosingMethod().getName() : METHODNAME;
		return _methodName;
	}
	
}
