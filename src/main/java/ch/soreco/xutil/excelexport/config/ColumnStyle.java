package ch.soreco.xutil.excelexport.config;

import lombok.Getter;
import lombok.Setter;

/** Class to hold all cell attributes for a column.
 *  The consumer of the excel export has to create/support an instance of this class for each column that he wants to customize.
 * 
 * @author he / 07.03.2022
 *
 */
public class ColumnStyle extends CellStyle {
	
	// *** column properties *** 
	/** position of the column, first column is 0 */
	@Getter @Setter
	protected int columnNumber = -1;
	
	/** data type, corresponds to class.getName() (e.g. for String: 'java.lang.String') */
	@Getter @Setter
	protected String dataType = String.class.getName();
	
	
	public ColumnStyle() {
		super();
	}

	/** Creates a ColumnStyle for the declared column
	 * @param columnNumber zero-based index of the column (first column is 0)
	 */
	public ColumnStyle(int columnNumber) {
		super();
		this.columnNumber = columnNumber;
	}
	
    /** Creates a ColumnStyle for the declared column and background color
     * @param columnNumber zero-based index of the column (first column is 0)
     * @param backgroundColorRgb RGB value in hex e.g. "#DCDCDC"
     */
    public ColumnStyle(int columnNumber, String backgroundColorRgb) {
		super();
		this.columnNumber = columnNumber;
		this.backgroundColorRgb = backgroundColorRgb;
	}

}
