package ch.soreco.xutil.excelexport.config;

import java.util.HashMap;

import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.DefaultIndexedColorMap;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;

/** Class that creates and holds the XSSFCellStyle objects for individual properties of all cells of a row
 * 
 * @author he / 14.03.2022
 */
public class CustomRowStyle extends RowStyle {

	HashMap<Integer,XSSFCellStyle> colMap = new HashMap<Integer,XSSFCellStyle>();
	

	/** Constructor CustomRowStyle
	 * @param _rowStyle - RowStyle object (usually delivered by the end user), contains the individual style
	 * @param _workbook - the workbook object of this excel export
	 * @param _baseCellStylesToClone - array of ColumnBaseInfo (contains base column style and data type)
	 */
	public CustomRowStyle(RowStyle _rowStyle, SXSSFWorkbook _workbook, ColumnBaseInfo[] _baseCellStylesToClone) {
		super(new String(_rowStyle.id));
		// clone style
		//this.id = new String(_rowStyle.id);
		this.backgroundColorRgb = new String(_rowStyle.backgroundColorRgb);
		this.fontColorRgb = new String(_rowStyle.fontColorRgb);
		this.bold = new Boolean(_rowStyle.bold);
		this.scale = _rowStyle.scale;
		
		//ExLog.info(this, new Object(){}, "CustomRowStyle: generateXSSFCellStyle -> Start generating row styles for "+this.id +" !");
		
		// generate a XSSFCellStyle based on the default style (and data type) for every column
		if( _baseCellStylesToClone != null ) {
			for( int i=0 ; i<_baseCellStylesToClone.length ; i++ ) {
			
				XSSFCellStyle cellStyle = generateXSSFCellStyle(_workbook, _baseCellStylesToClone[i]);
				if( cellStyle != null) {
					colMap.put(i+1, cellStyle);
				}
				else {
					colMap.put(i+1, _baseCellStylesToClone[i].getCellStyle());
				}
			}
		}
		//ExLog.info(this, new Object(){}, "CustomRowStyle: generateXSSFCellStyle -> Generating row styles done !");
		
	}
	
	/** Create a CustomRowStyle based on a already existing XSSFCellStyle.<br>
	 *  REMARKS: Use this constructor only to create a predefined row style (PredefinedRowStyle) like HEADER. This one uses the same data Type for every column (String)!<br>
	 *           Typically used to insert a repetitive header row.
	 * @param _rowStyleId - name of the predefined header row (a value of enum PredefinedRowStyle)
	 * @param _baseCellStyle - base style to use for all columns in this row
	 * @param _baseCellStylesToClone - array of base column styles
	 */
	public CustomRowStyle(String _rowStyleId, XSSFCellStyle _baseCellStyle, XSSFCellStyle[] _baseCellStylesToClone) {
		super(new String(_rowStyleId));
		// *** we do not set any attributes here, style is given by _baseCellStyle
		//this.backgroundColorRgb = new String(_rowStyle.backgroundColorRgb);
		//this.fontColorRgb = new String(_rowStyle.fontColorRgb);
		//this.bold = new Boolean(_rowStyle.bold);
		//this.scale = _rowStyle.scale;
		
		
		if( _baseCellStyle != null ) {
			for( int i=0 ; i<_baseCellStylesToClone.length ; i++ ) {
			
				XSSFCellStyle cellStyle = _baseCellStyle;
				if( cellStyle != null) {
					colMap.put(i+1, cellStyle);
				}
				else {
					colMap.put(i+1, _baseCellStylesToClone[i]);
				}
			}
		}
	}
	

	private XSSFCellStyle generateXSSFCellStyle(SXSSFWorkbook workbook, ColumnBaseInfo _baseCellStyleToClone) {
		
		XSSFCellStyle cellStyle = null;
		
		if( workbook != null  ) {
			cellStyle = (XSSFCellStyle)workbook.createCellStyle();
			cellStyle.cloneStyleFrom(_baseCellStyleToClone.getCellStyle());
			// set background color
			cellStyle.setFillForegroundColor( new XSSFColor((this.getBackgroundColorAsByteArray()), new DefaultIndexedColorMap()) );
			//cellStyle.setFillForegroundColor(new XSSFColor( this.getBackgroundColor() ));		// deprecated method
			cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			// set font attributes
			setFontAttributes(workbook, cellStyle);
			
			// get the DataFormat instance of this workbook
			DataFormat format = workbook.createDataFormat();
			
//			short gaga = cellStyle.getDataFormat();
//			String pattern = format.getFormat(gaga);
//			ExLog.log("CustomRowStyle: generateXSSFCellStyle -> DataFormat: "+gaga+"|"+pattern);
			
			// set format for scale (if overwritten -> greater than -1 )
			if( scale >= 0 && (
					java.math.BigDecimal.class.getName().equalsIgnoreCase(_baseCellStyleToClone.getDataType()) ||
					java.lang.Double.class.getName().equalsIgnoreCase(_baseCellStyleToClone.getDataType()) ||
					java.lang.Float.class.getName().equalsIgnoreCase(_baseCellStyleToClone.getDataType()) ) ) {
				// set format for requested scale
				int fmt = format.getFormat(getPattern(scale));
				cellStyle.setDataFormat( fmt );
			}
			
		}
		else {
			cellStyle = null;
		}
		
		//ExLog.info(this, new Object(){}, "CustomRowStyle: generateXSSFCellStyle -> Generating ...");
		
		return cellStyle;
	}

	public XSSFCellStyle getCellStyle(int _columnNumber) {
		return colMap.get(_columnNumber);
	}

}
