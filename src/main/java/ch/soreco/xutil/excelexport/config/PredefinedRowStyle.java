package ch.soreco.xutil.excelexport.config;

import lombok.Getter;

/** PredefinedRowStyle: Use this predefined styles in your RowStyleIdMap, to get the id use PredefinedRowStyle.HEADER.getId()
 *  
 * @author he / 01.04.2022
 */
public enum PredefinedRowStyle {

	// enumeration PrintOptionMultiArea 
	/** HEADER: same style same as column header, typically only strings (use this style for repetitive header rows) */
	HEADER(1, "HEADER"),
	/** SEPARATOR: a blank style, typically only strings (use this style to insert blank rows) */
	SEPARATOR(2, "SEPARATOR");
	
	/** CellStyle id */
	@Getter
	private String id;
	/** surrogate/enumeration of this predefined style */
	@Getter
	private Integer surrogate;
	
	
	private PredefinedRowStyle(Integer _surrogate, String _id) {
		this.id = _id;
		this.surrogate = _surrogate;
	}


}
