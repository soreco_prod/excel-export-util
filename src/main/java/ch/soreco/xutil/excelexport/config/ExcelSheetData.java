package ch.soreco.xutil.excelexport.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ch.soreco.xutil.util.ExLog;
import lombok.Getter;
import lombok.Setter;

/** Class that holds all the necessary data, parameters and formatting for the export.<br><br>
 * 
 * <b>Mandatory attributes:</b><br>
 * <b>headers</b> - a list of strings with the header text. The list length must correspond to the column count of parameter matrix.<br>
 * <b>matrix</b> - a list (rows) of list of objects (columns) that contains the data to export.<br>
 * The data type of the objects determines the output format, so choose the right data type to get the correct format in the excel file:<br>
 * examples:<br>
 * Amounts - use BigDecimal with scale 2<br>
 * Date - use Date or java.util.Date to get a date with no time part<br>
 * Integer - use Integer for numbers with no decimal places<br><br>
 * <b>Optional attributes:</b><br>
 * <b>sheetName</b> - the name of the sheet. If null or empty String, "sheet" will be the name of the sheet.<br>
 * <b>title</b> - the title is inserted in the first row of the excel sheet. If the string is null or empty, no title will be generated and the header data start at first row.<br>
 * <b>subtitle</b> - a list of subtitles or any other information, every subtitle will be inserted in a own row after the title. If the list is null or empty, no rows will be generated.<br>
 * <b>convertNumericStrings</b> - if true all strings with a numeric value will be converted to a number (cell of type numeric, right alignment etc.)<br>
 * <b>templatePath</b> - full path or only file name to a excel template (depending on constructor used before), if null or empty String a no template is used (a new workbook will be created)<br>
 * <b>columnStyles</b> - List of ColumnStyle or null if no custom formatting is required/desired. ColumnStyle contains the information concerning background color, font color, bold font etc. <b>of a column</b>.<br>
 * <b>rowStyles</b> - List of RowStyle or null if no custom formatting is required/desired. RowStyle contains the information concerning background color, font color, bold font etc. <b>for a row type</b> (identified by id).<br>
 * <b>rowStyleIdMap</b> - Mapping of row number to RowStyle-Id. The assignment must contain all lines that are to be specially formatted.<br>
 * 
 * 
 * @author he / 29.03.2022
 * 
 */
public class ExcelSheetData {
	
	
	List<String> headers = new ArrayList<String>();
	@Getter
	private String[] heads;
	
	List<ArrayList<Object>> matrix = new ArrayList<ArrayList<Object>>();
	@Getter
	private Object[][] rows;
	
	/** provide a sheet name */
	@Getter
	@Setter
	private String sheetName = "";
	
	@Getter
	@Setter
	private String title = "";
	
	@Getter
	@Setter
	private String[] subtitle = null; 
	
	@Getter
	@Setter
	private boolean convertNumericStrings = false;
	
	@Getter
	@Setter
	private String templatePath = null;
	
	@Getter
	@Setter
	private List<ColumnStyle> columnStyles = null;
	
	@Getter
	@Setter
	private List<RowStyle> rowStyles = null;
	
	@Getter
	@Setter
	private Map<Integer,String> rowStyleIdMap = null;
	
	
	public ExcelSheetData(List<String> headers, List<ArrayList<Object>> matrix) {
		super();
		this.headers = headers;
		this.matrix = matrix;
		
		initHeadsAndRows();
		
	}


	/** Create a one-dimensional array for the heads and a two-dimensional array for the rows
	 */
	private void initHeadsAndRows() {
		
		int x = headers.size();
		int y = matrix.size();
		
		try 
		{
		    this.heads = new String[x];
		    for(int i = 0; i<x; i++) {
		    	try {
					if( headers.get(i) == null) {
						this.heads[i] = null;
					}
					else {
						this.heads[i] = headers.get(i);
					}
		    	}
				catch(NullPointerException ex) {
					this.heads[i]="";
				}
		    }
		
		    // create a Array[][] from the List
		    this.rows = new Object[y][x];
		    for(int i=0; i<y; i++) {	// get every row
		    	// inner List ( = record)
		    	for(int j=0; j<x; j++) {	// get every column
		    		this.rows[i][j] = ( matrix.get(i).get(j) );
		    	}
		    }
	    }
		catch (Exception e) 
		{
			ExLog.error(this, new Object(){}, "EXCEL EXPORT: exportListAsExcel -> Exception: "+e.getMessage());
			e.printStackTrace();
		}
	}

}
