package ch.soreco.xutil.excelexport.config;

import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.DefaultIndexedColorMap;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;

/** Class that creates and holds the XSSFCellStyle object for individual properties of a column cell
 * 
 * @author he / 07.03.2022
 */
public class CustomColumnStyle extends ColumnStyle {

	
	XSSFCellStyle cellStyle;

	public CustomColumnStyle(ColumnStyle _columnStyle, SXSSFWorkbook _workbook, ColumnBaseInfo _columnBaseInfo) {
		super();
		// clone style
		this.id = new String(_columnStyle.id);
		this.dataType = _columnBaseInfo.getDataType();
		this.columnNumber = _columnStyle.columnNumber;
		this.backgroundColorRgb = new String(_columnStyle.backgroundColorRgb);
		this.fontColorRgb = new String(_columnStyle.fontColorRgb);
		this.bold = new Boolean(_columnStyle.bold);
		this.scale = _columnStyle.scale;
		
		generateXSSFCellStyle(_workbook, _columnBaseInfo);
		
	}

	private void generateXSSFCellStyle(SXSSFWorkbook workbook, ColumnBaseInfo _baseCellStyleToClone) {
		
		if( workbook != null  ) {
			this.cellStyle = (XSSFCellStyle)workbook.createCellStyle();
			// clone the style from the base cell
			this.cellStyle.cloneStyleFrom(_baseCellStyleToClone.getCellStyle());
			// set background color
			this.cellStyle.setFillForegroundColor( new XSSFColor((this.getBackgroundColorAsByteArray()), new DefaultIndexedColorMap()) );
			//this.cellStyle.setFillForegroundColor(new XSSFColor( this.getBackgroundColor() ));		// deprecated method
			this.cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			// set font attributes
			setFontAttributes(workbook, this.cellStyle);
			
			// get the DataFormat instance of this workbook
			DataFormat format = workbook.createDataFormat();
			
//			short gaga = cellStyle.getDataFormat();
//			String pattern = format.getFormat(gaga);
//			ExLog.log("CustomColumnStyle: generateXSSFCellStyle -> DataFormat: "+gaga+"|"+pattern);
			
			// set format for scale (if overwritten -> greater than -1 )
			if( scale >= 0 && (
					java.math.BigDecimal.class.getName().equalsIgnoreCase(_baseCellStyleToClone.getDataType()) ||
					java.lang.Double.class.getName().equalsIgnoreCase(_baseCellStyleToClone.getDataType()) ||
					java.lang.Float.class.getName().equalsIgnoreCase(_baseCellStyleToClone.getDataType()) ) ) {
				// set format for requested scale
				int fmt = format.getFormat(getPattern(scale));
				cellStyle.setDataFormat( fmt );
			}
			
		}
		else {
			this.cellStyle = null;
		}
	
	}

	public XSSFCellStyle getCellStyle() {
		return cellStyle;
	}

	public void setCellStyle(XSSFCellStyle cellStyle) {
		this.cellStyle = cellStyle;
	}
	
//	private Color hex2Rgb(String colorStr) {
//	return new Color(
//		Integer.valueOf(colorStr.substring(1, 3), 16),
//		Integer.valueOf(colorStr.substring(3, 5), 16),
//		Integer.valueOf(colorStr.substring(5, 7), 16));
//	}

}
