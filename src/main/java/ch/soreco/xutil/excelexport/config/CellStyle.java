package ch.soreco.xutil.excelexport.config;

import java.awt.Color;

import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.DefaultIndexedColorMap;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;

/** Abstract Base class that hold the general attributes of a cell like background color, font color etc.
 * 
 * @author he / 07.03.2022
 */
public abstract class CellStyle {
	
	public static final String BACKGROUNDCOLOR_DEFAULT = "#ffffff";
	public static final String FONTCOLOR_DEFAULT = "#000000";
	
	// id (has to be unique)
	protected String id = "";
	
	// *** properties *** 
	// cell background color
	protected String backgroundColorRgb = "#ffffff";
	// font color
	protected String fontColorRgb = "#000000";
	// font normal/bold
	protected Boolean bold = false;
	// scale of BigDecimal (if the cell contains a BigDecimal)
	protected int scale = -1;
	
	
	//private ArrayList<Integer> rowList = new ArrayList<Integer>();
	
	
	protected CellStyle() {
		super();
	}

    
	protected CellStyle(String id) {
		super();
		this.id = id;
	}
		
	protected CellStyle(String id, String backgroundColorRgb) {
		super();
		this.id = id;
		this.backgroundColorRgb = backgroundColorRgb;
	}

	protected CellStyle(String id, String backgroundColorRgb, Boolean bold) {
		super();
		this.id = id;
		this.backgroundColorRgb = backgroundColorRgb;
		this.bold = bold;
	}

	protected CellStyle(String id, String backgroundColorRgb, Boolean bold, String fontColorRgb) {
		super();
		this.id = id;
		this.backgroundColorRgb = backgroundColorRgb;
		this.fontColorRgb = fontColorRgb;
		this.bold = bold;
	}


	protected void setFontAttributes(SXSSFWorkbook workbook, XSSFCellStyle cellStyle) {
		
		// set font attributes
		if( this.bold || (this.fontColorRgb != null && !this.fontColorRgb.isEmpty() && !this.fontColorRgb.equalsIgnoreCase(FONTCOLOR_DEFAULT) ) ) {
			// create XSSFFont object
			XSSFFont font = (XSSFFont)workbook.createFont(); 	// create a new font instead of cellStyle.getFont()! Otherwise you change the font of all other cells that reference this font.

			// set font bold
			if( this.bold ) {
				font.setBold(bold);
			}

			// set font color
			if( this.fontColorRgb != null && !this.fontColorRgb.isEmpty() && !this.fontColorRgb.equalsIgnoreCase(FONTCOLOR_DEFAULT) ) {
				// create color
				XSSFColor fontColor = new XSSFColor((this.getFontColorAsByteArray()), new DefaultIndexedColorMap());
				font.setColor(fontColor);
				//font.setColor(IndexedColors.WHITE.index);
			}
			
			cellStyle.setFont(font);
		}
		
	}


	public Color getBackgroundColor() {
		// hex2Rgb '#F0D4A0'
        return new Color(
                Integer.valueOf(backgroundColorRgb.substring(1, 3), 16),
                Integer.valueOf(backgroundColorRgb.substring(3, 5), 16),
                Integer.valueOf(backgroundColorRgb.substring(5, 7), 16));
    }
	
	/** get background color as byte array 
	 * @return color as byte array (converted from a string like '#F0D4A0')
	 */
	public byte[] getBackgroundColorAsByteArray() {
		byte[] rgb = new byte[3];
		//int z = Integer.valueOf(backgroundColorRgb.substring(1, 3), 16);
		rgb[0] = Integer.valueOf(backgroundColorRgb.substring(1, 3), 16).byteValue(); // red
		rgb[1] = Integer.valueOf(backgroundColorRgb.substring(3, 5), 16).byteValue(); // green
		rgb[2] = Integer.valueOf(backgroundColorRgb.substring(5, 7), 16).byteValue(); // blue
		
        return rgb;
	}
	
	public Color getFontColor() {
		// hex2Rgb '#F0D4A0'
        return new Color(
                Integer.valueOf(fontColorRgb.substring(1, 3), 16),
                Integer.valueOf(fontColorRgb.substring(3, 5), 16),
                Integer.valueOf(fontColorRgb.substring(5, 7), 16));
    }
	
	/** get font color as byte array 
	 * @return color as byte array (converted from a string like '#F0D4A0')
	 */
	public byte[] getFontColorAsByteArray() {
		byte[] rgb = new byte[3];
		rgb[0] = Integer.valueOf(fontColorRgb.substring(1, 3), 16).byteValue(); // red
		rgb[1] = Integer.valueOf(fontColorRgb.substring(3, 5), 16).byteValue(); // green
		rgb[2] = Integer.valueOf(fontColorRgb.substring(5, 7), 16).byteValue(); // blue
		
        return rgb;
	}
	
	protected String getPattern(int decimalPlaces)
	{
		String result = "#,##0.00";

		if (decimalPlaces == 0) {
			result = "#,##0";
		} else if (decimalPlaces == 1) {
			result = "#,##0.0";
		} else if (decimalPlaces == 2) {
			result = "#,##0.00";
		} else if (decimalPlaces == 3) {
			result = "#,##0.000";
		} else if (decimalPlaces == 4) {
			result = "#,##0.0000";
		} else if (decimalPlaces == 5) {
			result = "#,##0.00000";
		} else if (decimalPlaces == 6) {
			result = "#,##0.000000";
		} else if (decimalPlaces == 7) {
			result = "#,##0.0000000";
		} else if (decimalPlaces == 8) {
			result = "#,##0.00000000";
		} else if (decimalPlaces >= 9) {
			result = "#,##0.000000000";
		} else {
			result = "#,##0.00";
		}
		return result;
	}


	public String getBackgroundColorRgb() {
		return backgroundColorRgb;
	}

	public void setBackgroundColorRgb(String backgroundColorRgb) {
		this.backgroundColorRgb = backgroundColorRgb;
	}

	public String getFontColorRgb() {
		return fontColorRgb;
	}

	public void setFontColorRgb(String fontColorRgb) {
		this.fontColorRgb = fontColorRgb;
	}

	public Boolean getBold() {
		return bold;
	}

	public void setBold(Boolean bold) {
		this.bold = bold;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getScale() {
		return scale;
	}

	public void setScale(int scale) {
		this.scale = scale;
	}

}
