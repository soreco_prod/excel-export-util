package ch.soreco.xutil.excelexport.config;

/** Class to hold all cell attributes for a row.
 *  The consumer of the excel export has to create/support an instance of this class for each row type that he wants to customize (e.g. key figures/indicators, subtotal, total etc.).
 * 
 * @author he / 14.03.2022
 *
 */
public class RowStyle extends CellStyle {
	
	// *** row properties *** 
	// nothing so far ...
	
	// row styles must have an id (no default constructor)
//	protected RowStyle() {
//		super();
//	}

	/** Creates a RowStyle with the declared id
	 * @param _id id/name of this style (used for mapping row number to row style id)
	 */
	public RowStyle(String _id) {
		super(_id);
	}
	
    /** Creates a RowStyle with the declared id and background color
     * @param _id id/name of this style (used for mapping row number to row style id)
     * @param backgroundColorRgb RGB value in hex e.g. "#DCDCDC"
     */
    public RowStyle(String _id, String backgroundColorRgb) {
    	super(_id);
		this.backgroundColorRgb = backgroundColorRgb;
	}


}
