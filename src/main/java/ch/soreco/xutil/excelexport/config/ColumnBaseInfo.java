package ch.soreco.xutil.excelexport.config;

import org.apache.poi.xssf.usermodel.XSSFCellStyle;

import lombok.Getter;
import lombok.Setter;

/** Class to hold the original determined data type and the corresponding XSSFCellStyle for a column.
 *  REMARKS: The data type is usually determined based on the first data line.
 * 
 * @author he / 01.04.2022
 */
@Getter
@Setter
public class ColumnBaseInfo {
	
	private XSSFCellStyle cellStyle;
	
	private String dataType = String.class.getName();

	public ColumnBaseInfo(XSSFCellStyle cellStyle, String dataType) {
		super();
		this.cellStyle = cellStyle;
		this.dataType = dataType;
	}
	

}
