package ch.soreco.xutil.excelexport.config;

import java.awt.Color;
import java.util.ArrayList;

public class RowStyleFirstImpl {
	
	private String name = "";
	
	private String backgroundColorRgb = "#ffffff";
	
	private ArrayList<Integer> rowList = new ArrayList<Integer>();
	
	
	public RowStyleFirstImpl() {
		super();
	}

	public RowStyleFirstImpl(String name) {
		super();
		this.name = name;
	}

	
    public Color getBackgroundColor() {
        return new Color(
                Integer.valueOf(backgroundColorRgb.substring(1, 3), 16),
                Integer.valueOf(backgroundColorRgb.substring(3, 5), 16),
                Integer.valueOf(backgroundColorRgb.substring(5, 7), 16));
    }
	
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBackgroundColorRgb() {
		return backgroundColorRgb;
	}

	public void setBackgroundColorRgb(String backgroundColorRgb) {
		this.backgroundColorRgb = backgroundColorRgb;
	}

	public ArrayList<Integer> getRowList() {
		return rowList;
	}

	public void setRowList(ArrayList<Integer> rowList) {
		this.rowList = rowList;
	}


}
