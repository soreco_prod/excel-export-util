package ch.soreco.xutil.excelexport;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.regex.Pattern;
import java.math.BigDecimal;

import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFCreationHelper;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRelation;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.ss.usermodel.BorderStyle;
//import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Footer;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;

import ch.soreco.xutil.filemanager.FileHandler;
import ch.soreco.xutil.util.Base64Encoder;
import ch.soreco.xutil.util.ExLog;
import ch.soreco.xutil.util.GuidGenerator;
import ch.soreco.xutil.util.PerfMeter;
import ch.soreco.xutil.excelexport.config.ColumnBaseInfo;
import ch.soreco.xutil.excelexport.config.ColumnStyle;
import ch.soreco.xutil.excelexport.config.CustomColumnStyle;
import ch.soreco.xutil.excelexport.config.CustomRowStyle;
import ch.soreco.xutil.excelexport.config.ExcelSheetData;
import ch.soreco.xutil.excelexport.config.PredefinedRowStyle;
import ch.soreco.xutil.excelexport.config.RowStyle;


//import com.ulcjava.base.application.table.ULCTableColumn;

/** This class enables a simple export of data into an Excel file. In addition, the formatting of columns and rows is possible, 
 *  whereby the row formatting overwrites the column specifications (row has priority). <br><br>
 *  NOTE: Use a new instance of this class for each export (due to generation of the filename). 
 * 
 * @author SORECO AG/HE R.Heiniger
 * @version 
 * 1.0   - 06.04.2010/he -- First version (based on a rudimentary version of ec)<br>
 * 1.1   - 06.04.2010/he -- Improvements: proper processing of numeric values(!), formatting of different data types, layout of header and values (cell styles)<br>
 * 1.2   - 19.09.2011/he -- Improvements: style processing for large sheets, layout (number formatting, title and subtitle etc.)<br>
 * 1.3   - 01.11.2011/he -- Improvements: new public function exportRecordsetAsExcel(), <br>
 * 										- new public helper function removeColumns(), <br>
 * 										- new private helper function writeWbFile(), <br>
 * 										- clean up log messages <br>
 * 1.4   - 19.08.2014/he -- Improvements: Support of templates (.xlt) as base-file,  to support (pre-)formatting and macros! <br>
 * 1.5   - 20.08.2014/he -- Improvements: Converting to XSSF Model --{@literal >} Excel 2007 OOXML (.xlsx) file format <br>
 * 1.6   - 14.02.2018/he -- Improvements: New Constructors with arguments _pathExcelFile and _basePathOfTemplates <br>
 * 1.7   - 29.01.2020/he -- Improvements: SXSSF variant for exporting big amount of records (into very large spreadsheet), Logging with ExLog (formatted System.out) <br>
 * 1.8   - 15.03.2022/he -- Improvements: SXSSF with new formatting possibilities: Columns (ColumnStyle), Rows (RowStyle) <br>
 * 1.8.1 - 30.03.2022/he -- Improvements: SXSSF with support of multiple sheets: new method exportListAsExcel(List&lt;ExcelSheetData&gt; excelSheetDataList) <br>
 * 1.8.3 - 05.04.2022/he -- Improvements: SXSSF with support of decimal places per column or row, class PredefinedRowStyle for repetitive header row or separator row <br>
 * 
 */
public class ExcelExporterSXSSF 
{
	private String fileName = "";
	private String outputPath;
	private File eeFile = null;
	private String excelExportTemplatePath = "";

	private static final String excelExportTempPath = "C:\\temp\\";			// Temp folder if not defined in System.getProperty("java.io.tmpdir")
	private static final String prefixPath = "EXCEL_EXPORT";				// subfolder in temp path for export file creation
	
	
	/** Constructor
	 *  Excel export file will be created in default temp path
	 */
	public ExcelExporterSXSSF()
	{
		outputPath = System.getProperty("java.io.tmpdir");
		if( outputPath == null || outputPath.isEmpty() )
		{
			outputPath = new String( excelExportTempPath );
		}
		// prepare and create path (if not already exists)
		outputPath = FileHandler.formatPathWithEndSeparator(outputPath)+prefixPath;
		outputPath = FileHandler.formatPathWithEndSeparator(outputPath, true);
		
		eeFile = this.createFile(true);
	}
	
	
	/** Constructor
	 * @param _pathExcelFile target path to excel export file (without file name, will be generated automatically)
	 */
	public ExcelExporterSXSSF(String _pathExcelFile)
	{
		outputPath = new String(_pathExcelFile);
		if( outputPath == null || outputPath.isEmpty() )
		{
			throw new IllegalArgumentException("Path argument may not be empty!");
		}
		// prepare and create path (if not already exists)
		outputPath = FileHandler.formatPathWithEndSeparator(outputPath, true);
		
		eeFile = this.createFile(true);
	}
	
	
	/** Constructor
	 * @param _pathExcelFile target path to excel export file (without file name, will be generated automatically)
	 * @param _basePathOfTemplates base path of templates files like .xltx (e.g. C:\\myTemplates\)
	 */
	public ExcelExporterSXSSF(String _pathExcelFile, String _basePathOfTemplates)
	{
		outputPath = new String(_pathExcelFile);
		if( outputPath == null || outputPath.isEmpty() )
		{
			throw new IllegalArgumentException("Path argument may not be empty!");
		}
		// prepare and create path (if not already exists)
		outputPath = FileHandler.formatPathWithEndSeparator(outputPath, true);
		
		// set path of templates, prepare and create path (if not already exists)
		excelExportTemplatePath = new String(_basePathOfTemplates);
		excelExportTemplatePath = FileHandler.formatPathWithEndSeparator(excelExportTemplatePath, true);
		
		eeFile = this.createFile(true);
	}
	
//	/** Constructor
//	 * @param _fileName - the String name of the excel File, extension will changes automatically to *.xlsx, if the string is empty an unique name will be generated.<br>
//	 * if null or empty String, a unique name will be generated (like Excel_Export_<UniqueID>).
//	 */
//	public ExcelExporterXSSF(String _fileName)
//	{
//		fileName = _fileName;
//		outputPath = "";
//		ivyFile = this.createFile(false);
//	}
	
//	/** Constructor
//	 * @param _outputPath  - the String name of the path on the server where the excel file has to be saved<br>
//	 * if null or empty String, "excelExport" will be the name of the output path. output path of excel file without file name
//	 * @param _fileName file name, extension will changes automatically to *.xlsx, if the string is empty an unique name will be generated.
//	 */
//	public ExcelExporterXSSF(String _outputPath, String _fileName)
//	{
//		fileName = _fileName;
//		outputPath = _outputPath;
//		ivyFile = this.createFile(true);
//	}
	
	
	private File createFile(boolean bUsePath)
	{
		eeFile = null;
		
		try
		{
			//CHECK AND INITIALIZE THE ARGUMENTS
			//if(outputPath==null || outputPath.trim().equalsIgnoreCase("")) outputPath="Excel_Export";
			if(fileName==null || fileName.trim().equalsIgnoreCase("")) fileName="ExcelExp_"+GuidGenerator.generateID();
			//if(sheetName==null || sheetName.trim().equalsIgnoreCase("")) sheetName="sheet";
			
			if( bUsePath )
			{
				// use the path the caller has given
				fileName = FileHandler.formatPathWithEndSeparator(outputPath, false)+FileHandler.getFileNameWithoutExt(fileName)+".xlsx";
			}
			else
			{
				// generate a path automatically (prefixPath + UserID)
				outputPath = FileHandler.formatPathWithEndSeparator(prefixPath, false);
				//outputPath += FileHandler.formatPathWithEndSeparator(/*Ivy.session().getSessionUserName()*/"", false);
				fileName = FileHandler.formatPathWithEndSeparator(outputPath, false)+FileHandler.getFileNameWithoutExt(fileName)+".xlsx";
			}
			
			eeFile = new File(fileName); 
			eeFile.createNewFile();
		
		} 
	    catch (FileNotFoundException e) 
		{
	    	//ExLog.info(_className, _methodName, _message);
	    	ExLog.error(this, new Object(){}, "EXCEL EXPORT: createFile -> FileNotFoundException: "+e.getMessage());
			e.printStackTrace();
			eeFile = null;
		} 
	    catch (IOException e) 
		{
	    	ExLog.error(this, new Object(){}, "EXCEL EXPORT: createFile -> IOException: "+e.getMessage());
			e.printStackTrace();
			eeFile = null;
		}
		catch (Exception e) 
		{
			ExLog.error(this, new Object(){}, "EXCEL EXPORT: createFile -> Exception: "+e.getMessage());
			e.printStackTrace();
			eeFile = null;
		}
		
	    return eeFile;
	}
	
	/**
	 * Exports a two dimensional List (matrix) as Excel file (this version returns the excel file as a base64 encoded string!)
	 * @param headers - a list of strings with the header text. The list length must correspond to the column count of parameter matrix
	 * @param matrix - a list (rows) of list of objects (columns) that contains the data to export.<br>
	 * The data type of the objects determines the output format, so choose the right data type to get the correct format in the excel file:<br>
	 * examples:<br>
	 * Amounts - use BigDecimal with scale 2<br>
	 * Date - use Date or java.util.Date to get a date with no time part<br>
	 * Integer - use Integer for numbers with no decimal places<br>
	 * @param sheetName - the name of the sheet. If null or empty String, "sheet" will be the name of the sheet.
	 * @param title - the title is inserted in the first row of the excel sheet. If the string is null or empty, no title will be generated and the header data start at first row.
	 * @param subtitle - a list of subtitles or any other information, every subtitle will be inserted in a own row after the title. If the list is null or empty, no rows will be generated.
	 * @param convertNumericStrings: if true all strings with a numeric value will be converted to a number (cell of type numeric, right alignment etc.)
	 * @param templatePath - full path or only file name to a excel template (depending on constructor used before), if null or empty String a no template is used (a new workbook will be created)
	 * @return String - the excel file as a base64 encoded string
	 * 
	 * @deprecated use {@link ExcelExporterSXSSF#exportListAsExcelBase64(List, List, String, String, String[], boolean, String, List, List, Map)} instead.  
	 */
	@Deprecated
	public String exportListAsExcelBase64(List<String> headers,List<ArrayList<Object>> matrix, String sheetName, String title, String[] subtitle, boolean convertNumericStrings, String templatePath)
	{
		return exportListAsExcelBase64(headers, matrix, sheetName, title, subtitle, convertNumericStrings, templatePath, null, null, null);
	}
	
	/**
	 * Exports a two dimensional List (matrix) as Excel file (this version returns the excel file as a base64 encoded string!)
	 * @param headers - a list of strings with the header text. The list length must correspond to the column count of parameter matrix
	 * @param matrix - a list (rows) of list of objects (columns) that contains the data to export.<br>
	 * The data type of the objects determines the output format, so choose the right data type to get the correct format in the excel file:<br>
	 * examples:<br>
	 * Amounts - use BigDecimal with scale 2<br>
	 * Date - use Date or java.util.Date to get a date with no time part<br>
	 * Integer - use Integer for numbers with no decimal places<br>
	 * @param sheetName - the name of the sheet. If null or empty String, "sheet" will be the name of the sheet.
	 * @param title - the title is inserted in the first row of the excel sheet. If the string is null or empty, no title will be generated and the header data start at first row.
	 * @param subtitle - a list of subtitles or any other information, every subtitle will be inserted in a own row after the title. If the list is null or empty, no rows will be generated.
	 * @param convertNumericStrings: if true all strings with a numeric value will be converted to a number (cell of type numeric, right alignment etc.)
	 * @param templatePath - full path or only file name to a excel template (depending on constructor used before), if null or empty String a no template is used (a new workbook will be created)
	 * @param columnStyles - List of ColumnStyle or null if no custom formatting is required/desired. ColumnStyle contains the information concerning background color, font color, bold font etc. <b>of a column</b>.
	 * @param rowStyles - List of RowStyle or null if no custom formatting is required/desired. RowStyle contains the information concerning background color, font color, bold font etc. <b>for a row type</b> (identified by id).
	 * @param rowStyleIdMap - Mapping of row number to RowStyle-Id. The assignment must contain all lines that are to be specially formatted.
	 * @return String - the excel file as a base64 encoded string
	 */
	public String exportListAsExcelBase64(	List<String> headers,List<ArrayList<Object>> matrix, String sheetName, String title, String[] subtitle, boolean convertNumericStrings, String templatePath,
											List<ColumnStyle> columnStyles,
											List<RowStyle> rowStyles,
											Map<Integer,String> rowStyleIdMap)
	{
		String base64 = null;
		File excelFile = null;
		
		try
		{
			excelFile = exportListAsExcel(headers, matrix, sheetName, title, subtitle, convertNumericStrings, templatePath, columnStyles, rowStyles, rowStyleIdMap);
			
			if( excelFile != null )
			{
				// create base64 string
				base64 = Base64Encoder.encodeBase64(excelFile);
			}
		}
		catch(Exception ex)
		{
			throw ex;
		}
		finally 
		{
			// cleanup file (we only return the base64 string)
			if( excelFile != null )
			{
				if( !FileHandler.deleteFile(excelFile.getPath()) )
				{
					String msg = String.format("EXCEL EXPORT: exportListAsExcelBase64 ->  Can't cleanup, delete of file '%1$s' failed! ", excelFile.getPath() );
					ExLog.info(this, new Object(){}, msg);
				}
			}
		}
		
		return base64;
	}
	
	
	/**
	 * Exports a two dimensional List (matrix) as Excel file
	 * @param headers - a list of strings with the header text. The list length must correspond to the column count of parameter matrix
	 * @param matrix - a list (rows) of list of objects (columns) that contains the data to export.<br>
	 * The data type of the objects determines the output format, so choose the right data type to get the correct format in the excel file:<br>
	 * examples:<br>
	 * Amounts - use BigDecimal with scale 2<br>
	 * Date - use Date or java.util.Date to get a date with no time part<br>
	 * Integer - use Integer for numbers with no decimal places<br>
	 * @param sheetName - the name of the sheet. If null or empty String, "sheet" will be the name of the sheet.
	 * @param title - the title is inserted in the first row of the excel sheet. If the string is null or empty, no title will be generated and the header data start at first row.
	 * @param subtitle - a list of subtitles or any other information, every subtitle will be inserted in a own row after the title. If the list is null or empty, no rows will be generated.
	 * @param convertNumericStrings: if true all strings with a numeric value will be converted to a number (cell of type numeric, right alignment etc.)
	 * @param templatePath - full path or only file name to a excel template (depending on constructor used before), if null or empty String a no template is used (a new workbook will be created)
	 * @return File - the excel java.io.File made with this export.
	 * 
	 * @deprecated use {@link ExcelExporterSXSSF#exportListAsExcel(List, List, String, String, String[], boolean, String, List, List, Map)} instead.  
	 */
	@Deprecated
	public File exportListAsExcel(List<String> headers,List<ArrayList<Object>> matrix, String sheetName, String title, String[] subtitle, boolean convertNumericStrings, String templatePath)
	{
		return exportListAsExcel(headers, matrix, sheetName, title, subtitle, convertNumericStrings, templatePath, null, null, null);
	}
	
	/**
	 * Exports a two dimensional List (matrix) as Excel file
	 * @param headers - a list of strings with the header text. The list length must correspond to the column count of parameter matrix
	 * @param matrix - a list (rows) of list of objects (columns) that contains the data to export.<br>
	 * The data type of the objects determines the output format, so choose the right data type to get the correct format in the excel file:<br>
	 * examples:<br>
	 * Amounts - use BigDecimal with scale 2<br>
	 * Date - use Date or java.util.Date to get a date with no time part<br>
	 * Integer - use Integer for numbers with no decimal places<br>
	 * @param sheetName - the name of the sheet. If null or empty String, "sheet" will be the name of the sheet.
	 * @param title - the title is inserted in the first row of the excel sheet. If the string is null or empty, no title will be generated and the header data start at first row.
	 * @param subtitle - a list of subtitles or any other information, every subtitle will be inserted in a own row after the title. If the list is null or empty, no rows will be generated.
	 * @param convertNumericStrings: if true all strings with a numeric value will be converted to a number (cell of type numeric, right alignment etc.)
	 * @param templatePath - full path or only file name to a excel template (depending on constructor used before), if null or empty String a no template is used (a new workbook will be created)
	 * @param columnStyles - List of ColumnStyle or null if no custom formatting is required/desired. ColumnStyle contains the information concerning background color, font color, bold font etc. <b>of a column</b>.
	 * @param rowStyles - List of RowStyle or null if no custom formatting is required/desired. RowStyle contains the information concerning background color, font color, bold font etc. <b>for a row type</b> (identified by id).
	 * @param rowStyleIdMap - Mapping of row number to RowStyle-Id. The assignment must contain all lines that are to be specially formatted.
	 * @return File - the excel java.io.File made with this export.
	 */
	public File exportListAsExcel(	List<String> headers,List<ArrayList<Object>> matrix, String sheetName, String title, String[] subtitle, boolean convertNumericStrings, String templatePath,
									List<ColumnStyle> columnStyles,
									List<RowStyle> rowStyles,
									Map<Integer,String> rowStyleIdMap)
	{
		
		File excelFile = null;
    	List<ExcelSheetData> excelSheetDataList = new ArrayList<ExcelSheetData>();
		
	    try 
	    {
	    	// create a ExcelSheetData object (to ensure the same type of processing as with multiple sheets)
	    	ExcelSheetData esd = new ExcelSheetData(headers, matrix);
	    	esd.setSheetName(sheetName);
	    	esd.setTitle(title);
	    	esd.setSubtitle(subtitle);
	    	esd.setConvertNumericStrings(convertNumericStrings);
	    	esd.setColumnStyles(columnStyles);
	    	esd.setRowStyles(rowStyles);
	    	esd.setRowStyleIdMap(rowStyleIdMap);
	    	
	    	excelSheetDataList.add(esd);
	    	
		    // call standard export processing
	    	excelFile = this.exportListAsExcel(excelSheetDataList, templatePath);

	    }
		catch (Exception e) 
		{
			ExLog.error(this, new Object(){}, "EXCEL EXPORT: exportListAsExcel -> Exception: "+e.getMessage());
			e.printStackTrace();
		}
		return excelFile;
	}

	

	/** Exports data to an Excel file. For each worksheet, the data must be passed as an ExcelSheetData object.
	 * @param excelSheetDataList List of ExcelSheetData objects. see {@link ExcelSheetData}
	 * @param templatePath - full path or only file name to a excel template (depending on constructor used before), if null or empty String a no template is used (a new workbook will be created)
	 * @return File - the excel java.io.File made with this export.
	 */
	public File exportListAsExcel(List<ExcelSheetData> excelSheetDataList, String templatePath)
	{
		SXSSFWorkbook wbs = null;
		XSSFWorkbook wbx = null;
		
		// log output path
		ExLog.info(this, new Object(){}, "using following output path for export -> "+outputPath);
		// start PerfMeter to log execution time
		PerfMeter pm = new PerfMeter();
	    
	    try 
	    {
	    	if( excelSheetDataList != null && !excelSheetDataList.isEmpty() ) {
	    		
			    // create Workbook 
			    // String templatePath = "C:\\temp\\ExcelExport_Template\\TemplateBase4.xlt";
			    wbx = createWorkbook(templatePath);
			    
				if (wbx == null) {
					throw new NullPointerException("EXCEL EXPORT: generateWorkbook -> creation of new Workbook XSSF failed: Reference wb is null.");
				}
			    // create SXSSFWorkbook
			    wbs = new SXSSFWorkbook(wbx, 500);
	    		
	    		// generate sheet by sheet
		    	for( int i=0 ; i<excelSheetDataList.size() ; i++ ) {
		    		ExcelSheetData esd = excelSheetDataList.get(i);
		    		// generate workbook sheet
		    		this.generateWorkbook(wbs, esd, i);
		    	}
	    	}
		    
		    // write all generated workbook data to file
		    eeFile = this.writeWbFile(wbs);
		    
	       // dispose of temporary files backing this workbook on disk
	       wbs.dispose();
	       
	       // try to log (trying variants to look which one pulls through ...)
	       //String path = eeFile != null ? eeFile.getAbsolutePath() : "no file found ...?" ;
	       //Logger.getLogger("").info("--> root logger <-- Excel File SXSSF_MultiSheet: " + path+" !");
	       //Logger.getLogger("excel.export").info("--> excel.export <--Excel File SXSSF_MultiSheet: " + path+" !");
	       
	       // log performance
	       pm.stop().logExecutionTimeInfo("EXCEL EXPORT::exportListAsExcel()");
	    }
		catch (Exception e) 
		{
			//Ivy.log().error("EXCEL EXPORT: exportListAsExcel -> Exception: "+e.getMessage());
			ExLog.error(this, new Object(){}, "EXCEL EXPORT: exportListAsExcel -> Exception: "+e.getMessage());
			e.printStackTrace();
		}
		return eeFile;
	}


	private File writeWbFile(SXSSFWorkbook wb)
	{
		FileOutputStream fileOut;
		
		try
		{
			ExLog.info(this, new Object(){}, "EXCEL EXPORT: Start writeWbFile ... ");
			
		    fileOut = new FileOutputStream(eeFile.getAbsolutePath());
		    //Ivy.log().info("EXCEL EXPORT: writeWbFile -> Start writing excel export file: "+ (new DateTime().format()));
			wb.write(fileOut);
		    fileOut.close();
		    //Ivy.log().info("EXCEL EXPORT: writeWbFile -> Finished writing excel export file: "+ (new DateTime().format()));
		    ExLog.info(this, new Object(){}, "EXCEL EXPORT: End writeWbFile ! ");
		    
		    // change content type
		    changeContentType(eeFile.getAbsolutePath());
		    //Ivy.log().info("EXCEL EXPORT: writeWbFile -> Finished changing content type of excel export file: "+ (new DateTime().format()));
		    
		} 
		catch (FileNotFoundException e) 
		{
			//Ivy.log().error("EXCEL EXPORT: writeWbFile -> FileNotFoundException: "+e.getMessage());
			ExLog.error(this, new Object(){}, "EXCEL EXPORT: writeWbFile -> FileNotFoundException: "+e.getMessage());
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			//Ivy.log().error("EXCEL EXPORT: writeWbFile -> IOException: "+e.getMessage());
			ExLog.error(this, new Object(){}, "EXCEL EXPORT: writeWbFile -> IOException: "+e.getMessage());
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			//Ivy.log().error("EXCEL EXPORT: writeWbFile -> Exception: "+e.getMessage());
			ExLog.error(this, new Object(){}, "EXCEL EXPORT: writeWbFile -> Exception: "+e.getMessage());
			e.printStackTrace();
		}
		return eeFile;
	}
	
	private XSSFWorkbook createWorkbook(String templatePath)
	{
		XSSFWorkbook wb = null;
		java.io.File f = null;
		FileInputStream inputStream = null;
		
		try
		{
			if( templatePath != null && !templatePath.isEmpty() )
			{
				// check if template exists
				// 1. value in templatePath is already full path
				f = new java.io.File(templatePath);
				if( f.isFile() )
				{
				    //String templateFile = "C:\\temp\\ExcelExport_Template\\TemplateBase3.xlt";
					inputStream = new FileInputStream(new java.io.File(templatePath));
				    wb = new XSSFWorkbook(inputStream);
				    inputStream.close();
				}
				else
				{
					// 2. value in templatePath is only a file name (try with base path from Global Variables)
					String tPath;
					//tPath = FileHandler.formatPathWithEndSeparator(Ivy.var().get("xbase_excel_jsf_ExcelExportTemplatePath"), false)+templatePath;
					tPath = FileHandler.formatPathWithEndSeparator(excelExportTemplatePath, false)+templatePath;
					f = new java.io.File(tPath);
					if( f.isFile() )
					{
						//Ivy.log().info("EXCEL EXPORT: createWorkbook -> create Workbook from template 2 (Template found: "+tPath+" )!");
						inputStream = new FileInputStream(new java.io.File(tPath));
					    wb = new XSSFWorkbook(inputStream);
					    inputStream.close();
					}
					else
					{
						String msg = String.format("EXCEL EXPORT: createWorkbook -> Template FILE NOT FOUND neither in '%1$s' nor '%2$s'!", templatePath, tPath);
						ExLog.error(this, new Object(){}, msg);
					}
				}
			}
		}
		catch(SecurityException ex)
		{
			ExLog.error(this, new Object(){}, "EXCEL EXPORT: createWorkbook -> SecurityException: "+ex.getMessage());
			wb = null;
		}
		catch(FileNotFoundException ex)
		{
			ExLog.error(this, new Object(){}, "EXCEL EXPORT: createWorkbook -> FileNotFoundException: "+ex.getMessage());
			wb = null;
		}
		catch(Exception ex)
		{
			ExLog.error(this, new Object(){}, "EXCEL EXPORT: createWorkbook -> Exception: "+ex.getMessage());
			wb = null;
		}
		finally
		{
			if( inputStream != null )
			{
				try
				{
					inputStream.close();
				}
				catch(IOException ex)
				{
					ExLog.error(this, new Object(){}, "EXCEL EXPORT: createWorkbook -> Can't close inputStream - Exception: "+ex.getMessage());
				}
			}
		}

		if( wb == null )
		{
			// no valid template exists -> create a empty new workbook
			ExLog.info(this, new Object(){}, "EXCEL EXPORT: createWorkbook -> Create a new workbook (no template found)!");
			try
			{
				wb = new XSSFWorkbook();
			}
			catch(Exception ex)
			{
				ExLog.error(this, new Object(){}, "EXCEL EXPORT: createWorkbook -> Exception: "+ex.getMessage()+"/"+ex.getCause());
				wb = null;
			}
		}
		
		return wb;
	}
	
	private XSSFCellStyle getRowStyle(int _columnNumber, int _rowNumber, XSSFCellStyle[] aCS, Map<Integer,String> _rowStyleIdMap, Map<String,CustomRowStyle> _customRowStyleMap) {
		XSSFCellStyle result = null;
		
		// set default style
		result = aCS[_columnNumber];
		
		// check if a customized RowStyle exists for this row number
		if( _rowStyleIdMap != null &&_rowStyleIdMap.containsKey(_rowNumber) ) {
			// get id of the RowStyle
			String id = _rowStyleIdMap.get(_rowNumber);
			// get the CustomRowStyle for this id
			CustomRowStyle crs = _customRowStyleMap.get(id);
			if( crs != null ) {
				// get the column specific style of this cell (column)
				result = crs.getCellStyle(_columnNumber+1);
			}
		}

		return result;
	}
	

	/** creates and generates the worksheet according to excelSheetData object
	 * @param wbs - SXSSFWorkbook instance
	 * @param excelSheetData - ExcelSheetData object. see {@link ExcelSheetData}
	 * @param sheetNumber - zero-based index of sheet to create
	 * @return the generated XSSFWorkbook object, same instance as passed as param
	 */
	private SXSSFWorkbook generateWorkbook(SXSSFWorkbook wbs, ExcelSheetData excelSheetData, int sheetNumber)
	{
		SXSSFSheet sheet = null;
		
		String[] heads = excelSheetData.getHeads();
		Object[][] rows = excelSheetData.getRows();
		String sheetName = excelSheetData.getSheetName();
		String title = excelSheetData.getTitle();
		String[] subtitle = excelSheetData.getSubtitle();
		boolean convertNumericStrings = excelSheetData.isConvertNumericStrings();
		List<ColumnStyle> columnStyles = excelSheetData.getColumnStyles();
		List<RowStyle> rowStyles = excelSheetData.getRowStyles();
		Map<Integer,String> rowStyleIdMap = excelSheetData.getRowStyleIdMap();
		
		// Map for CustomRowStyles <ID,CustomRowStyle>
		Map<String,CustomRowStyle> customRowStyleMap = new HashMap<String,CustomRowStyle>();
		
		
		// ColumnStyles and RowStyles are delivered by consumer
		// create columnStyleMap for easy access to a style by column number
		Map<Integer,ColumnStyle> columnStyleMap = new HashMap<Integer,ColumnStyle>();
		if( columnStyles != null ) {
			for( ColumnStyle cs : columnStyles ) {
				columnStyleMap.put(cs.getColumnNumber(), cs);
			}
		}
		
		// create rowStyleMap for easy access to a row style by id
		Map<String,RowStyle> rowStyleMap = new HashMap<String,RowStyle>();
		if( rowStyles != null ) {
			for( RowStyle rs : rowStyles ) {
				rowStyleMap.put(rs.getId(), rs);
			}
		}
		
		
		try
		{
		    int x= heads.length;
		    int y = rows.length;
		    int nStartRow = 0;	// row where the data output should start (including header row)
		    
		    
		    // create/get sheet of workbook
			// Note that sheet name in Excel must not exceed 31 characters and must not contain any of the any of the following characters:
		    // 0x0000, 0x0003, colon (:), backslash (\), asterisk (*), question mark (?), forward slash (/), opening or closing square bracket ([])
			// for a safe way to create valid names, this utility replaces invalid characters with a space (' ') 
			String safeName = WorkbookUtil.createSafeSheetName(sheetName);
			try {
				// try to get requested sheet (only possible if the workbook based on a template)
				sheet = wbs.getSheetAt(sheetNumber); // throws java.lang.IllegalArgumentException if sheet not exists!
			} catch (Exception ex) {
				// sheet not found (pretty sure not a template)
				// do nothing further
			}

			if (sheet != null) {
				// get existing sheet (important if template with macros is used)
				wbs.setSheetName(wbs.getSheetIndex(sheet), safeName);
				//ExLog.info("EXCEL EXPORT: generateWorkbook -> Existing sheet found and renamed!");
			} else {
				// no sheet exists (e.g. if a new workbook was created -> no template is given )
				sheet = wbs.createSheet(safeName);
				// ExLog.info("EXCEL EXPORT: generateWorkbook -> New sheet created!");
			}
			
			
			// create DataFormat to format values (never do this in the main loop of cell creating!!)
		    DataFormat format = wbs.createDataFormat();
			SXSSFCreationHelper createHelper = (SXSSFCreationHelper)wbs.getCreationHelper(); 
			short formatDate = createHelper.createDataFormat().getFormat("dd.mm.yyyy");
			short formatTime = createHelper.createDataFormat().getFormat("h:mm:ss");
			short formatDateTime = createHelper.createDataFormat().getFormat("dd.mm.yyyy h:mm:ss");


			//--- MAIN-TITLE ----- Create a main title (if not empty)
			if( title != null && title.length() > 0 )
			{
				nStartRow = 2;
				XSSFCellStyle csTitle = (XSSFCellStyle)wbs.createCellStyle();
				csTitle.setAlignment(HorizontalAlignment.LEFT);
				csTitle.setVerticalAlignment(VerticalAlignment.BOTTOM);
				csTitle.setWrapText(false);
				Font tFont = wbs.createFont();
				//ExLog.info("TITLE FONT HEIGHT: "+ tFont.getFontHeightInPoints());
				tFont.setFontHeightInPoints((short)(tFont.getFontHeightInPoints()+2));
				tFont.setBold(true);
		        //tFont.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		        csTitle.setFont(tFont);
				
				SXSSFRow rowTitle = sheet.createRow((short)0);
				SXSSFCell tCell = rowTitle.createCell(0);
				tCell.setCellValue(title);
				tCell.setCellStyle(csTitle);
				sheet.addMergedRegion(new CellRangeAddress(
										0,				//first row (0-based)
										0,				//last row  (0-based)
										0,				//first column (0-based)
										heads.length-1	//last column  (0-based)
										));
			}
			
			//--- SUB-TITLE ----- Create subtitles (if not empty)
			if( subtitle != null && subtitle.length > 0 )
			{
				int startRowSubTitle = nStartRow;
				nStartRow += subtitle.length+1;	//set value for data rows
				XSSFCellStyle csSubTitle = (XSSFCellStyle)wbs.createCellStyle();
				csSubTitle.setAlignment(HorizontalAlignment.LEFT);
				csSubTitle.setVerticalAlignment(VerticalAlignment.BOTTOM);
				csSubTitle.setWrapText(false);
				Font stFont = wbs.createFont();
				//ExLog.info("TITLE FONT HEIGHT: "+ tFont.getFontHeightInPoints());
				stFont.setFontHeightInPoints((short)(stFont.getFontHeightInPoints()+0));	// don't change height for subtitles
				stFont.setBold(true);
				//stFont.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		        csSubTitle.setFont(stFont);
				
			    for(int i=0; i<subtitle.length;i++)
			    {
			    	//HSSFCell hCell = row.createCell(i);
			    	//hCell.setCellValue(new HSSFRichTextString(heads[i]));
			        //hCell.setCellStyle(csHeader);
			    
		        
					SXSSFRow rowSubTitle = sheet.createRow((short)(i+startRowSubTitle));
					SXSSFCell stCell = rowSubTitle.createCell(0);
					stCell.setCellValue(subtitle[i]);
					stCell.setCellStyle(csSubTitle);
					sheet.addMergedRegion(new CellRangeAddress(
							i+startRowSubTitle,				//first row (0-based)
							i+startRowSubTitle,				//last row  (0-based)
											0,				//first column (0-based)
											heads.length-1	//last column  (0-based)
											));
				}
			}
			
			//--- FOOTER ----- Create a footer with timestamp and page number info
			Footer footer = sheet.getFooter();
			// preserve existing footer (e.g. in a template)
			if( footer.getLeft().isEmpty() )
			{
			    footer.setLeft( "&\"Arial,Regular\"" + "&7 " + "&D &T" );
			}
			if( footer.getRight().isEmpty() )
			{
				footer.setRight( "&\"Arial,Regular\"" + "&7 " + "&P / &N" );
			}

			//--- HEADERS ----- make the titles row
			// header style
			XSSFCellStyle csHeader = (XSSFCellStyle)wbs.createCellStyle();
	    	csHeader.setAlignment(HorizontalAlignment.CENTER);
	    	csHeader.setVerticalAlignment(VerticalAlignment.CENTER);
	    	csHeader.setWrapText(false);
	    	csHeader.setFillPattern(FillPatternType.SOLID_FOREGROUND /*XSSFCellStyle.SOLID_FOREGROUND*/);
	    	csHeader.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
	        // Note - many cells are actually filled with a foreground fill, not a background fill !!!
	        //cs.setFillBackgroundColor(new HSSFColor.GREY_25_PERCENT().getIndex());
	    	csHeader.setBorderBottom(BorderStyle.THIN);
	    	csHeader.setBottomBorderColor(IndexedColors.BLACK.getIndex());
	    	csHeader.setBorderLeft(BorderStyle.THIN);
	    	csHeader.setLeftBorderColor(IndexedColors.BLACK.getIndex());
	    	csHeader.setBorderTop(BorderStyle.THIN);
	    	csHeader.setTopBorderColor(IndexedColors.BLACK.getIndex());
	    	csHeader.setBorderRight(BorderStyle.THIN);
	    	csHeader.setRightBorderColor(IndexedColors.BLACK.getIndex());
			Font font = wbs.createFont();
			font.setFontHeightInPoints((short)(font.getFontHeightInPoints()-1));
			font.setBold(true);
	        //font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
	        csHeader.setFont(font);
	        
			SXSSFRow row = sheet.createRow(nStartRow);
		    for(int i=0; i<heads.length;i++)
		    {
		    	SXSSFCell hCell = row.createCell(i);
		    	hCell.setCellValue(new XSSFRichTextString(heads[i]));
		        hCell.setCellStyle(csHeader);
		    }

		    
			//--- CREATE CELL STYLES ----- Create a cell styles for every column
		    XSSFCellStyle[] baseColStyles = new XSSFCellStyle[heads.length];
		    ColumnBaseInfo[] columnBaseInfos = new ColumnBaseInfo[heads.length];
		    XSSFCellStyle[] aCS = new XSSFCellStyle[heads.length];
			//ArrayList arrCellStyle = new ArrayList(heads.length);
			
			// create some basic cell styles to use for column cells 
			// Caution: You can define up to 4000 unique styles in a .xls workbook (so never create them in a loop!!)
		    XSSFCellStyle csCommonLeft = (XSSFCellStyle)wbs.createCellStyle();
	        csCommonLeft.setAlignment(HorizontalAlignment.LEFT);
	        csCommonLeft.setVerticalAlignment(VerticalAlignment.TOP);
	        csCommonLeft.setWrapText(false);
	        csCommonLeft.setBorderBottom(BorderStyle.THIN);
	        csCommonLeft.setBottomBorderColor(IndexedColors.GREY_50_PERCENT.getIndex());
	        csCommonLeft.setBorderLeft(BorderStyle.THIN);
	        csCommonLeft.setLeftBorderColor(IndexedColors.GREY_50_PERCENT.getIndex());
	        csCommonLeft.setBorderTop(BorderStyle.THIN);
	        csCommonLeft.setTopBorderColor(IndexedColors.GREY_50_PERCENT.getIndex());
	        csCommonLeft.setBorderRight(BorderStyle.THIN);
	        csCommonLeft.setRightBorderColor(IndexedColors.GREY_50_PERCENT.getIndex());
	        // cell style with center alignment
	        XSSFCellStyle csCommonCenter = (XSSFCellStyle)wbs.createCellStyle();
	        csCommonCenter.cloneStyleFrom(csCommonLeft);
	        csCommonCenter.setAlignment(HorizontalAlignment.CENTER);
	        // cell style with right alignment
	        XSSFCellStyle csCommonRight = (XSSFCellStyle)wbs.createCellStyle();
	        csCommonRight.cloneStyleFrom(csCommonLeft);
	        csCommonRight.setAlignment(HorizontalAlignment.RIGHT);
	        
	        // create a cell style for every column (basis are the data types in the first row)
	        //ExLog.info("EXCEL EXPORT: generateWorkbook -> START CREATE CELL STYLES: "+ (new DateTime().format()));
	    	for(int j=0; j<x; j++)	// get every column
	    	{ 
	    		//CellStyle csTemp = null;
	    		XSSFCellStyle csTemp = null;
	    	   	// get the data object in the first row to detect the data type
	    		Object obj = rows[0][j];
	    		// convert numeric strings to number if requested
				if( convertNumericStrings && obj instanceof String )
				{
					try 
					{
						obj = new Double(Double.parseDouble((String) obj.toString()));
					} 
					catch (NumberFormatException e) 
					{
						// Not a number --> Nothing to do
						//Ivy.log().info("NumberFormatException: "+ e.getMessage());
					}
					catch (Exception e) 
					{
						// Not a number --> Nothing to do
						//Ivy.log().warn("Parsing of Number failed: "+ e.getMessage());
						ExLog.warn(this, new Object(){}, "Parsing of Number failed: "+ e.getMessage());
					}
				}
	    		// --- INTEGER / BIG INTEGER-----
	    		if( obj instanceof java.lang.Integer || obj instanceof java.math.BigInteger )
	    		{
	    			// treat it like double
	    			csTemp = (XSSFCellStyle)wbs.createCellStyle();
	    			csTemp.cloneStyleFrom(csCommonRight);
	    			csTemp.setDataFormat(format.getFormat("#,##0"));
	    			aCS[j] = csTemp;
	    			columnBaseInfos[j] = new ColumnBaseInfo(csTemp, obj.getClass().getName());
	    		}
	    		// --- BIG DECIMAL -----
	    		else if( obj instanceof java.math.BigDecimal )
	    		{
	    			csTemp = (XSSFCellStyle)wbs.createCellStyle();
	    			csTemp.cloneStyleFrom(csCommonRight);
	    			BigDecimal bg =  (BigDecimal)obj;
	    			// set decimalPlaces
	    			// default: use the scale on the BigDecimal
	    			short numberFormat = getNumberFormat(wbs, bg.scale());
	    			if( columnStyleMap.containsKey(j) ) {
	    				// get it from ColumnStyle (if defined)
	    				ColumnStyle cs = columnStyleMap.get(j);
	    				numberFormat = getNumberFormat(wbs, cs.getScale());
	    			}
	    			csTemp.setDataFormat( numberFormat );
	    			aCS[j] = csTemp;
	    			columnBaseInfos[j] = new ColumnBaseInfo(csTemp, obj.getClass().getName());
	    		}
	    		// --- DOUBLE / FLOAT -----
	    		else if( obj instanceof java.lang.Double || obj instanceof java.lang.Float )
	    		{
	    			csTemp = (XSSFCellStyle)wbs.createCellStyle();
	    			csTemp.cloneStyleFrom(csCommonRight);
	    			// set decimalPlaces
	    			// default: set to 2 ("#,##0.00")
	    			short numberFormat = format.getFormat("#,##0.00");
	    			if( columnStyleMap.containsKey(j) ) {
	    				// get it from ColumnStyle (if defined)
	    				ColumnStyle cs = columnStyleMap.get(j);
	    				numberFormat = getNumberFormat(wbs, cs.getScale());
	    			}
	    			csTemp.setDataFormat(numberFormat);
	    			//csTemp.setDataFormat( format.getFormat("#,##0.00") );
	    			aCS[j] = csTemp;
	    			columnBaseInfos[j] = new ColumnBaseInfo(csTemp, obj.getClass().getName());
	    		}
	    		// --- NUMBER / ALL OTHER NUMERIC -----
	    		else if( obj instanceof java.lang.Number )
	    		{
	    			csTemp = (XSSFCellStyle)wbs.createCellStyle();
	    			csTemp.cloneStyleFrom(csCommonRight);
	    			//csTemp.setDataFormat(format.getFormat("#,##0.00"));
	    			aCS[j] = csTemp;
	    			columnBaseInfos[j] = new ColumnBaseInfo(csTemp, obj.getClass().getName());
	    		}
	    		// --- DATE ----- 
	    		else if( obj instanceof java.util.Date || 
	    				 obj instanceof GregorianCalendar )
	    		{
	    			GregorianCalendar cal;
	    			csTemp = (XSSFCellStyle)wbs.createCellStyle();
	    			csTemp.cloneStyleFrom(csCommonCenter);
	    				    			
	    			if( obj instanceof GregorianCalendar )
	    			{
	    				cal = (GregorianCalendar)obj;
	    			}
	    			else
	    			{
		    			// java.util.Date is deprecated, so we use GregorianCalendar here
		    			java.util.Date dValue = null;
//		    			if( obj instanceof Date )
//		    			{
//		    				dValue = ((Date) obj).toDate(); 
//		    			}
//		    			else if( obj instanceof DateTime )
//		    			{
//		    				dValue = ((DateTime) obj).toDate(); 
//		    			}
//		    			else
//		    			{
//		    				dValue = (java.util.Date)obj;
//		    			}
		    			dValue = (java.util.Date)obj;
		    			cal = new GregorianCalendar();
		    			cal.setTime(dValue);
	    			}
	    			// check for time part
	    			long nTime = cal.get(Calendar.HOUR)+cal.get(Calendar.MINUTE)+cal.get(Calendar.SECOND)+cal.get(Calendar.MILLISECOND);
	    			//Ivy.log().info("TIME: "+nTime);
	    			//Ivy.log().info("YEAR: "+cal.get(Calendar.YEAR));
	    			if( nTime == 0 )
	    			{
	    				// no time part --> format it as Date
	    				csTemp.setDataFormat(formatDate);
	    			}
	    			else if( cal.get(Calendar.YEAR) < 1900  )
	    			{
	    				// no date part given --> format as Time
	    				// the function setDataFormat seems not to work with YEAR < 1900, so set it to 1900 (however, output is only the time part)
	    				cal.set(Calendar.YEAR, 1900);
	    				csTemp.setDataFormat(formatTime);
//	    				Ivy.log().info("DATETIME: "+cal.get(Calendar.YEAR)+"="+cal.get(Calendar.MONTH)+"="+cal.get(Calendar.DAY_OF_MONTH));
	    			}
	    			else 
	    			{
	    				// format as full date
	    				csTemp.setDataFormat(formatDateTime);
	    			}
	    			aCS[j] = csTemp;
	    			columnBaseInfos[j] = new ColumnBaseInfo(csTemp, obj.getClass().getName());
	    			///ExLog.info("CELL CREATED: Date = "+ cal.toString());
	    		}
	    		// --- TIME ----- 
	    		// --> does not really work for RTable source, because we never get an object of type Time from RTable (always java.util.Date)
//	    		else if( obj instanceof Time )
//	    		{
//	    			csTemp = wb.createCellStyle();
//	    			csTemp.cloneStyleFrom(csCommonCenter);
//	    			csTemp.setDataFormat(formatDateTime);
//	    			aCS[j] = csTemp;
//	    			///ExLog.info("CELL CREATED: Time = "+ cal.toString());
//	    		}
	    		// --- STRING -----
	    		else if( obj instanceof  java.lang.String )
	    		{
	    			csTemp = (XSSFCellStyle)wbs.createCellStyle();
	    			csTemp.cloneStyleFrom(csCommonLeft);
	    			aCS[j] = csTemp;
	    			columnBaseInfos[j] = new ColumnBaseInfo(csTemp, obj.getClass().getName());
	    		}
	    		// --- ALL OTHERS -----
	    		else
	    		{
	    			csTemp = (XSSFCellStyle)wbs.createCellStyle();
	    			csTemp.cloneStyleFrom(csCommonLeft);
	    			aCS[j] = csTemp;
	    			columnBaseInfos[j] = new ColumnBaseInfo(csTemp, obj.getClass().getName());
	    		}
	    		
	    		
	    		// *** save/hold all base common styles (like csCommonLeft, csCommonCenter, csCommonRight)
	    		for( int n=0 ; n<aCS.length ; n++) {
	    			baseColStyles[n] = aCS[n];
	    		}
	    		
	    		// *** set custom column style (overwrite base style in aCS[]) ***
	    		if( columnStyleMap.containsKey(j) ) {
	    			CustomColumnStyle ccs = new CustomColumnStyle(columnStyleMap.get(j), wbs, columnBaseInfos[j]);
	    			aCS[j] = ccs.getCellStyle();
	    		}
	    		
	    	}
	    	
			// *** generate CustomRowStyles for every RowStyle ***
			for( Map.Entry<String,RowStyle> entry : rowStyleMap.entrySet() ) {
				CustomRowStyle crs = new CustomRowStyle(entry.getValue(), wbs, columnBaseInfos);
				// add to map <ID,CustomRowStyle>
				customRowStyleMap.put(crs.getId(), crs);
			}
			
			// *** add predefined RowStyles
			// add style for repetitive header rows
			CustomRowStyle crsHeader = new CustomRowStyle(PredefinedRowStyle.HEADER.getId(), csHeader, baseColStyles);
			customRowStyleMap.put(crsHeader.getId(), crsHeader); 
			// add style for a plain row (separator row)
			XSSFCellStyle csSeparator = (XSSFCellStyle)wbs.createCellStyle();
			csSeparator.setAlignment(HorizontalAlignment.LEFT);
			csSeparator.setVerticalAlignment(VerticalAlignment.BOTTOM);
			csSeparator.setWrapText(false);
			CustomRowStyle crsSeparator = new CustomRowStyle(PredefinedRowStyle.SEPARATOR.getId(), csSeparator, baseColStyles);
			customRowStyleMap.put(crsSeparator.getId(), crsSeparator); 
	    	
	    	
	    	//ExLog.info("EXCEL EXPORT: generateWorkbook -> END CREATE CELL STYLES: "+ (new DateTime().format()));
		    
		    // --- VALUES ----- make the values cells
		    // Never use float or double datatypes to calculate amounts/monetary values, always use BigDecimal (als no casts between String and Float or Double )
		    // --> you will lost decimal places (i.e. to avoid any precision loss)!!
		    // CAUTION: Always use the BigDecimal("String") constructor, and never BigDecimal(double) for monetary calculation

		    ///ExLog.info("START ROW: "+ nStartRow);
	    	//ExLog.info("EXCEL EXPORT: generateWorkbook -> START DATA LOOP: "+ (new DateTime().format()));
			
	    	// 05.09.2014/HE: with XSSF and version 2007 the limits are 1,048,576 rows by 16,384 columns
	    	//                Let's leave it at 65536, because more rows does not make sense and requires a lot of time!!
	    	// 22.01.2020/HE: unblock limit of 65536, set it to 1,000,000 rows
		    for(int i=0; i<y && nStartRow+i+1 < 1000000 ; i++)	// get every row (excel max rows 1,048,576)
		    { 
		    	SXSSFRow r= sheet.createRow(nStartRow+i+1);
		    	for(int j=0; j<x; j++)	// get every column
		    	{ 
		    		// we build each cell value
		    		//HSSFCell cell = r.createCell(j);
		    		SXSSFCell cell = null;
			    	// get the data object
		    		//String sType = table.getValueAt(i, j).getClass().getName();
		    		Object obj = rows[i][j]; 				//table.getValueAt(i, j);
					///ExLog.info("OBJECT FOR CELL: "+ obj.getClass().getName());
		    		
		    		if( obj != null)
		    		{
		    			// convert numeric strings to number if requested
						if( convertNumericStrings && obj instanceof String )
						{
							try 
							{
								obj = new Double(Double.parseDouble((String) obj.toString()));
							} 
							catch (NumberFormatException e) 
							{
								// Not a number --> Nothing to do
								//Ivy.log().info("NumberFormatException: "+ e.getMessage());
							}
							catch (Exception e) 
							{
								// Not a number --> Nothing to do
								//Ivy.log().warn("EXCEL EXPORT: generateWorkbook -> Parsing of Number failed: "+ e.getMessage());
								ExLog.warn(this, new Object(){}, "EXCEL EXPORT: generateWorkbook -> Parsing of Number failed: "+ e.getMessage());
							}
						}
			    		// --- INTEGER / BIG INTEGER-----
			    		if( obj instanceof java.lang.Integer || obj instanceof java.math.BigInteger )
			    		{
			    			// treat it like double
			    			cell = r.createCell(j, CellType.NUMERIC);
			    			cell.setCellStyle(aCS[j]);
			    			cell.setCellStyle( getRowStyle(j, i, aCS, rowStyleIdMap, customRowStyleMap) );
			    			//cell = ExcelExporterHSSF.createCell(wb, r, j, HSSFCell.CELL_TYPE_NUMERIC, csCommon, HSSFCellStyle.ALIGN_RIGHT, HSSFCellStyle.VERTICAL_TOP);
			    			BigDecimal bg = new BigDecimal( obj.toString() );
			    			//cell.getCellStyle().setDataFormat(format.getFormat("#,##0"));
			    			cell.setCellValue( bg.doubleValue() );
			    			///ExLog.info("CELL CREATED: Integer = "+ bg.toString());
			    			//cell.setCellValue( ((java.lang.Integer)table.getValueAt(i, j)) );
			    		}
			    		// --- BIG DECIMAL -----
			    		else if( obj instanceof java.math.BigDecimal )
			    		{
			    			cell = r.createCell(j, CellType.NUMERIC);
			    			cell.setCellStyle( getRowStyle(j, i, aCS, rowStyleIdMap, customRowStyleMap) );
			    			//cell = ExcelExporterHSSF.createCell(wb, r, j, HSSFCell.CELL_TYPE_NUMERIC, csCommon, HSSFCellStyle.ALIGN_RIGHT, HSSFCellStyle.VERTICAL_TOP);
			    			BigDecimal bg =  (BigDecimal)obj;
			    			//Ivy.log().info("BIGDECIMAL_Double_SCALE: "+ bg.doubleValue() +" :: "+bg.scale() );
			    			//cell.getCellStyle().setDataFormat((short)4);	//"#,##0.00"
			    			// set decimalPlaces depending on scale
			    			//cell.getCellStyle().setDataFormat( getNumberFormat(wb, bg.scale()) );
			    			cell.setCellValue( bg.doubleValue() );
			    			///ExLog.info("CELL CREATED: BigDecimal = "+ bg.toString());
			    		}
			    		// --- DOUBLE / FLOAT -----
			    		else if( obj instanceof java.lang.Double || obj instanceof java.lang.Float )
			    		{
			    			cell = r.createCell(j, CellType.NUMERIC);
			    			cell.setCellStyle( getRowStyle(j, i, aCS, rowStyleIdMap, customRowStyleMap) );
			    			//cell = ExcelExporterHSSF.createCell(wb, r, j, HSSFCell.CELL_TYPE_NUMERIC, csCommon, HSSFCellStyle.ALIGN_RIGHT, HSSFCellStyle.VERTICAL_TOP);
			    			BigDecimal bg = new BigDecimal( obj.toString() );
			    			//Ivy.log().info("BIGDECIMAL_Double_SCALE: "+ bg.doubleValue() +" :: "+bg.scale() );
			    			//cell.getCellStyle().setDataFormat((short)4);	//"#,##0.00"
			    			//cell.getCellStyle().setDataFormat(format.getFormat("#,##0.00"));
			    			cell.setCellValue( bg.doubleValue() );
			    			///ExLog.info("CELL CREATED: Double/Float = "+ bg.toString());
			    		}
			    		// --- NUMBER / ALL OTHER NUMERIC -----
			    		else if( obj instanceof java.lang.Number )
			    		{
			    			cell = r.createCell(j, CellType.NUMERIC);
			    			cell.setCellStyle( getRowStyle(j, i, aCS, rowStyleIdMap, customRowStyleMap) );
			    			//cell = ExcelExporterHSSF.createCell(wb, r, j, HSSFCell.CELL_TYPE_NUMERIC, csCommon, HSSFCellStyle.ALIGN_RIGHT, HSSFCellStyle.VERTICAL_TOP);
			    			BigDecimal bg = new BigDecimal( obj.toString() );
			    			cell.setCellValue( bg.doubleValue() );
			    			///ExLog.info("CELL CREATED: Number = "+ bg.toString());
			    		}
			    		// --- DATE ----- 
			    		else if( obj instanceof java.util.Date  || 
			    				 obj instanceof GregorianCalendar )
			    		{
			    			GregorianCalendar cal;
			    			cell = r.createCell(j, CellType.NUMERIC);
			    			cell.setCellStyle( getRowStyle(j, i, aCS, rowStyleIdMap, customRowStyleMap) );
			    			//cell = ExcelExporterHSSF.createCell(wb, r, j, HSSFCell.CELL_TYPE_NUMERIC, csCommon, HorizontalAlignment.CENTER, HSSFCellStyle.VERTICAL_TOP);
			    			if( obj instanceof GregorianCalendar )
			    			{
			    				cal = (GregorianCalendar)obj;
			    			}
			    			else
			    			{
				    			// java.util.Date is deprecated, so we use GregorianCalendar here
				    			java.util.Date dValue = null;
//				    			if( obj instanceof Date )
//				    			{
//				    				dValue = ((Date) obj).toDate();
//				    			}
//				    			else if( obj instanceof DateTime )
//				    			{
//				    				dValue = ((DateTime) obj).toDate(); 
//				    			}
//				    			else
//				    			{
//				    				dValue = (java.util.Date)obj;
//				    			}
				    			dValue = (java.util.Date)obj;
				    			cal = new GregorianCalendar();
				    			cal.setTime(dValue);
			    			}
			    			// check for time part
			    			long nTime = cal.get(Calendar.HOUR)+cal.get(Calendar.MINUTE)+cal.get(Calendar.SECOND)+cal.get(Calendar.MILLISECOND);
			    			//Ivy.log().info("TIME: "+nTime);
			    			//Ivy.log().info("YEAR: "+cal.get(Calendar.YEAR));
			    			if( nTime == 0 )
			    			{
			    				if( cal.get(Calendar.YEAR) < 1900  )
			    				{
			    					cal.set(Calendar.YEAR, 1900);
			    					//Ivy.log().info("DATETIME RESETTED: "+cal.get(Calendar.YEAR)+"="+cal.get(Calendar.MONTH)+"="+cal.get(Calendar.DAY_OF_MONTH));
			    				}
			    				// no time part --> format it as Date
			    				//cell.getCellStyle().setDataFormat(formatDate);
			    			}
			    			else if( cal.get(Calendar.YEAR) < 1900  )
			    			{
			    				// no date part given --> format as Time
			    				// the function setDataFormat seems not to work with YEAR < 1900, so set it to 1900 (however, output is only the time part)
			    				cal.set(Calendar.YEAR, 1900);
			    				//cell.getCellStyle().setDataFormat(formatTime);
			    				//Ivy.log().info("DATETIME: "+cal.get(Calendar.YEAR)+"="+cal.get(Calendar.MONTH)+"="+cal.get(Calendar.DAY_OF_MONTH));
			    			}
			    			else 
			    			{
			    				// format as full date
			    				//cell.getCellStyle().setDataFormat(formatDateTime);
			    			}
			    			// Fill cell with an empty value if the date is prior 1900 (in fact the originally data was empty or 01.01.0001)
			    			if( cal.get(Calendar.YEAR) <= 1900 )
			    			{
			    				cell.setCellValue("");
			    			}else
			    			{
			    				cell.setCellValue(cal);
			    			}
			    			//ExLog.info("CELL CREATED: Date = "+ cal.toString());
			    			
			    		}
			    		// --- TIME ----- 
			    		// --> does not really work for RTable source, because we never get an object of type Time from RTable (always java.util.Date)
//			    		else if( obj instanceof Time )
//			    		{
//			    			cell = r.createCell(j, CellType.NUMERIC);
//			    			cell.setCellStyle(aCS[j]);
//			    			//cell = ExcelExporterHSSF.createCell(wb, r, j, HSSFCell.CELL_TYPE_NUMERIC, csCommon, HorizontalAlignment.CENTER, HSSFCellStyle.VERTICAL_TOP);
//			    			Time tValue = (Time) obj;
//			    			java.util.GregorianCalendar cal = new GregorianCalendar(1900, 1, 1, tValue.getHours(), tValue.getMinutes(), tValue.getSeconds());
//			    			//cell.getCellStyle().setDataFormat(formatDateTime);
//			    			cell.setCellValue(cal);
//			    			///ExLog.info("CELL CREATED: Time = "+ cal.toString());
//			    		}
			    		// --- STRING -----
			    		else if( obj instanceof  java.lang.String )
			    		{
			    			cell = r.createCell(j, CellType.STRING);
			    			cell.setCellStyle( getRowStyle(j, i, aCS, rowStyleIdMap, customRowStyleMap) );
			    			//cell = ExcelExporterHSSF.createCell(wb, r, j, HSSFCell.CELL_TYPE_STRING, csCommon, HSSFCellStyle.ALIGN_LEFT, HSSFCellStyle.VERTICAL_TOP);
			    			cell.setCellValue(new XSSFRichTextString( obj.toString() ));
			    			///ExLog.info("CELL CREATED: String = "+ obj.toString());
			    		}
			    		// --- ALL OTHERS -----
	
			    		else
			    		{
			    			cell = r.createCell(j, CellType.STRING);
			    			cell.setCellStyle( getRowStyle(j, i, aCS, rowStyleIdMap, customRowStyleMap) );
			    			//cell = ExcelExporterHSSF.createCell(wb, r, j, HSSFCell.CELL_TYPE_STRING, csCommon, HSSFCellStyle.ALIGN_LEFT, HSSFCellStyle.VERTICAL_TOP);
			    			cell.setCellValue( obj.toString() );
			    			///ExLog.info("CELL CREATED: Other = "+ obj.toString());
			    		}
		    		}
		    		else
		    		{
		    			// No object is given, so set an empty string
		    			cell = r.createCell(j, CellType.STRING);
		    			cell.setCellStyle(csCommonLeft);
		    			cell.setCellValue( "" );
		    			//ExLog.info("EXCEL EXPORT: generateWorkbook -> CELL CREATED: Value of cell is null !!");
		    			ExLog.info(this, new Object(){}, "EXCEL EXPORT: generateWorkbook -> CELL CREATED: Value of cell is null !!");
		    		}
		    	}
		    }
		    //ExLog.info("EXCEL EXPORT: generateWorkbook -> END DATA LOOP: "+ (new DateTime().format())); 
		    // adjust size of columns (This process can be relatively slow on large sheets, so do it only on small sheets)
		    if( y <= 999999 )
		    {
		    	sheet.trackAllColumnsForAutoSizing();
			    for(short i =0; i< x; i++){
			    	sheet.autoSizeColumn(i, false);
			    }
			    //ExLog.info("EXCEL EXPORT: generateWorkbook -> AUTOSIZING_COLUMNS FINISHED: "+ (new DateTime().format()));
		    }
		    // print setup, set margins to 10 mm (0.394 inch), header/footer to 5 mmm (0.197 inch)
		    sheet.setAutobreaks(true);
		    //ExLog.info("EXCEL EXPORT: generateWorkbook -> AUTOBREAKS FINISHED: "+ (new DateTime().format()));
		    //Ivy.log().warn("MARGIN CONSTANTS: "+HSSFSheet.BottomMargin+":"+HSSFSheet.TopMargin+":"+HSSFSheet.LeftMargin+":"+HSSFSheet.RightMargin+":"+HSSFSheet.HeaderMargin+":"+HSSFSheet.FooterMargin);
		    /*
		    sheet.setMargin(HSSFSheet.BottomMargin, 0.394);
		    sheet.setMargin(HSSFSheet.TopMargin, 0.394);
		    sheet.setMargin(HSSFSheet.LeftMargin, 1.0);
		    sheet.setMargin(HSSFSheet.RightMargin, 0.394);
		    //sheet.setMargin(HSSFSheet.HeaderMargin, 0.197);
		    //sheet.setMargin(HSSFSheet.FooterMargin, 0.197);
		    HSSFPrintSetup ps = sheet.getPrintSetup();
		    ps.setHeaderMargin(0.197);
		    ps.setFooterMargin(0.197);
		    ps.setPaperSize(HSSFPrintSetup.A4_PAPERSIZE);
		    ps.setLandscape(true);
		    */
		    
		    // log number of CellStyles created/used
			ExLog.info(this, new Object(){}, "Total CellStyles created: " +wbs.getNumCellStyles()+ "!" );
		    
		    // log number of exported rows
		    ExLog.info(this, new Object(){}, y +" rows with "+x+" columns exported! (Sheet: '"+sheetName+"')");
		    
		}
		catch (Exception e) 
		{
			//Ivy.log().error("EXCEL EXPORT: generateWorkbook -> Exception(generating Workbook): "+e.getMessage());
			ExLog.error(this, new Object(){}, "EXCEL EXPORT: generateWorkbook -> Exception(generating Workbook): "+e.getMessage());
			e.printStackTrace();
		    if( wbs == null )
		    {
		    	throw new NullPointerException("EXCEL EXPORT: generateWorkbook -> creation of new Workbook XSSF failed: Reference wb is null.");
		    }
		}

		return wbs;
	}
	
//    private Color hex2Rgb(String colorStr) {
//        return new Color(
//                Integer.valueOf(colorStr.substring(1, 3), 16),
//                Integer.valueOf(colorStr.substring(3, 5), 16),
//                Integer.valueOf(colorStr.substring(5, 7), 16));
//    }
	
	/** Changes the content type of the workbook. <br>
	 *  If the workbook file is generated from a template like xltx the content has to be converted to xlsx content type!<br>
	 *  REMARKS: we are NOT talking about the file extension here, the file name is usually already xlsx but the content type is still xltx.
	 * @param filePath
	 */
	private void changeContentType(String filePath)
	{
		java.io.File f = null;
		OPCPackage opcPackage;
		FileInputStream inputStream = null;
		FileOutputStream outputStream = null;
		boolean bReplacementSuccessful = false;
		
		ExLog.info(this, new Object(){}, "EXCEL EXPORT: Start changeContentType ... ");
		
		try
		{
			if( filePath != null && !filePath.isEmpty() )
			{
				// check if excel file exists
				f = new java.io.File(filePath);
				if( f.isFile() )
				{
					//ExLog.info("EXCEL EXPORT: changeContentType -> file to change found: "+filePath+" )!");
					inputStream = new FileInputStream(new java.io.File(filePath));
				    
				    opcPackage = OPCPackage.open(inputStream);

				    // try to get the contentType of the PackagePart (WORKBOOK)
				    java.util.List<PackagePart> al = opcPackage.getPartsByName(Pattern.compile("/xl/workbook.xml"));
				    if( al.size() == 1 )
				    {
				    	// detect XSSFRelation (ContentType)
				    	for (PackagePart packagePart : al) 
					    {
			                String pContent = packagePart.getContentType();
			                if( pContent.equals( XSSFRelation.WORKBOOK.getContentType() ) )
			                {
			                	// no conversion required (type is .xlsx)
			                	//ExLog.info("EXCEL EXPORT: changeContentType -> CONTENT TYPE already correct: "+pContent);
			                }
			                else
			                {
			                	// convert to .xlsx (e.g. from .xltx to .xlsx)
			                	//ExLog.info("CONTENT TYPE WORKBOOK: "+XSSFRelation.WORKBOOK.getContentType());
			                	//ExLog.info("EXCEL EXPORT: changeContentType -> CONTENT TYPE to replace: "+pContent);
			                	bReplacementSuccessful = opcPackage.replaceContentType(pContent, XSSFRelation.WORKBOOK.getContentType());
			                	//ExLog.info("EXCEL EXPORT: changeContentType -> CONTENT TYPE successful replaced: "+bReplacementSuccessful);
			                }
						    inputStream.close();
					    }
				    	
				    	// Save file (replace existing)
				    	if( bReplacementSuccessful )
				    	{
					    	outputStream = new FileOutputStream(filePath);
					    	opcPackage.save(outputStream);
					    	outputStream.close();
				    	}
				    }
				    else
				    {
				    	// PackagePart not found --> replacment not possible -> write log entry
				    	ExLog.error(this, new Object(){}, "EXCEL EXPORT: changeContentType -> File type for content change (OPCPackage) not supported: "+filePath);
				    }
				}
				else
				{
					// file seems to be a directory or does not exist 
					ExLog.error(this, new Object(){}, "EXCEL EXPORT: changeContentType -> File is not a file (directory ??) or file does not exist (not supported): "+filePath);
				}
			}
		}
		catch(SecurityException ex)
		{
			ExLog.error(this, new Object(){}, "EXCEL EXPORT: changeContentType -> SecurityException: "+ex.getMessage());
		}
		catch(FileNotFoundException ex)
		{
			ExLog.error(this, new Object(){}, "EXCEL EXPORT: changeContentType -> FileNotFoundException: "+ex.getMessage());
		}
		catch(Exception ex)
		{
			ExLog.error(this, new Object(){}, "EXCEL EXPORT: changeContentType -> GeneralException: "+ex.getMessage());
		}
		finally
		{
			ExLog.info(this, new Object(){}, "EXCEL EXPORT: End changeContentType ! ");
			
			// clean up: close all streams
			if( inputStream != null )
			{
				try{ inputStream.close(); }
				catch(IOException ex)
				{
					ExLog.error(this, new Object(){}, "EXCEL EXPORT: createWorkbook -> Can't close inputStream - Exception: "+ex.getMessage());
				}
			}
			if( outputStream != null )
			{
				try{ outputStream.close(); }
				catch(IOException ex)
				{
					ExLog.error(this, new Object(){}, "EXCEL EXPORT: createWorkbook -> Can't close outputStream - Exception: "+ex.getMessage());
				}
			}
		}
	}
	
	private short getNumberFormat(SXSSFWorkbook wb, int decimalPlaces)
	{
		short result = 0;
		try
		{
			DataFormat format = wb.createDataFormat();
			if( decimalPlaces == 0)
			{
				result = format.getFormat("#,##0");
			}
			else if( decimalPlaces == 1)
			{
				result = format.getFormat("#,##0.0");
			}
			else if( decimalPlaces == 2)
			{
				result = format.getFormat("#,##0.00");
			}
			else if( decimalPlaces == 3)
			{
				result = format.getFormat("#,##0.000");
			}
			else if( decimalPlaces == 4)
			{
				result = format.getFormat("#,##0.0000");
			}
			else if( decimalPlaces == 5)
			{
				result = format.getFormat("#,##0.00000");
			}
			else if( decimalPlaces == 6)
			{
				result = format.getFormat("#,##0.000000");
			}
			else if( decimalPlaces == 7)
			{
				result = format.getFormat("#,##0.0000000");
			}
			else if( decimalPlaces == 8)
			{
				result = format.getFormat("#,##0.00000000");
			}
			else if( decimalPlaces >= 9)
			{
				result = format.getFormat("#,##0.000000000");
			}
			else 
			{
				result = format.getFormat("#,##0.00");
			}
		}
		catch(Exception ex)
		{
			ExLog.warn(this, new Object(){}, "EXCEL EXPORT: getNumberFormat -> NumberFormat for cell not found: argument for decimal places: = "+ decimalPlaces);
		}
		return result;
	}
	
// ----------- PUBLIC HELPER FUNCTIONS -----------------------------------------------
	
	// DEACTIVATED FOR PURE JAVA VERSION !
	
//	public static List<List<Object>> removeColumns(List<Number> lstCol, List<List<Object>> matrix)
//	{
//		// helper function to remove columns in a list
//		// much higher performance here in java than in ivy script !!
//		List<List<Object>> lstTempData = matrix.clone();
//		lstTempData.clear();
//		List<Object> lst = List.create(Object.class);
//		
//		for(int k=0 ; k < matrix.size() ; k++ )
//		{
//			lst = matrix.get(k).clone();
//			// step backwards through the list and remove non selected columns/fields 
//			for(int n=lst.size()-1 ; n >= 0 ; n-- )
//			{
//				if( lstCol.contains(n) == false )
//				{
//					lst.remove(n);
//				}
//			}
//			lstTempData.add(lst);
//			//ExLog.info("LIST ADDED: "+ lst.size());
//		}
//		return lstTempData;
//	}
	
	
	
	
/*	
	/**
     * Creates a cell and aligns it a certain way.
     *
     * @param wb     the workbook
     * @param row    the row to create the cell in
     * @param column the column number to create the cell in
     * @param type   the cell type to create
     * @param halign the horizontal alignment for the cell.
     * @param valign the vertical alignment for the cell.
     */
	
/*
    private static HSSFCell createCell(HSSFWorkbook wb, HSSFRow row, int column, int cellType, HSSFCellStyle cs) 
    {
    	HSSFCell cell = row.createCell(column, cellType);
	    try
	    {
	        //cell.setCellValue(new HSSFRichTextString("Align It"));
	    	Ivy.log().warn("NUMBER OF STYLES: "+wb.getNumCellStyles());
	        //HSSFCellStyle cs = wb.createCellStyle();
//	        cs.setWrapText(true);
//	        cs.setBorderBottom(HSSFCellStyle.BORDER_DOTTED);
//	        cs.setBottomBorderColor(new HSSFColor.BLACK().getIndex());
//	        cs.setBorderLeft(HSSFCellStyle.BORDER_DOTTED);
//	        cs.setLeftBorderColor(new HSSFColor.BLACK().getIndex());
//	        cs.setBorderTop(HSSFCellStyle.BORDER_DOTTED);
//	        cs.setTopBorderColor(new HSSFColor.BLACK().getIndex());
//	        cs.setBorderRight(HSSFCellStyle.BORDER_DOTTED);
//	        cs.setRightBorderColor(new HSSFColor.BLACK().getIndex());
	        cell.setCellStyle(cs);
	        
	    }
	    catch(java.lang.IllegalStateException ex)
	    {
	    	Ivy.log().error("STYLE ERROR: maximum number of cell styles exceeded the limit! ("+wb.getNumCellStyles()+"), "+ex.getMessage());
	    }
	    catch(Exception ex)
	    {
	    	Ivy.log().error("STYLE ERROR: Unknown error! ("+wb.getNumCellStyles()+"), "+ex.getMessage());
	    }
	    
	    return cell;
    }
	
*/
/*	
	/**
	 * 
	 * @param headers 
	 * @param rows 
	 * @param outputPath 
	 * @param fileName 
	 * @param SheetName 
	 * @return
	 */

/*	
	public static java.io.File exportListAsExcel(List<String> headers,List<List<String>> rows, String outputPath, String fileName, String SheetName){
		java.io.File myXLS =null;
		if(rows== null || headers == null || headers.isEmpty() || rows.isEmpty() || rows.get(0).size()!= headers.size()) return myXLS;
		
//		CHECK AND INITIALIZE THE ARGUMENTS
		if(outputPath==null || outputPath.trim().equalsIgnoreCase("")) outputPath="excelExport";
		if(fileName==null || fileName.trim().equalsIgnoreCase("")) fileName="excelFile";
		if(SheetName==null || SheetName.trim().equalsIgnoreCase("")) SheetName="sheet";
		fileName = FileHandler.formatPathWithEndSeparator(outputPath)+FileHandler.getFileNameWithoutExt(fileName)+".xls";
		
	    //build tables of values from the table
		
	    int x= headers.size();
	    int y = rows.size();
	    
	    try {
	    	
		    String heads[] = new String[x];
		    for(int i = 0; i<x; i++){
		    	heads[i] = headers.get(i);
		    }
		    	
			HSSFWorkbook wb = new HSSFWorkbook();
		    FileOutputStream fileOut;
			HSSFSheet sheet = wb.createSheet(SheetName);
			HSSFCellStyle cs = wb.createCellStyle();
			cs.setWrapText(true);
			//make the titles row
			HSSFRow row = sheet.createRow((short)0);
		    for(int i=0; i<heads.length;i++){
		    	HSSFCell cell = row.createCell(i);
		    	cell.setCellValue(new HSSFRichTextString(heads[i]));
		    }
		   
		    //make the values cells
		    for(int i=0; i<y; i++){ // get every row
		    	HSSFRow r= sheet.createRow((short)i+1);
		    	List<String> l = rows.get(i);
		    	for(int j=0; j<x; j++){ // get every column 
		    		// we build each cell value
		    		HSSFCell cell = r.createCell(j);
		    		cell.setCellValue(new HSSFRichTextString(l.get(j)));
		    		cell.setCellStyle(cs);
		    	}
		    }
		    for(short i =0; i< x; i++){
		    	sheet.autoSizeColumn(i, false);
		    }
		    
			fileOut = new FileOutputStream(fileName);
			wb.write(fileOut);
		    fileOut.close();
		    myXLS = new java.io.File(fileName);
		} catch (FileNotFoundException e) {
			Ivy.log().error("FileNotFoundException "+e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			Ivy.log().error("IOException "+e.getMessage());
			e.printStackTrace();
		}
		catch (Exception e) {
			Ivy.log().error("Exception "+e.getMessage());
			e.printStackTrace();
		}
		return myXLS;
	}
*/
	
	

}

