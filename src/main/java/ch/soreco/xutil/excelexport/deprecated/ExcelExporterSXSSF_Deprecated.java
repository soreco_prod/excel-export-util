package ch.soreco.xutil.excelexport.deprecated;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.regex.Pattern;
import java.math.BigDecimal;

import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFCreationHelper;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRelation;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Footer;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;

import ch.soreco.xutil.excelexport.ExcelExporterSXSSF;
import ch.soreco.xutil.filemanager.FileHandler;
import ch.soreco.xutil.util.Base64Encoder;
import ch.soreco.xutil.util.ExLog;
import ch.soreco.xutil.util.GuidGenerator;
import ch.soreco.xutil.util.PerfMeter;


//import com.ulcjava.base.application.table.ULCTableColumn;

/**
 * based on a rudimentary version of ec
 * @author SORECO AG/HE R.Heiniger
 * @version 
 * 1.0 - 06.04.2010/he -- First version (based on a rudimentary version of ec)<br>
 * 1.1 - 06.04.2010/he -- Improvements: proper processing of numeric values(!), formatting of different data types, layout of header and values (cell styles)<br>
 * 1.2 - 19.09.2011/he -- Improvements: style processing for large sheets, layout (number formatting, title and subtitle etc.)<br>
 * 1.3 - 01.11.2011/he -- Improvements: new public function exportRecordsetAsExcel(), <br>
 * 										- new public helper function removeColumns(), <br>
 * 										- new private helper function writeWbFile(), <br>
 * 										- clean up log messages <br>
 * 1.4 - 19.08.2014/he -- Improvements: Support of templates (.xlt) as base-file,  to support (pre-)formatting and macros! <br>
 * 1.5 - 20.08.2014/he -- Improvements: Converting to XSSF Model --{@literal >} Excel 2007 OOXML (.xlsx) file format <br>
 * 1.6 - 14.02.2018/he -- Improvements: New Constructors with arguments _pathExcelFile and _basePathOfTemplates <br>
 * 1.7 - 29.01.2020/he -- Improvements: SXSSF variant for exporting big amount of records (into very large spreadsheet), Logging with ExLog (formatted System.out) <br>
 * 
 *
 * @deprecated use {@link ExcelExporterSXSSF} instead.  
 */
@Deprecated
public class ExcelExporterSXSSF_Deprecated 
{
	private String fileName = "";
	private String outputPath;
	private File eeFile = null;
	private String excelExportTemplatePath = "";

	private static final String excelExportTempPath = "C:\\temp\\";			// Temp folder if not defined in System.getProperty("java.io.tmpdir")
	private static final String prefixPath = "EXCEL_EXPORT";				// subfolder in temp path for export file creation
	
	
	/** Constructor
	 *  Excel export file will be created in default temp path
	 */
	public ExcelExporterSXSSF_Deprecated()
	{
		outputPath = System.getProperty("java.io.tmpdir");
		if( outputPath.isEmpty() )
		{
			outputPath = new String( excelExportTempPath );
		}
		// prepare and create path (if not already exists)
		outputPath = FileHandler.formatPathWithEndSeparator(outputPath)+prefixPath;
		outputPath = FileHandler.formatPathWithEndSeparator(outputPath, true);
		
		eeFile = this.createFile(true);
	}
	
	
	/** Constructor
	 * @param _pathExcelFile target path to excel export file (without file name, will be generated automatically)
	 */
	public ExcelExporterSXSSF_Deprecated(String _pathExcelFile)
	{
		outputPath = new String(_pathExcelFile);
		if( outputPath.isEmpty() )
		{
			throw new IllegalArgumentException("Path argument may not be empty!");
		}
		// prepare and create path (if not already exists)
		outputPath = FileHandler.formatPathWithEndSeparator(outputPath, true);
		
		eeFile = this.createFile(true);
	}
	
	
	/** Constructor
	 * @param _pathExcelFile target path to excel export file (without file name, will be generated automatically)
	 * @param _basePathOfTemplates base path of templates files like .xltx (e.g. C:\\myTemplates\)
	 */
	public ExcelExporterSXSSF_Deprecated(String _pathExcelFile, String _basePathOfTemplates)
	{
		outputPath = new String(_pathExcelFile);
		if( outputPath.isEmpty() )
		{
			throw new IllegalArgumentException("Path argument may not be empty!");
		}
		// prepare and create path (if not already exists)
		outputPath = FileHandler.formatPathWithEndSeparator(outputPath, true);
		
		// set path of templates, prepare and create path (if not already exists)
		excelExportTemplatePath = new String(_basePathOfTemplates);
		excelExportTemplatePath = FileHandler.formatPathWithEndSeparator(excelExportTemplatePath, true);
		
		eeFile = this.createFile(true);
	}
	
//	/** Constructor
//	 * @param _fileName - the String name of the excel File, extension will changes automatically to *.xlsx, if the string is empty an unique name will be generated.<br>
//	 * if null or empty String, a unique name will be generated (like Excel_Export_<UniqueID>).
//	 */
//	public ExcelExporterXSSF(String _fileName)
//	{
//		fileName = _fileName;
//		outputPath = "";
//		ivyFile = this.createFile(false);
//	}
	
//	/** Constructor
//	 * @param _outputPath  - the String name of the path on the server where the excel file has to be saved<br>
//	 * if null or empty String, "excelExport" will be the name of the output path. output path of excel file without file name
//	 * @param _fileName file name, extension will changes automatically to *.xlsx, if the string is empty an unique name will be generated.
//	 */
//	public ExcelExporterXSSF(String _outputPath, String _fileName)
//	{
//		fileName = _fileName;
//		outputPath = _outputPath;
//		ivyFile = this.createFile(true);
//	}
	
	
	private File createFile(boolean bUsePath)
	{
		eeFile = null;
		
		try
		{
			//CHECK AND INITIALIZE THE ARGUMENTS
			//if(outputPath==null || outputPath.trim().equalsIgnoreCase("")) outputPath="Excel_Export";
			if(fileName==null || fileName.trim().equalsIgnoreCase("")) fileName="ExcelExp_"+GuidGenerator.generateID();
			//if(sheetName==null || sheetName.trim().equalsIgnoreCase("")) sheetName="sheet";
			
			if( bUsePath )
			{
				// use the path the caller has given
				fileName = FileHandler.formatPathWithEndSeparator(outputPath, false)+FileHandler.getFileNameWithoutExt(fileName)+".xlsx";
			}
			else
			{
				// generate a path automatically (prefixPath + UserID)
				outputPath = FileHandler.formatPathWithEndSeparator(prefixPath, false);
				//outputPath += FileHandler.formatPathWithEndSeparator(/*Ivy.session().getSessionUserName()*/"", false);
				fileName = FileHandler.formatPathWithEndSeparator(outputPath, false)+FileHandler.getFileNameWithoutExt(fileName)+".xlsx";
			}
			
			eeFile = new File(fileName); 
			eeFile.createNewFile();
		
		} 
	    catch (FileNotFoundException e) 
		{
			//Ivy.log().error("EXCEL EXPORT: createFile -> FileNotFoundException: "+e.getMessage());
	    	//ExLog.info(_className, _methodName, _message);
	    	ExLog.error(this, new Object(){}, "EXCEL EXPORT: createFile -> FileNotFoundException: "+e.getMessage());
			e.printStackTrace();
			eeFile = null;
		} 
	    catch (IOException e) 
		{
			//Ivy.log().error("EXCEL EXPORT: createFile -> IOException: "+e.getMessage());
	    	ExLog.error(this, new Object(){}, "EXCEL EXPORT: createFile -> IOException: "+e.getMessage());
			e.printStackTrace();
			eeFile = null;
		}
		catch (Exception e) 
		{
			//Ivy.log().error("EXCEL EXPORT: createFile -> Exception: "+e.getMessage());
			ExLog.error(this, new Object(){}, "EXCEL EXPORT: createFile -> Exception: "+e.getMessage());
			e.printStackTrace();
			eeFile = null;
		}
		
	    return eeFile;
	}
	
	
	/**
	 * Exports a two dimensional List (matrix) as Excel file (this version returns the excel file as a base64 encoded string!)
	 * @param headers - a list of strings with the header text. The list length must correspond to the column count of parameter matrix
	 * @param matrix - a list (rows) of list of objects (columns) that contains the data to export.<br>
	 * The data type of the objects determines the output format, so choose the right data type to get the correct format in the excel file:<br>
	 * examples:<br>
	 * Amounts - use BigDecimal with scale 2<br>
	 * Date - use Date or java.util.Date to get a date with no time part<br>
	 * Integer - use Integer for numbers with no decimal places<br>
	 * @param sheetName - the name of the sheet. If null or empty String, "sheet" will be the name of the sheet.
	 * @param title - the title is inserted in the first row of the excel sheet. If the string is null or empty, no title will be generated and the header data start at first row.
	 * @param subtitle - a list of subtitles or any other information, every subtitle will be inserted in a own row after the title. If the list is null or empty, no rows will be generated.
	 * @param convertNumericStrings: if true all strings with a numeric value will be converted to a number (cell of type numeric, right alignment etc.)
	 * @param templatePath - full path or only file name to a excel template (depending on constructor used before), if null or empty String a no template is used (a new workbook will be created)
	 * @return String - the excel file as a base64 encoded string
	 */
	public String exportListAsExcelBase64(List<String> headers,List<ArrayList<Object>> matrix, String sheetName, String title, String[] subtitle, boolean convertNumericStrings, String templatePath)
	{
		String base64 = null;
		File excelFile = null;
		
		try
		{
			excelFile = exportListAsExcel(headers, matrix, sheetName, title, subtitle, convertNumericStrings, templatePath);
			
			if( excelFile != null )
			{
				// create base64 string
				base64 = Base64Encoder.encodeBase64(excelFile);
			}
		}
		catch(Exception ex)
		{
			throw ex;
		}
		finally 
		{
			// cleanup file (we only return the base64 string)
			if( excelFile != null )
			{
				if( !FileHandler.deleteFile(excelFile.getPath()) )
				{
					String msg = String.format("EXCEL EXPORT: exportListAsExcelBase64 ->  Can't cleanup, delete of file '%1$s' failed! ", excelFile.getPath() );
					ExLog.info(this, new Object(){}, msg);
				}
			}
		}
		
		return base64;
	}
	
	
	/**
	 * Exports a two dimensional List (matrix) as Excel file
	 * @param headers - a list of strings with the header text. The list length must correspond to the column count of parameter matrix
	 * @param matrix - a list (rows) of list of objects (columns) that contains the data to export.<br>
	 * The data type of the objects determines the output format, so choose the right data type to get the correct format in the excel file:<br>
	 * examples:<br>
	 * Amounts - use BigDecimal with scale 2<br>
	 * Date - use Date or java.util.Date to get a date with no time part<br>
	 * Integer - use Integer for numbers with no decimal places<br>
	 * @param sheetName - the name of the sheet. If null or empty String, "sheet" will be the name of the sheet.
	 * @param title - the title is inserted in the first row of the excel sheet. If the string is null or empty, no title will be generated and the header data start at first row.
	 * @param subtitle - a list of subtitles or any other information, every subtitle will be inserted in a own row after the title. If the list is null or empty, no rows will be generated.
	 * @param convertNumericStrings: if true all strings with a numeric value will be converted to a number (cell of type numeric, right alignment etc.)
	 * @param templatePath - full path or only file name to a excel template (depending on constructor used before), if null or empty String a no template is used (a new workbook will be created)
	 * @return File - the excel java.io.File made with this export.
	 */
	public File exportListAsExcel(List<String> headers,List<ArrayList<Object>> matrix, String sheetName, String title, String[] subtitle, boolean convertNumericStrings, String templatePath)
	{
		SXSSFWorkbook wbs = null;
		
		// log output path
		ExLog.info(this, new Object(){}, "using following output path for export -> "+outputPath);
		// start PerfMeter to log execution time
		PerfMeter pm = new PerfMeter();
		
		//File ivyFile = null;
		if(matrix == null || headers == null || headers.isEmpty() || matrix.isEmpty() || matrix.get(0).size()!= headers.size()) return null; //ivyFile;
		//ivyFile = this.createFile(false/* outputPath, fileName*/);
		
	    int x = headers.size();
	    int y = matrix.size();
	    
	    try 
	    {
		    String heads[] = new String[x];
		    for(int i = 0; i<x; i++)
		    {
		    	try
		    	{
			    	if( headers.get(i) == null)
			    	{
			    		heads[i] = null;
			    	}
			    	else
			    	{
			    		heads[i] = headers.get(i);
			    	}
		    	}
		    	catch(NullPointerException ex)
		    	{
		    		heads[i]="";
		    	}
		    }
		
		    //Ivy.log().debug("EXCEL EXPORT: exportListAsExcel -> Start creating object array...: "+ (new DateTime().format()));
		    // create a Array[][] from the List
		    Object[][] rows = new Object[y][x];
		    for(int i=0; i<y; i++)	// get every row
		    { 
		    	// inner List ( = record)
		    	///Ivy.log().debug("CREATE ROW: "+i);
		    	for(int j=0; j<x; j++)	// get every column
		    	{ 
		    		///Ivy.log().debug("FILL VALUES ROW/COLUMN: "+i+"/"+j);
		    		rows[i][j] = ( matrix.get(i).get(j) );
		    	}
		    	///Ivy.log().debug("ROW FILLED: "+i);
		    }
		    ///Ivy.log().debug("ROWS:"+rows.length);
		    ///Ivy.log().debug("COLUMN:"+rows[0].length);
		    //Ivy.log().debug("EXCEL EXPORT: exportListAsExcel -> Finished creating object array (from List): "+ (new DateTime().format()));
		    
		    // generate the Workbook object
		    wbs = this.generateWorkbook(heads, rows, sheetName, title, subtitle, convertNumericStrings, templatePath);
		    eeFile = this.writeWbFile(/*ivyFile,*/ wbs);
		    
	       // dispose of temporary files backing this workbook on disk
	       wbs.dispose();
	       
	       // log performance
	       pm.stop().logExecutionTimeInfo("EXCEL EXPORT::exportListAsExcel()");
	    }
		catch (Exception e) 
		{
			//Ivy.log().error("EXCEL EXPORT: exportListAsExcel -> Exception: "+e.getMessage());
			ExLog.error(this, new Object(){}, "EXCEL EXPORT: exportListAsExcel -> Exception: "+e.getMessage());
			e.printStackTrace();
		}
		return eeFile;
	}

//	/**
//	 * Exports a two dimensional List (matrix) as Excel file
//	 * @param headers - a list of strings with the header text. The list length must correspond to the column count of parameter matrix
//	 * @param matrix - a recordset that contains the data to export.<br>
//	 * The data type of the objects (fields) determines the output format, so choose the right data type to get the correct format in the excel file:<br>
//	 * examples:<br>
//	 * Amounts - use BigDecimal with scale 2<br>
//	 * Date - use Date or java.util.Date to get a date with no time part<br>
//	 * Integer - use Integer for numbers with no decimal places<br>
//	 * @param sheetName - the name of the sheet. If null or empty String, "sheet" will be the name of the sheet.
//	 * @param title - the title is inserted in the first row of the excel sheet. If the string is null or empty, no title will be generated and the header data start at first row.
//	 * @param subtitle - a list of subtitles or any other information, every subtitle will be inserted in a own row after the title. If the list is null or empty, no rows will be generated.
//	 * @param convertNumericStrings: if true all strings with a numeric value will be converted to a number (cell of type numeric, right alignment etc.)
//	 * @param templatePath - path to a excel template, if null or empty String a no template is used (a new workbook will be created)
//	 * @return the excel java.io.File made with this export.
//	 */
//	public File exportRecordsetAsExcel(List<String> headers,Recordset matrix, String sheetName, String title, String[] subtitle, boolean convertNumericStrings, String templatePath)
//	{
//		XSSFWorkbook wb = null;
//	    //FileOutputStream fileOut;
//		
//		//File ivyFile = null;
//		if(matrix == null || headers == null || headers.isEmpty() || matrix.size()<= 0 || matrix.getAt(0).size()!= headers.size()) return null; //ivyFile;
//		//ivyFile = this.createFile(false/*, outputPath, fileName*/);
//		
//	    int x = headers.size();
//	    int y = matrix.size();
//	    
//	    try 
//	    {
//		    String heads[] = new String[x];
//		    for(int i = 0; i<x; i++)
//		    {
//		    	try
//		    	{
//			    	if( headers.get(i) == null)
//			    	{
//			    		heads[i] = null;
//			    	}
//			    	else
//			    	{
//			    		heads[i] = headers.get(i);
//			    	}
//		    	}
//		    	catch(NullPointerException ex)
//		    	{
//		    		heads[i]="";
//		    	}
//		    }
//		
//		    Ivy.log().debug("EXCEL EXPORT: exportRecordsetAsExcel -> Start creating object array...: "+ (new DateTime().format()));
//		    // create a Array[][] from the Recordset
//		    Object[][] rows = new Object[y][x];
//		    for(int i=0; i<y; i++)	// get every row
//		    { 
//		    	// inner List ( = record)
//		    	///Ivy.log().debug("CREATE ROW: "+i);
//		    	for(int j=0; j<x; j++)	// get every column
//		    	{ 
//		    		///Ivy.log().debug("FILL VALUES ROW/COLUMN: "+i+"/"+j);
//		    		rows[i][j] = ( matrix.getField(i, j) );
//		    	}
//		    	///Ivy.log().debug("ROW FILLED: "+i);
//		    }
//		    ///Ivy.log().debug("ROWS:"+rows.length);
//		    ///Ivy.log().debug("COLUMN:"+rows[0].length);
//		    Ivy.log().debug("EXCEL EXPORT: exportRecordsetAsExcel -> Finished creating object array (from Recordset): "+ (new DateTime().format()));
//		    
//		    // generate the Workbook object
//		    wb = this.generateWorkbook(heads, rows, sheetName, title, subtitle, convertNumericStrings, templatePath);
//		    ivyFile = this.writeWbFile(/*ivyFile,*/ wb);
//	    }
//		catch (Exception e) 
//		{
//			Ivy.log().error("EXCEL EXPORT: exportRecordsetAsExcel -> Exception: "+e.getMessage());
//			e.printStackTrace();
//		}
//		return ivyFile;
//	}
	
	private File writeWbFile(SXSSFWorkbook wb)
	{
		FileOutputStream fileOut;
		
		try
		{
		    fileOut = new FileOutputStream(eeFile.getAbsolutePath());
		    //Ivy.log().info("EXCEL EXPORT: writeWbFile -> Start writing excel export file: "+ (new DateTime().format()));
			wb.write(fileOut);
		    fileOut.close();
		    //Ivy.log().info("EXCEL EXPORT: writeWbFile -> Finished writing excel export file: "+ (new DateTime().format()));
		    
		    // change content type
		    changeContentType(eeFile.getAbsolutePath());
		    //Ivy.log().info("EXCEL EXPORT: writeWbFile -> Finished changing content type of excel export file: "+ (new DateTime().format()));
		    
		} 
		catch (FileNotFoundException e) 
		{
			//Ivy.log().error("EXCEL EXPORT: writeWbFile -> FileNotFoundException: "+e.getMessage());
			ExLog.error(this, new Object(){}, "EXCEL EXPORT: writeWbFile -> FileNotFoundException: "+e.getMessage());
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			//Ivy.log().error("EXCEL EXPORT: writeWbFile -> IOException: "+e.getMessage());
			ExLog.error(this, new Object(){}, "EXCEL EXPORT: writeWbFile -> IOException: "+e.getMessage());
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			//Ivy.log().error("EXCEL EXPORT: writeWbFile -> Exception: "+e.getMessage());
			ExLog.error(this, new Object(){}, "EXCEL EXPORT: writeWbFile -> Exception: "+e.getMessage());
			e.printStackTrace();
		}
		return eeFile;
	}
	
	private XSSFWorkbook createWorkbook(String templatePath)
	{
		XSSFWorkbook wb = null;
		java.io.File f = null;
		FileInputStream inputStream = null;
		
		try
		{
			if( templatePath != null && !templatePath.isEmpty() )
			{
				// check if template exists
				// 1. value in templatePath is already full path
				f = new java.io.File(templatePath);
				if( f.isFile() )
				{
					//Ivy.log().info("EXCEL EXPORT: createWorkbook -> create Workbook from template (Template found: "+templatePath+" )!");
				    //String templateFile = "C:\\temp\\ExcelExport_Template\\TemplateBase3.xlt";
					inputStream = new FileInputStream(new java.io.File(templatePath));
				    wb = new XSSFWorkbook(inputStream);
				    inputStream.close();
				}
				else
				{
					// 2. value in templatePath is only a file name (try with base path from Global Variables)
					String tPath;
					//tPath = FileHandler.formatPathWithEndSeparator(Ivy.var().get("xbase_excel_jsf_ExcelExportTemplatePath"), false)+templatePath;
					tPath = FileHandler.formatPathWithEndSeparator(excelExportTemplatePath, false)+templatePath;
					f = new java.io.File(tPath);
					if( f.isFile() )
					{
						//Ivy.log().info("EXCEL EXPORT: createWorkbook -> create Workbook from template 2 (Template found: "+tPath+" )!");
						inputStream = new FileInputStream(new java.io.File(tPath));
					    wb = new XSSFWorkbook(inputStream);
					    inputStream.close();
					}
					else
					{
						String msg = String.format("EXCEL EXPORT: createWorkbook -> Template FILE NOT FOUND neither in '%1$s' nor '%2$s'!", templatePath, tPath);
						//Ivy.log().error(msg);
						ExLog.error(this, new Object(){}, msg);
					}
				}
			}
		}
		catch(SecurityException ex)
		{
			//Ivy.log().error("EXCEL EXPORT: createWorkbook -> SecurityException: "+ex.getMessage());
			ExLog.error(this, new Object(){}, "EXCEL EXPORT: createWorkbook -> SecurityException: "+ex.getMessage());
			wb = null;
		}
		catch(FileNotFoundException ex)
		{
			//Ivy.log().error("EXCEL EXPORT: createWorkbook -> FileNotFoundException: "+ex.getMessage());
			ExLog.error(this, new Object(){}, "EXCEL EXPORT: createWorkbook -> FileNotFoundException: "+ex.getMessage());
			wb = null;
		}
		catch(Exception ex)
		{
			//Ivy.log().error("EXCEL EXPORT: createWorkbook -> Exception: "+ex.getMessage());
			ExLog.error(this, new Object(){}, "EXCEL EXPORT: createWorkbook -> Exception: "+ex.getMessage());
			wb = null;
		}
		finally
		{
			if( inputStream != null )
			{
				try
				{
					inputStream.close();
				}
				catch(IOException ex)
				{
					//Ivy.log().error("EXCEL EXPORT: createWorkbook -> Can't close inputStream - Exception: "+ex.getMessage());
					ExLog.error(this, new Object(){}, "EXCEL EXPORT: createWorkbook -> Can't close inputStream - Exception: "+ex.getMessage());
				}
			}
		}

		if( wb == null )
		{
			// no valid template exists -> create a empty new workbook
			//Ivy.log().info("EXCEL EXPORT: createWorkbook -> Create a new workbook (no template found)!");
			ExLog.info(this, new Object(){}, "EXCEL EXPORT: createWorkbook -> Create a new workbook (no template found)!");
			try
			{
				wb = new XSSFWorkbook();
			}
			catch(Exception ex)
			{
				//Ivy.log().error("EXCEL EXPORT: createWorkbook -> Exception: "+ex.getMessage()+"/"+ex.getCause());
				ExLog.error(this, new Object(){}, "EXCEL EXPORT: createWorkbook -> Exception: "+ex.getMessage()+"/"+ex.getCause());
				wb = null;
			}
		}
		
		return wb;
	}
	
	

	//@SuppressWarnings("deprecation")
	/**
	 * @param heads
	 * @param rows
	 * @param sheetName
	 * @param title
	 * @param subtitle
	 * @param convertNumericStrings
	 * @param templatePath
	 * @return the generated XSSFWorkbook object
	 */
	private SXSSFWorkbook generateWorkbook(String[] heads,Object[][] rows, String sheetName, String title, String[] subtitle, boolean convertNumericStrings, String templatePath)
	{
		SXSSFWorkbook wbs = null;
		XSSFWorkbook wbx = null;
		SXSSFSheet sheet = null;
		
		try
		{
		    int x= heads.length;
		    int y = rows.length;
		    int nStartRow = 0;	// row where the data output should start (including header row)
		    
		    // create Workbook 
		    // String templatePath = "C:\\temp\\ExcelExport_Template\\TemplateBase4.xlt";
		    wbx = createWorkbook(templatePath);
		    
		    if( wbx == null )
		    {
		    	throw new NullPointerException("EXCEL EXPORT: generateWorkbook -> creation of new Workbook XSSF failed: Reference wb is null.");
		    }
		    
		    wbs = new SXSSFWorkbook(wbx, 500);
		    
		    // create/get sheet of workbook
			// Note that sheet name is Excel must not exceed 31 characters and must not contain any of the any of the following characters:
		    // 0x0000, 0x0003, colon (:), backslash (\), asterisk (*), question mark (?), forward slash (/), opening or closing square bracket ([])
			// for a safe way to create valid names, this utility replaces invalid characters with a space (' ') 
			String safeName = WorkbookUtil.createSafeSheetName(sheetName); 
			try
			{
				// try to get the first sheet (only possible if the workbook based on a template)
				sheet = wbs.getSheetAt(0);	// throws java.lang.IllegalArgumentException if sheet not exists!
			}
			catch(Exception ex)
			{
				//Ivy.log().debug("EXCEL EXPORT: generateWorkbook -> sheet not found ...! "+ ex.getClass().getName());
			}
			
			if( sheet != null )
			{
				// get first existing sheet (important if template with macros is used)
				wbs.setSheetName(wbs.getSheetIndex(sheet), safeName);
				//Ivy.log().debug("EXCEL EXPORT: generateWorkbook -> Existing sheet found and renamed!");
			}
			else
			{
				// no sheet exists (e.g. if a new workbook was created -> no template is given )
				sheet = wbs.createSheet(safeName);
				//Ivy.log().debug("EXCEL EXPORT: generateWorkbook -> New sheet created!");
			}
			
			
			// create DataFormat to format values (never do this in the main loop of cell creating!!)
		    DataFormat format = wbs.createDataFormat();
			SXSSFCreationHelper createHelper = (SXSSFCreationHelper)wbs.getCreationHelper(); 
			short formatDate = createHelper.createDataFormat().getFormat("dd.mm.yyyy");
			short formatTime = createHelper.createDataFormat().getFormat("h:mm:ss");
			short formatDateTime = createHelper.createDataFormat().getFormat("dd.mm.yyyy h:mm:ss");


			//--- MAIN-TITLE ----- Create a main title (if not empty)
			if( title != null && title.length() > 0 )
			{
				nStartRow = 2;
				CellStyle csTitle = wbs.createCellStyle();
				csTitle.setAlignment(HorizontalAlignment.LEFT);
				csTitle.setVerticalAlignment(VerticalAlignment.BOTTOM);
				csTitle.setWrapText(false);
				Font tFont = wbs.createFont();
				//Ivy.log().debug("TITLE FONT HEIGHT: "+ tFont.getFontHeightInPoints());
				tFont.setFontHeightInPoints((short)(tFont.getFontHeightInPoints()+2));
				tFont.setBold(true);
		        //tFont.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		        csTitle.setFont(tFont);
				
				SXSSFRow rowTitle = sheet.createRow((short)0);
				SXSSFCell tCell = rowTitle.createCell(0);
				tCell.setCellValue(title);
				tCell.setCellStyle(csTitle);
				sheet.addMergedRegion(new CellRangeAddress(
										0,				//first row (0-based)
										0,				//last row  (0-based)
										0,				//first column (0-based)
										heads.length-1	//last column  (0-based)
										));
			}
			
			//--- SUB-TITLE ----- Create subtitles (if not empty)
			if( subtitle != null && subtitle.length > 0 )
			{
				int startRowSubTitle = nStartRow;
				nStartRow += subtitle.length+1;	//set value for data rows
				CellStyle csSubTitle = wbs.createCellStyle();
				csSubTitle.setAlignment(HorizontalAlignment.LEFT);
				csSubTitle.setVerticalAlignment(VerticalAlignment.BOTTOM);
				csSubTitle.setWrapText(false);
				Font stFont = wbs.createFont();
				//Ivy.log().debug("TITLE FONT HEIGHT: "+ tFont.getFontHeightInPoints());
				stFont.setFontHeightInPoints((short)(stFont.getFontHeightInPoints()+0));	// don't change height for subtitles
				stFont.setBold(true);
				//stFont.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		        csSubTitle.setFont(stFont);
				
			    for(int i=0; i<subtitle.length;i++)
			    {
			    	//HSSFCell hCell = row.createCell(i);
			    	//hCell.setCellValue(new HSSFRichTextString(heads[i]));
			        //hCell.setCellStyle(csHeader);
			    
		        
					SXSSFRow rowSubTitle = sheet.createRow((short)(i+startRowSubTitle));
					SXSSFCell stCell = rowSubTitle.createCell(0);
					stCell.setCellValue(subtitle[i]);
					stCell.setCellStyle(csSubTitle);
					sheet.addMergedRegion(new CellRangeAddress(
							i+startRowSubTitle,				//first row (0-based)
							i+startRowSubTitle,				//last row  (0-based)
											0,				//first column (0-based)
											heads.length-1	//last column  (0-based)
											));
				}
			}
			
			//--- FOOTER ----- Create a footer with timestamp and page number info
			Footer footer = sheet.getFooter();
			// preserve existing footer (e.g. in a template)
			if( footer.getLeft().isEmpty() )
			{
			    footer.setLeft( "&\"Arial,Regular\"" + "&7 " + "&D &T" );
			}
			if( footer.getRight().isEmpty() )
			{
				footer.setRight( "&\"Arial,Regular\"" + "&7 " + "&P / &N" );
			}

			//--- HEADERS ----- make the titles row
			// header style
	    	CellStyle csHeader = wbs.createCellStyle();
	    	csHeader.setAlignment(HorizontalAlignment.CENTER);
	    	csHeader.setVerticalAlignment(VerticalAlignment.CENTER);
	    	csHeader.setWrapText(false);
	    	csHeader.setFillPattern(FillPatternType.SOLID_FOREGROUND /*XSSFCellStyle.SOLID_FOREGROUND*/);
	    	csHeader.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
	        // Note - many cells are actually filled with a foreground fill, not a background fill !!!
	        //cs.setFillBackgroundColor(new HSSFColor.GREY_25_PERCENT().getIndex());
	    	csHeader.setBorderBottom(BorderStyle.THIN);
	    	csHeader.setBottomBorderColor(IndexedColors.BLACK.getIndex());
	    	csHeader.setBorderLeft(BorderStyle.THIN);
	    	csHeader.setLeftBorderColor(IndexedColors.BLACK.getIndex());
	    	csHeader.setBorderTop(BorderStyle.THIN);
	    	csHeader.setTopBorderColor(IndexedColors.BLACK.getIndex());
	    	csHeader.setBorderRight(BorderStyle.THIN);
	    	csHeader.setRightBorderColor(IndexedColors.BLACK.getIndex());
			Font font = wbs.createFont();
			font.setFontHeightInPoints((short)(font.getFontHeightInPoints()-1));
			font.setBold(true);
	        //font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
	        csHeader.setFont(font);
	        
			SXSSFRow row = sheet.createRow(nStartRow);
		    for(int i=0; i<heads.length;i++)
		    {
		    	SXSSFCell hCell = row.createCell(i);
		    	hCell.setCellValue(new XSSFRichTextString(heads[i]));
		        hCell.setCellStyle(csHeader);
		    }

		    
			//--- CREATE CELL STYLES ----- Create a cell styles for every column
			CellStyle[] aCS = new CellStyle[heads.length];
			//ArrayList arrCellStyle = new ArrayList(heads.length);
			
			// create some basic cell styles to use for column cells 
			// Caution: You can define up to 4000 unique styles in a .xls workbook (so never create them in a loop!!)
	        CellStyle csCommonLeft = wbs.createCellStyle();
	        csCommonLeft.setAlignment(HorizontalAlignment.LEFT);
	        csCommonLeft.setVerticalAlignment(VerticalAlignment.TOP);
	        csCommonLeft.setWrapText(false);
	        csCommonLeft.setBorderBottom(BorderStyle.DOTTED);
	        csCommonLeft.setBottomBorderColor(IndexedColors.BLACK.getIndex());
	        csCommonLeft.setBorderLeft(BorderStyle.DOTTED);
	        csCommonLeft.setLeftBorderColor(IndexedColors.BLACK.getIndex());
	        csCommonLeft.setBorderTop(BorderStyle.DOTTED);
	        csCommonLeft.setTopBorderColor(IndexedColors.BLACK.getIndex());
	        csCommonLeft.setBorderRight(BorderStyle.DOTTED);
	        csCommonLeft.setRightBorderColor(IndexedColors.BLACK.getIndex());
	        // cell style with center alignment
	        CellStyle csCommonCenter = wbs.createCellStyle();
	        csCommonCenter.cloneStyleFrom(csCommonLeft);
	        csCommonCenter.setAlignment(HorizontalAlignment.CENTER);
	        // cell style with right alignment
	        CellStyle csCommonRight = wbs.createCellStyle();
	        csCommonRight.cloneStyleFrom(csCommonLeft);
	        csCommonRight.setAlignment(HorizontalAlignment.RIGHT);
	        
	        // create a cell style for every column (basis are the data types in the first row)
	        //Ivy.log().debug("EXCEL EXPORT: generateWorkbook -> START CREATE CELL STYLES: "+ (new DateTime().format()));
	    	for(int j=0; j<x; j++)	// get every column
	    	{ 
	    		CellStyle csTemp = null;
	    	   	// get the data object in the first row to detect the data type
	    		Object obj = rows[0][j];
	    		// convert numeric strings to number if requested
				if( convertNumericStrings && obj instanceof String )
				{
					try 
					{
						obj = new Double(Double.parseDouble((String) obj.toString()));
					} 
					catch (NumberFormatException e) 
					{
						// Not a number --> Nothing to do
						//Ivy.log().info("NumberFormatException: "+ e.getMessage());
					}
					catch (Exception e) 
					{
						// Not a number --> Nothing to do
						//Ivy.log().warn("Parsing of Number failed: "+ e.getMessage());
						ExLog.warn(this, new Object(){}, "Parsing of Number failed: "+ e.getMessage());
					}
				}
	    		// --- INTEGER / BIG INTEGER-----
	    		if( obj instanceof java.lang.Integer || obj instanceof java.math.BigInteger )
	    		{
	    			// treat it like double
	    			csTemp = wbs.createCellStyle();
	    			csTemp.cloneStyleFrom(csCommonRight);
	    			csTemp.setDataFormat(format.getFormat("#,##0"));
	    			aCS[j] = csTemp;
	    		}
	    		// --- BIG DECIMAL -----
	    		else if( obj instanceof java.math.BigDecimal )
	    		{
	    			csTemp = wbs.createCellStyle();
	    			csTemp.cloneStyleFrom(csCommonRight);
	    			BigDecimal bg =  (BigDecimal)obj;
	    			csTemp.setDataFormat( getNumberFormat(wbs, bg.scale()) );
	    			aCS[j] = csTemp;
	    		}
	    		// --- DOUBLE / FLOAT -----
	    		else if( obj instanceof java.lang.Double || obj instanceof java.lang.Float )
	    		{
	    			csTemp = wbs.createCellStyle();
	    			csTemp.cloneStyleFrom(csCommonRight);
	    			csTemp.setDataFormat(format.getFormat("#,##0.00"));
	    			aCS[j] = csTemp;
	    		}
	    		// --- NUMBER / ALL OTHER NUMERIC -----
	    		else if( obj instanceof java.lang.Number )
	    		{
	    			csTemp = wbs.createCellStyle();
	    			csTemp.cloneStyleFrom(csCommonRight);
	    			//csTemp.setDataFormat(format.getFormat("#,##0.00"));
	    			aCS[j] = csTemp;
	    		}
	    		// --- DATE ----- 
	    		else if( obj instanceof java.util.Date || 
	    				 obj instanceof GregorianCalendar )
	    		{
	    			GregorianCalendar cal;
	    			csTemp = wbs.createCellStyle();
	    			csTemp.cloneStyleFrom(csCommonCenter);
	    				    			
	    			if( obj instanceof GregorianCalendar )
	    			{
	    				cal = (GregorianCalendar)obj;
	    			}
	    			else
	    			{
		    			// java.util.Date is deprecated, so we use GregorianCalendar here
		    			java.util.Date dValue = null;
//		    			if( obj instanceof Date )
//		    			{
//		    				dValue = ((Date) obj).toDate(); 
//		    			}
//		    			else if( obj instanceof DateTime )
//		    			{
//		    				dValue = ((DateTime) obj).toDate(); 
//		    			}
//		    			else
//		    			{
//		    				dValue = (java.util.Date)obj;
//		    			}
		    			dValue = (java.util.Date)obj;
		    			cal = new GregorianCalendar();
		    			cal.setTime(dValue);
	    			}
	    			// check for time part
	    			long nTime = cal.get(Calendar.HOUR)+cal.get(Calendar.MINUTE)+cal.get(Calendar.SECOND)+cal.get(Calendar.MILLISECOND);
	    			//Ivy.log().info("TIME: "+nTime);
	    			//Ivy.log().info("YEAR: "+cal.get(Calendar.YEAR));
	    			if( nTime == 0 )
	    			{
	    				// no time part --> format it as Date
	    				csTemp.setDataFormat(formatDate);
	    			}
	    			else if( cal.get(Calendar.YEAR) < 1900  )
	    			{
	    				// no date part given --> format as Time
	    				// the function setDataFormat seems not to work with YEAR < 1900, so set it to 1900 (however, output is only the time part)
	    				cal.set(Calendar.YEAR, 1900);
	    				csTemp.setDataFormat(formatTime);
//	    				Ivy.log().info("DATETIME: "+cal.get(Calendar.YEAR)+"="+cal.get(Calendar.MONTH)+"="+cal.get(Calendar.DAY_OF_MONTH));
	    			}
	    			else 
	    			{
	    				// format as full date
	    				csTemp.setDataFormat(formatDateTime);
	    			}
	    			aCS[j] = csTemp;
	    			///Ivy.log().debug("CELL CREATED: Date = "+ cal.toString());
	    		}
	    		// --- TIME ----- 
	    		// --> does not really work for RTable source, because we never get an object of type Time from RTable (always java.util.Date)
//	    		else if( obj instanceof Time )
//	    		{
//	    			csTemp = wb.createCellStyle();
//	    			csTemp.cloneStyleFrom(csCommonCenter);
//	    			csTemp.setDataFormat(formatDateTime);
//	    			aCS[j] = csTemp;
//	    			///Ivy.log().debug("CELL CREATED: Time = "+ cal.toString());
//	    		}
	    		// --- STRING -----
	    		else if( obj instanceof  java.lang.String )
	    		{
	    			csTemp = wbs.createCellStyle();
	    			csTemp.cloneStyleFrom(csCommonLeft);
	    			aCS[j] = csTemp;
	    		}
	    		// --- ALL OTHERS -----
	    		else
	    		{
	    			csTemp = wbs.createCellStyle();
	    			csTemp.cloneStyleFrom(csCommonLeft);
	    			aCS[j] = csTemp;
	    		}
	    	}
	    	//Ivy.log().debug("EXCEL EXPORT: generateWorkbook -> END CREATE CELL STYLES: "+ (new DateTime().format()));
		    
		    // --- VALUES ----- make the values cells
		    // Never use float or double datatypes to calculate amounts/monetary values, always use BigDecimal (als no casts between String and Float or Double )
		    // --> you will lost decimal places (i.e. to avoid any precision loss)!!
		    // CAUTION: Always use the BigDecimal("String") constructor, and never BigDecimal(double) for monetary calculation

		    ///Ivy.log().debug("START ROW: "+ nStartRow);
	    	//Ivy.log().debug("EXCEL EXPORT: generateWorkbook -> START DATA LOOP: "+ (new DateTime().format()));
	    	// 05.09.2014/HE: with XSSF and version 2007 the limits are 1,048,576 rows by 16,384 columns
	    	//                Let's leave it at 65536, because more rows does not make sense and requires a lot of time!!
	    	// 22.01.2020/HE: unblock limit of 65536, set it to 1,000,000 rows
		    for(int i=0; i<y && nStartRow+i+1 < 1000000 ; i++)	// get every row (excel max rows 1,048,576)
		    { 
		    	SXSSFRow r= sheet.createRow(nStartRow+i+1);
		    	for(int j=0; j<x; j++)	// get every column
		    	{ 
		    		// we build each cell value
		    		//HSSFCell cell = r.createCell(j);
		    		SXSSFCell cell = null;
			    	// get the data object
		    		//String sType = table.getValueAt(i, j).getClass().getName();
		    		Object obj = rows[i][j]; 				//table.getValueAt(i, j);
					///Ivy.log().debug("OBJECT FOR CELL: "+ obj.getClass().getName());
		    		
		    		if( obj != null)
		    		{
		    			// convert numeric strings to number if requested
						if( convertNumericStrings && obj instanceof String )
						{
							try 
							{
								obj = new Double(Double.parseDouble((String) obj.toString()));
							} 
							catch (NumberFormatException e) 
							{
								// Not a number --> Nothing to do
								//Ivy.log().info("NumberFormatException: "+ e.getMessage());
							}
							catch (Exception e) 
							{
								// Not a number --> Nothing to do
								//Ivy.log().warn("EXCEL EXPORT: generateWorkbook -> Parsing of Number failed: "+ e.getMessage());
								ExLog.warn(this, new Object(){}, "EXCEL EXPORT: generateWorkbook -> Parsing of Number failed: "+ e.getMessage());
							}
						}
			    		// --- INTEGER / BIG INTEGER-----
			    		if( obj instanceof java.lang.Integer || obj instanceof java.math.BigInteger )
			    		{
			    			// treat it like double
			    			cell = r.createCell(j, CellType.NUMERIC);
			    			cell.setCellStyle(aCS[j]);
			    			//cell = ExcelExporterHSSF.createCell(wb, r, j, HSSFCell.CELL_TYPE_NUMERIC, csCommon, HSSFCellStyle.ALIGN_RIGHT, HSSFCellStyle.VERTICAL_TOP);
			    			BigDecimal bg = new BigDecimal( obj.toString() );
			    			//cell.getCellStyle().setDataFormat(format.getFormat("#,##0"));
			    			cell.setCellValue( bg.doubleValue() );
			    			///Ivy.log().debug("CELL CREATED: Integer = "+ bg.toString());
			    			//cell.setCellValue( ((java.lang.Integer)table.getValueAt(i, j)) );
			    		}
			    		// --- BIG DECIMAL -----
			    		else if( obj instanceof java.math.BigDecimal )
			    		{
			    			cell = r.createCell(j, CellType.NUMERIC);
			    			cell.setCellStyle(aCS[j]);
			    			//cell = ExcelExporterHSSF.createCell(wb, r, j, HSSFCell.CELL_TYPE_NUMERIC, csCommon, HSSFCellStyle.ALIGN_RIGHT, HSSFCellStyle.VERTICAL_TOP);
			    			BigDecimal bg =  (BigDecimal)obj;
			    			//Ivy.log().info("BIGDECIMAL_Double_SCALE: "+ bg.doubleValue() +" :: "+bg.scale() );
			    			//cell.getCellStyle().setDataFormat((short)4);	//"#,##0.00"
			    			// set decimalPlaces depending on scale
			    			//cell.getCellStyle().setDataFormat( getNumberFormat(wb, bg.scale()) );
			    			cell.setCellValue( bg.doubleValue() );
			    			///Ivy.log().debug("CELL CREATED: BigDecimal = "+ bg.toString());
			    		}
			    		// --- DOUBLE / FLOAT -----
			    		else if( obj instanceof java.lang.Double || obj instanceof java.lang.Float )
			    		{
			    			cell = r.createCell(j, CellType.NUMERIC);
			    			cell.setCellStyle(aCS[j]);
			    			//cell = ExcelExporterHSSF.createCell(wb, r, j, HSSFCell.CELL_TYPE_NUMERIC, csCommon, HSSFCellStyle.ALIGN_RIGHT, HSSFCellStyle.VERTICAL_TOP);
			    			BigDecimal bg = new BigDecimal( obj.toString() );
			    			//Ivy.log().info("BIGDECIMAL_Double_SCALE: "+ bg.doubleValue() +" :: "+bg.scale() );
			    			//cell.getCellStyle().setDataFormat((short)4);	//"#,##0.00"
			    			//cell.getCellStyle().setDataFormat(format.getFormat("#,##0.00"));
			    			cell.setCellValue( bg.doubleValue() );
			    			///Ivy.log().debug("CELL CREATED: Double/Float = "+ bg.toString());
			    		}
			    		// --- NUMBER / ALL OTHER NUMERIC -----
			    		else if( obj instanceof java.lang.Number )
			    		{
			    			cell = r.createCell(j, CellType.NUMERIC);
			    			cell.setCellStyle(aCS[j]);
			    			//cell = ExcelExporterHSSF.createCell(wb, r, j, HSSFCell.CELL_TYPE_NUMERIC, csCommon, HSSFCellStyle.ALIGN_RIGHT, HSSFCellStyle.VERTICAL_TOP);
			    			BigDecimal bg = new BigDecimal( obj.toString() );
			    			cell.setCellValue( bg.doubleValue() );
			    			///Ivy.log().debug("CELL CREATED: Number = "+ bg.toString());
			    		}
			    		// --- DATE ----- 
			    		else if( obj instanceof java.util.Date  || 
			    				 obj instanceof GregorianCalendar )
			    		{
			    			GregorianCalendar cal;
			    			cell = r.createCell(j, CellType.NUMERIC);
			    			cell.setCellStyle(aCS[j]);
			    			//cell = ExcelExporterHSSF.createCell(wb, r, j, HSSFCell.CELL_TYPE_NUMERIC, csCommon, HorizontalAlignment.CENTER, HSSFCellStyle.VERTICAL_TOP);
			    			if( obj instanceof GregorianCalendar )
			    			{
			    				cal = (GregorianCalendar)obj;
			    			}
			    			else
			    			{
				    			// java.util.Date is deprecated, so we use GregorianCalendar here
				    			java.util.Date dValue = null;
//				    			if( obj instanceof Date )
//				    			{
//				    				dValue = ((Date) obj).toDate();
//				    			}
//				    			else if( obj instanceof DateTime )
//				    			{
//				    				dValue = ((DateTime) obj).toDate(); 
//				    			}
//				    			else
//				    			{
//				    				dValue = (java.util.Date)obj;
//				    			}
				    			dValue = (java.util.Date)obj;
				    			cal = new GregorianCalendar();
				    			cal.setTime(dValue);
			    			}
			    			// check for time part
			    			long nTime = cal.get(Calendar.HOUR)+cal.get(Calendar.MINUTE)+cal.get(Calendar.SECOND)+cal.get(Calendar.MILLISECOND);
			    			//Ivy.log().info("TIME: "+nTime);
			    			//Ivy.log().info("YEAR: "+cal.get(Calendar.YEAR));
			    			if( nTime == 0 )
			    			{
			    				if( cal.get(Calendar.YEAR) < 1900  )
			    				{
			    					cal.set(Calendar.YEAR, 1900);
			    					//Ivy.log().info("DATETIME RESETTED: "+cal.get(Calendar.YEAR)+"="+cal.get(Calendar.MONTH)+"="+cal.get(Calendar.DAY_OF_MONTH));
			    				}
			    				// no time part --> format it as Date
			    				//cell.getCellStyle().setDataFormat(formatDate);
			    			}
			    			else if( cal.get(Calendar.YEAR) < 1900  )
			    			{
			    				// no date part given --> format as Time
			    				// the function setDataFormat seems not to work with YEAR < 1900, so set it to 1900 (however, output is only the time part)
			    				cal.set(Calendar.YEAR, 1900);
			    				//cell.getCellStyle().setDataFormat(formatTime);
			    				//Ivy.log().info("DATETIME: "+cal.get(Calendar.YEAR)+"="+cal.get(Calendar.MONTH)+"="+cal.get(Calendar.DAY_OF_MONTH));
			    			}
			    			else 
			    			{
			    				// format as full date
			    				//cell.getCellStyle().setDataFormat(formatDateTime);
			    			}
			    			// Fill cell with an empty value if the date is prior 1900 (in fact the originally data was empty or 01.01.0001)
			    			if( cal.get(Calendar.YEAR) <= 1900 )
			    			{
			    				cell.setCellValue("");
			    			}else
			    			{
			    				cell.setCellValue(cal);
			    			}
			    			//Ivy.log().debug("CELL CREATED: Date = "+ cal.toString());
			    			
			    		}
			    		// --- TIME ----- 
			    		// --> does not really work for RTable source, because we never get an object of type Time from RTable (always java.util.Date)
//			    		else if( obj instanceof Time )
//			    		{
//			    			cell = r.createCell(j, CellType.NUMERIC);
//			    			cell.setCellStyle(aCS[j]);
//			    			//cell = ExcelExporterHSSF.createCell(wb, r, j, HSSFCell.CELL_TYPE_NUMERIC, csCommon, HorizontalAlignment.CENTER, HSSFCellStyle.VERTICAL_TOP);
//			    			Time tValue = (Time) obj;
//			    			java.util.GregorianCalendar cal = new GregorianCalendar(1900, 1, 1, tValue.getHours(), tValue.getMinutes(), tValue.getSeconds());
//			    			//cell.getCellStyle().setDataFormat(formatDateTime);
//			    			cell.setCellValue(cal);
//			    			///Ivy.log().debug("CELL CREATED: Time = "+ cal.toString());
//			    		}
			    		// --- STRING -----
			    		else if( obj instanceof  java.lang.String )
			    		{
			    			cell = r.createCell(j, CellType.STRING);
			    			cell.setCellStyle(aCS[j]);
			    			//cell = ExcelExporterHSSF.createCell(wb, r, j, HSSFCell.CELL_TYPE_STRING, csCommon, HSSFCellStyle.ALIGN_LEFT, HSSFCellStyle.VERTICAL_TOP);
			    			cell.setCellValue(new XSSFRichTextString( obj.toString() ));
			    			///Ivy.log().debug("CELL CREATED: String = "+ obj.toString());
			    		}
			    		// --- ALL OTHERS -----
	
			    		else
			    		{
			    			cell = r.createCell(j, CellType.STRING);
			    			cell.setCellStyle(aCS[j]);
			    			//cell = ExcelExporterHSSF.createCell(wb, r, j, HSSFCell.CELL_TYPE_STRING, csCommon, HSSFCellStyle.ALIGN_LEFT, HSSFCellStyle.VERTICAL_TOP);
			    			cell.setCellValue( obj.toString() );
			    			///Ivy.log().debug("CELL CREATED: Other = "+ obj.toString());
			    		}
		    		}
		    		else
		    		{
		    			// No object is given, so set an empty string
		    			cell = r.createCell(j, CellType.STRING);
		    			cell.setCellStyle(csCommonLeft);
		    			cell.setCellValue( "" );
		    			//Ivy.log().debug("EXCEL EXPORT: generateWorkbook -> CELL CREATED: Value of cell is null !!");
		    			ExLog.info(this, new Object(){}, "EXCEL EXPORT: generateWorkbook -> CELL CREATED: Value of cell is null !!");
		    		}
		    	}
		    }
		    //Ivy.log().debug("EXCEL EXPORT: generateWorkbook -> END DATA LOOP: "+ (new DateTime().format())); 
		    // adjust size of columns (This process can be relatively slow on large sheets, so do it only on small sheets)
		    if( y <= 999999 )
		    {
		    	sheet.trackAllColumnsForAutoSizing();
			    for(short i =0; i< x; i++){
			    	sheet.autoSizeColumn(i, false);
			    }
			    //Ivy.log().debug("EXCEL EXPORT: generateWorkbook -> AUTOSIZING_COLUMNS FINISHED: "+ (new DateTime().format()));
		    }
		    // print setup, set margins to 10 mm (0.394 inch), header/footer to 5 mmm (0.197 inch)
		    sheet.setAutobreaks(true);
		    //Ivy.log().debug("EXCEL EXPORT: generateWorkbook -> AUTOBREAKS FINISHED: "+ (new DateTime().format()));
		    //Ivy.log().warn("MARGIN CONSTANTS: "+HSSFSheet.BottomMargin+":"+HSSFSheet.TopMargin+":"+HSSFSheet.LeftMargin+":"+HSSFSheet.RightMargin+":"+HSSFSheet.HeaderMargin+":"+HSSFSheet.FooterMargin);
		    /*
		    sheet.setMargin(HSSFSheet.BottomMargin, 0.394);
		    sheet.setMargin(HSSFSheet.TopMargin, 0.394);
		    sheet.setMargin(HSSFSheet.LeftMargin, 1.0);
		    sheet.setMargin(HSSFSheet.RightMargin, 0.394);
		    //sheet.setMargin(HSSFSheet.HeaderMargin, 0.197);
		    //sheet.setMargin(HSSFSheet.FooterMargin, 0.197);
		    HSSFPrintSetup ps = sheet.getPrintSetup();
		    ps.setHeaderMargin(0.197);
		    ps.setFooterMargin(0.197);
		    ps.setPaperSize(HSSFPrintSetup.A4_PAPERSIZE);
		    ps.setLandscape(true);
		    */
		    
		    // log number of exported rows
		    ExLog.info(this, new Object(){}, y +" rows with "+x+" columns exported!");
		    
		}
		catch (Exception e) 
		{
			//Ivy.log().error("EXCEL EXPORT: generateWorkbook -> Exception(generating Workbook): "+e.getMessage());
			ExLog.error(this, new Object(){}, "EXCEL EXPORT: generateWorkbook -> Exception(generating Workbook): "+e.getMessage());
			e.printStackTrace();
		    if( wbs == null )
		    {
		    	throw new NullPointerException("EXCEL EXPORT: generateWorkbook -> creation of new Workbook XSSF failed: Reference wb is null.");
		    }
		}
	
		return wbs;
	}
	
	private void changeContentType(String filePath)
	{
		java.io.File f = null;
		OPCPackage opcPackage;
		FileInputStream inputStream = null;
		FileOutputStream outputStream = null;
		boolean bReplacementSuccessful = false;
		
		try
		{
			if( filePath != null && !filePath.isEmpty() )
			{
				// check if excel file exists
				f = new java.io.File(filePath);
				if( f.isFile() )
				{
					//Ivy.log().debug("EXCEL EXPORT: changeContentType -> file to change found: "+filePath+" )!");
					inputStream = new FileInputStream(new java.io.File(filePath));
				    
				    opcPackage = OPCPackage.open(inputStream);

				    // try to get the contentType of the PackagePart (WORKBOOK)
				    java.util.List<PackagePart> al = opcPackage.getPartsByName(Pattern.compile("/xl/workbook.xml"));
				    if( al.size() == 1 )
				    {
				    	// detect XSSFRelation (ContentType)
				    	for (PackagePart packagePart : al) 
					    {
			                String pContent = packagePart.getContentType();
			                if( pContent.equals( XSSFRelation.WORKBOOK.getContentType() ) )
			                {
			                	// no conversion required (type is .xlsx)
			                	//Ivy.log().debug("EXCEL EXPORT: changeContentType -> CONTENT TYPE already correct: "+pContent);
			                }
			                else
			                {
			                	// convert to .xlsx (e.g. from .xltx to .xlsx)
			                	//Ivy.log().debug("CONTENT TYPE WORKBOOK: "+XSSFRelation.WORKBOOK.getContentType());
			                	//Ivy.log().debug("EXCEL EXPORT: changeContentType -> CONTENT TYPE to replace: "+pContent);
			                	bReplacementSuccessful = opcPackage.replaceContentType(pContent, XSSFRelation.WORKBOOK.getContentType());
			                	//Ivy.log().debug("EXCEL EXPORT: changeContentType -> CONTENT TYPE successful replaced: "+bReplacementSuccessful);
			                }
						    inputStream.close();
					    }
				    	
				    	// Save file (replace existing)
				    	if( bReplacementSuccessful )
				    	{
					    	outputStream = new FileOutputStream(filePath);
					    	opcPackage.save(outputStream);
					    	outputStream.close();
				    	}
				    }
				    else
				    {
				    	// PackagePart not found --> replacment not possible -> write log entry
				    	//Ivy.log().error("EXCEL EXPORT: changeContentType -> File type for content change (OPCPackage) not supported: "+filePath);
				    	ExLog.error(this, new Object(){}, "EXCEL EXPORT: changeContentType -> File type for content change (OPCPackage) not supported: "+filePath);
				    }
				}
				else
				{
					// file seems to be a directory or does not exist 
					//Ivy.log().error("EXCEL EXPORT: changeContentType -> File is not a file (directory ??) or file does not exist (not supported): "+filePath);
					ExLog.error(this, new Object(){}, "EXCEL EXPORT: changeContentType -> File is not a file (directory ??) or file does not exist (not supported): "+filePath);
				}
			}
		}
		catch(SecurityException ex)
		{
			//Ivy.log().error("EXCEL EXPORT: changeContentType -> SecurityException: "+ex.getMessage());
			ExLog.error(this, new Object(){}, "EXCEL EXPORT: changeContentType -> SecurityException: "+ex.getMessage());
		}
		catch(FileNotFoundException ex)
		{
			//Ivy.log().error("EXCEL EXPORT: changeContentType -> FileNotFoundException: "+ex.getMessage());
			ExLog.error(this, new Object(){}, "EXCEL EXPORT: changeContentType -> FileNotFoundException: "+ex.getMessage());
		}
		catch(Exception ex)
		{
			//Ivy.log().error("EXCEL EXPORT: changeContentType -> GeneralException: "+ex.getMessage());
			ExLog.error(this, new Object(){}, "EXCEL EXPORT: changeContentType -> GeneralException: "+ex.getMessage());
		}
		finally
		{
			// clean up: close all streams
			if( inputStream != null )
			{
				try{ inputStream.close(); }
				catch(IOException ex)
				{
					//Ivy.log().error("EXCEL EXPORT: createWorkbook -> Can't close inputStream - Exception: "+ex.getMessage());
					ExLog.error(this, new Object(){}, "EXCEL EXPORT: createWorkbook -> Can't close inputStream - Exception: "+ex.getMessage());
				}
			}
			if( outputStream != null )
			{
				try{ outputStream.close(); }
				catch(IOException ex)
				{
					//Ivy.log().error("EXCEL EXPORT: createWorkbook -> Can't close outputStream - Exception: "+ex.getMessage());
					ExLog.error(this, new Object(){}, "EXCEL EXPORT: createWorkbook -> Can't close outputStream - Exception: "+ex.getMessage());
				}
			}
		}
	}
	
	private short getNumberFormat(SXSSFWorkbook wb, int decimalPlaces)
	{
		short result = 0;
		try
		{
			DataFormat format = wb.createDataFormat();
			if( decimalPlaces == 0)
			{
				result = format.getFormat("#,##0");
			}
			else if( decimalPlaces == 1)
			{
				result = format.getFormat("#,##0.0");
			}
			else if( decimalPlaces == 2)
			{
				result = format.getFormat("#,##0.00");
			}
			else if( decimalPlaces == 3)
			{
				result = format.getFormat("#,##0.000");
			}
			else if( decimalPlaces == 4)
			{
				result = format.getFormat("#,##0.0000");
			}
			else if( decimalPlaces == 5)
			{
				result = format.getFormat("#,##0.00000");
			}
			else if( decimalPlaces == 6)
			{
				result = format.getFormat("#,##0.000000");
			}
			else if( decimalPlaces == 7)
			{
				result = format.getFormat("#,##0.0000000");
			}
			else if( decimalPlaces == 8)
			{
				result = format.getFormat("#,##0.00000000");
			}
			else if( decimalPlaces >= 9)
			{
				result = format.getFormat("#,##0.000000000");
			}
			else 
			{
				result = format.getFormat("#,##0.00");
			}
		}
		catch(Exception ex)
		{
			//Ivy.log().warn("EXCEL EXPORT: getNumberFormat -> NumberFormat for cell not found: argument for decimal places: = "+ decimalPlaces);
			ExLog.warn(this, new Object(){}, "EXCEL EXPORT: getNumberFormat -> NumberFormat for cell not found: argument for decimal places: = "+ decimalPlaces);
		}
		return result;
	}
	
// ----------- PUBLIC HELPER FUNCTIONS -----------------------------------------------
	
	// DEACTIVATED FOR PURE JAVA VERSION !
	
//	public static List<List<Object>> removeColumns(List<Number> lstCol, List<List<Object>> matrix)
//	{
//		// helper function to remove columns in a list
//		// much higher performance here in java than in ivy script !!
//		List<List<Object>> lstTempData = matrix.clone();
//		lstTempData.clear();
//		List<Object> lst = List.create(Object.class);
//		
//		for(int k=0 ; k < matrix.size() ; k++ )
//		{
//			lst = matrix.get(k).clone();
//			// step backwards through the list and remove non selected columns/fields 
//			for(int n=lst.size()-1 ; n >= 0 ; n-- )
//			{
//				if( lstCol.contains(n) == false )
//				{
//					lst.remove(n);
//				}
//			}
//			lstTempData.add(lst);
//			//Ivy.log().debug("LIST ADDED: "+ lst.size());
//		}
//		return lstTempData;
//	}
	
	
	
	
/*	
	/**
     * Creates a cell and aligns it a certain way.
     *
     * @param wb     the workbook
     * @param row    the row to create the cell in
     * @param column the column number to create the cell in
     * @param type   the cell type to create
     * @param halign the horizontal alignment for the cell.
     * @param valign the vertical alignment for the cell.
     */
	
/*
    private static HSSFCell createCell(HSSFWorkbook wb, HSSFRow row, int column, int cellType, HSSFCellStyle cs) 
    {
    	HSSFCell cell = row.createCell(column, cellType);
	    try
	    {
	        //cell.setCellValue(new HSSFRichTextString("Align It"));
	    	Ivy.log().warn("NUMBER OF STYLES: "+wb.getNumCellStyles());
	        //HSSFCellStyle cs = wb.createCellStyle();
//	        cs.setWrapText(true);
//	        cs.setBorderBottom(HSSFCellStyle.BORDER_DOTTED);
//	        cs.setBottomBorderColor(new HSSFColor.BLACK().getIndex());
//	        cs.setBorderLeft(HSSFCellStyle.BORDER_DOTTED);
//	        cs.setLeftBorderColor(new HSSFColor.BLACK().getIndex());
//	        cs.setBorderTop(HSSFCellStyle.BORDER_DOTTED);
//	        cs.setTopBorderColor(new HSSFColor.BLACK().getIndex());
//	        cs.setBorderRight(HSSFCellStyle.BORDER_DOTTED);
//	        cs.setRightBorderColor(new HSSFColor.BLACK().getIndex());
	        cell.setCellStyle(cs);
	        
	    }
	    catch(java.lang.IllegalStateException ex)
	    {
	    	Ivy.log().error("STYLE ERROR: maximum number of cell styles exceeded the limit! ("+wb.getNumCellStyles()+"), "+ex.getMessage());
	    }
	    catch(Exception ex)
	    {
	    	Ivy.log().error("STYLE ERROR: Unknown error! ("+wb.getNumCellStyles()+"), "+ex.getMessage());
	    }
	    
	    return cell;
    }
	
*/
/*	
	/**
	 * 
	 * @param headers 
	 * @param rows 
	 * @param outputPath 
	 * @param fileName 
	 * @param SheetName 
	 * @return
	 */

/*	
	public static java.io.File exportListAsExcel(List<String> headers,List<List<String>> rows, String outputPath, String fileName, String SheetName){
		java.io.File myXLS =null;
		if(rows== null || headers == null || headers.isEmpty() || rows.isEmpty() || rows.get(0).size()!= headers.size()) return myXLS;
		
//		CHECK AND INITIALIZE THE ARGUMENTS
		if(outputPath==null || outputPath.trim().equalsIgnoreCase("")) outputPath="excelExport";
		if(fileName==null || fileName.trim().equalsIgnoreCase("")) fileName="excelFile";
		if(SheetName==null || SheetName.trim().equalsIgnoreCase("")) SheetName="sheet";
		fileName = FileHandler.formatPathWithEndSeparator(outputPath)+FileHandler.getFileNameWithoutExt(fileName)+".xls";
		
	    //build tables of values from the table
		
	    int x= headers.size();
	    int y = rows.size();
	    
	    try {
	    	
		    String heads[] = new String[x];
		    for(int i = 0; i<x; i++){
		    	heads[i] = headers.get(i);
		    }
		    	
			HSSFWorkbook wb = new HSSFWorkbook();
		    FileOutputStream fileOut;
			HSSFSheet sheet = wb.createSheet(SheetName);
			HSSFCellStyle cs = wb.createCellStyle();
			cs.setWrapText(true);
			//make the titles row
			HSSFRow row = sheet.createRow((short)0);
		    for(int i=0; i<heads.length;i++){
		    	HSSFCell cell = row.createCell(i);
		    	cell.setCellValue(new HSSFRichTextString(heads[i]));
		    }
		   
		    //make the values cells
		    for(int i=0; i<y; i++){ // get every row
		    	HSSFRow r= sheet.createRow((short)i+1);
		    	List<String> l = rows.get(i);
		    	for(int j=0; j<x; j++){ // get every column 
		    		// we build each cell value
		    		HSSFCell cell = r.createCell(j);
		    		cell.setCellValue(new HSSFRichTextString(l.get(j)));
		    		cell.setCellStyle(cs);
		    	}
		    }
		    for(short i =0; i< x; i++){
		    	sheet.autoSizeColumn(i, false);
		    }
		    
			fileOut = new FileOutputStream(fileName);
			wb.write(fileOut);
		    fileOut.close();
		    myXLS = new java.io.File(fileName);
		} catch (FileNotFoundException e) {
			Ivy.log().error("FileNotFoundException "+e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			Ivy.log().error("IOException "+e.getMessage());
			e.printStackTrace();
		}
		catch (Exception e) {
			Ivy.log().error("Exception "+e.getMessage());
			e.printStackTrace();
		}
		return myXLS;
	}
*/
	
	

}

