/**
 * (c)2006-2007 by Soreco AG, CH-8603 Schwerzenbach. http://www.soreco.ch
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information of 
 * Soreco AG. You shall not disclose such confidential information and 
 * shall use it only in accordance with the terms of the license 
 * agreement you entered into with Soreco.
 * 
 */

package ch.soreco.xutil.filemanager;


import java.io.*;


/**
 * @author Emmanuel Comba <br>
 * The abstract FileHandler class contains static methods that ease the File management on the server.<br>
 * Some of those methods are used by the FileUploadHandler and FileDownloadHandler java classes from the same package.<br>
 * 
 * It relies on the ch.xpertline.ria.util.file.ReturnedMessage, ch.xpertline.ria.util.file.FolderOnServer and ch.xpertline.ria.util.file.DocumentOnServer dataclasses for returning the result of most of the methods.
 * 
 * This Class has the following static fields:
 * <ul>
 * <li>public static final int SUCCESS_MESSAGE = 1;
 * <li>public static final int ERROR_MESSAGE = 2;
 * <li>public static final int INFORMATION_MESSAGE = 3;
 *</ul>
 * They are used to set the returnedMessage type.

 */

public abstract class FileHandler
{

	/** */
	public static final int SUCCESS_MESSAGE = 1;
	/** */
	public static final int ERROR_MESSAGE = 2;
	/** */
	public static final int INFORMATION_MESSAGE = 3;

	/**
	 * Formats a given path in a path with the right system File.separator characters<br>
	 * It doesn't check if there is a File.separator at the end of the path.
	 * @param _path path
	 * @return formatted path with the system File.separator
	 */
	public static String formatPath(String _path)
	{
		if(_path != null && !_path.equals(""))
		{
			//path = path.replaceAll("\\\\", File.separator);
			_path = org.apache.commons.lang3.StringUtils.replace(_path,"\\", File.separator);
			//path = path.replaceAll("/", File.separator);
			_path = org.apache.commons.lang3.StringUtils.replace(_path,"/", File.separator);
		}
		return _path;
	}

	/**
	 * Formats a given path in a path with the right system File.separator characters<br>
	 * It checks if there is a File.separator at the end of the path. If not it adds one.<br>
	 * It checks if the directory exists, if not it makes it.
	 * @param _path path
	 * @return formatted path with the system File.separator
	 */
	public static String formatPathWithEndSeparator(String _path)
	{
		if(_path != null && !_path.equals(""))
		{
//			path = path.replaceAll("\\\\", File.separator);
			_path = org.apache.commons.lang3.StringUtils.replace(_path,"\\", File.separator);
			//path = path.replaceAll("/", File.separator);
			_path = org.apache.commons.lang3.StringUtils.replace(_path,"/", File.separator);
			File serverDir = new File(_path);
			if((serverDir.exists() && !serverDir.isDirectory()) || !serverDir.exists())
				serverDir.mkdirs();
			if(_path.lastIndexOf(File.separator) != _path.length() - 1) 
				_path=_path+java.io.File.separator;
		}
		return _path;
	}

	/**
	 * Formats a given path in a path with the right system File.separator characters<br>
	 * It checks if there is a File.separator at the end of the path. If not it adds one.<br>
	 * If the boolean argument is true, it checks if the directory exists and creates it if it doesn't exist.
	 * @param _path the path to format with the right end file separator
	 * @param _createDirIfNotExits boolean telling if has to check for the directory existence
	 * @return formatted path with the system File.separator
	 */
	public static String formatPathWithEndSeparator(String _path, boolean _createDirIfNotExits)
	{
		if(_path != null && !_path.equals(""))
		{

			_path = org.apache.commons.lang3.StringUtils.replace(_path,"\\", File.separator);
			_path = org.apache.commons.lang3.StringUtils.replace(_path,"/", File.separator);
			if(_createDirIfNotExits){
				File serverDir = new File(_path);
				if((serverDir.exists() && !serverDir.isDirectory()) || !serverDir.exists())
					serverDir.mkdirs();
			}
			if(_path.lastIndexOf(File.separator) != _path.length() - 1) 
			{
				_path=_path+java.io.File.separator;
			}
		}
		return _path;
	}


	/**
	 * return the file name without the file extension
	 * @param fileName the string filename 
	 * @return file name without the file extension
	 */
	public static String getFileNameWithoutExt(String fileName){
		String s="";
		if(fileName.lastIndexOf(".")>0)
			s = fileName.substring(0,fileName.lastIndexOf("."));
		else s=fileName;

		return s;
	}

	/**
	 * Returns the File extension without "." from a given file name
	 * @param fileName the string filename
	 * @return the file extension
	 */
	public static String getFileExtension(String fileName){
		String s="";
		if(fileName.lastIndexOf(".")>0)
			s = fileName.substring(fileName.lastIndexOf(".")+1);

		return s;
	}
	
	/**
	 * deletes a file (directories are not supported)
	 * @param filePath the full path and file name (e.g. C:\temp\myfile.txt)
	 * @return true if and only if the file is successfully deleted; false otherwise
	 */
	public static boolean deleteFile(String filePath)
	{
		boolean result = false;
		
		File f = new File(filePath);
		if( f.exists() && f.isFile() )
		{
			result = f.delete();
		}
		
		return result;
	}
	

}
